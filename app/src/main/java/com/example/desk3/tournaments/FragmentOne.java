package com.example.desk3.tournaments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

/**
 * Created by Desk 3 on 5/31/2018.
 */

public class FragmentOne extends Fragment implements InactiveInterface {

    private static final String EXTRA_TYPE = "type";
    private static final String EXTRA_ID = "id";
    private static final String EXTRA_DATA = "extra_data";
    private static final String EXTRA_REFRESH = "extra_refresh";
    private RecyclerView recyclerView;
    private InactiveAdapter inactiveAdapter;
    private ApiService apiService;
    private String token;

    private static final int ACTIVITY_REQUEST_CODE_TOURNAMENT = 2;
    int type;
    int id;
    private EndlessRecyclerViewScrollListener scrollListener;
    private List<TournamentsEntity> tournamentsEntityList;
    private ProgressBar progressBar;
    private int offset = 1;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int limit;
    int trn_id;
    private boolean check = true;
    private int refresh;
    //    private List<TournamentsEntity> tournamentsEntities = new ArrayList<>();
    private String giftPrize;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt(EXTRA_TYPE);
            id = getArguments().getInt(EXTRA_ID);
            refresh = getArguments().getInt(EXTRA_REFRESH);
            tournamentsEntityList = getArguments().getParcelableArrayList(EXTRA_DATA);

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_one, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

        recyclerView = view.findViewById(R.id.inactive_recycler);
        swipeRefreshLayout = view.findViewById(R.id.activity_main_swipe_refresh_layout);
//        progressBar = view.findViewById(R.id.progressBar1);

        token = SharedPrefUtils.getStringPreference(getContext(), VerificationPageActivity.TOKEN);


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);


//        getTournamentByStatus(token, type, id);
        if (tournamentsEntityList != null) {
            inactiveAdapter = new InactiveAdapter(this);
            recyclerView.setAdapter(inactiveAdapter);
            inactiveAdapter.addTournaments(tournamentsEntityList);
        }
//        else{
//            getTournamentByStatus(token,type,id,0,4,false);
//        }


        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                getTournamentByStatus(token, type, id, offset, limit, false);
                offset += 1;

            }
        };
        // Adds the scroll listener to RecyclerView
        recyclerView.addOnScrollListener(scrollListener);


        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                getTournamentByStatus(token, type, id, offset, limit, false);
                offset += 1;

               /* getTournamentByStatus(token, type, id, offset, limit, false);
                offset += 1;*/
            }
        };
        // Adds the scroll listener to RecyclerView
        recyclerView.addOnScrollListener(scrollListener);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });

        if (refresh == 1){
            swipeRefreshLayout.setEnabled(false);
        }
        else {
            swipeRefreshLayout.setEnabled(true);
        }

    }

    private void refreshContent() {

        getTournamentByStatus(token, type, id, 0, 4, true);
        offset=1;
    }


    private void getTournamentByStatus(String token, int type, int categoryid, int offset, int limit, final boolean refresh) {
        apiService = ApiUtils.getAPIService(token);
//        Locale locale = new Locale(language);
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        config.locale = locale;
//        getBaseContext().getResources().updateConfiguration(config,
//                getBaseContext().getResources().getDisplayMetrics());
        Call<TournamentResponse> call = apiService.getTournamentByStatus(token, type, categoryid, offset, 4);


        call.enqueue(new Callback<TournamentResponse>() {
            @Override
            public void onResponse(Call<TournamentResponse> call, Response<TournamentResponse> response) {
                if (response.isSuccessful()) {
                    if (refresh) {
                        inactiveAdapter.cleartItems();
                        scrollListener.resetState();
                    }
                    tournamentsEntityList = response.body().getTournaments();
                    inactiveAdapter.addTournaments(tournamentsEntityList);
//                    progressBar.setVisibility(View.GONE);


                }

                else if(response.code()==401){
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

                else {
                    Log.e(TAG, "On Response :" + response.errorBody());

                }
                swipeRefreshLayout.setRefreshing(false);
            }


            @Override
            public void onFailure(Call<TournamentResponse> call, Throwable t) {
                Log.e(TAG, "On Failure :" + t.getMessage());
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public void onClick(TournamentsEntity tournamentsEntity) {
        if (tournamentsEntity.getStatus() == 2) {
            Intent i = new Intent(getActivity(), UpcomingTournamentsDetailsActivity.class);
            i.putExtra("tour", (Parcelable) tournamentsEntity);
            i.putExtra("key", 0);
            i.putExtra("token", token);
            i.putExtra("category_id", tournamentsEntity.getCategory_ID());
            startActivityForResult(i, ACTIVITY_REQUEST_CODE_TOURNAMENT);
        } else {
            Intent i = new Intent(getActivity(), UpcomingTournamentsDetailsActivity.class);
            i.putExtra("tour", (Parcelable) tournamentsEntity);
            i.putExtra("token", token);
            i.putExtra("key", 1);
            i.putExtra("category_id", tournamentsEntity.getCategory_ID());
            startActivity(i);
        }


//        ((Activity) getActivity()).overridePendingTransition(0, 0);

    }

    @Override
    public void OnLongItemClick(TournamentsEntity tournamentsEntity, View view) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTIVITY_REQUEST_CODE_TOURNAMENT) {
            if (resultCode == RESULT_OK) {
                int tournamentID = data.getIntExtra(UpcomingTournamentsDetailsActivity.TOURNAMENT_ID, 0);
                inactiveAdapter.deleteItem(tournamentID);

            }
        }
    }
//
//    @Override
//    public void OnLongItemClick(final TournamentsEntity tournamentsEntity, View view) {
//        int type =   SharedPrefUtils.getIntegerPreference(getContext(),MainActivity.USER_TYPE,0);
//
//        if(type != 0) {
//            final PopupMenu menu = new PopupMenu(getContext(), view);
//            menu.getMenuInflater().inflate(R.menu.delete_menu, menu.getMenu());
//            menu.show();
//
//            menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                @Override
//                public boolean onMenuItemClick(MenuItem item) {
//                    deleteTournament(tournamentsEntity.getID());
//                    return true;
//                }
//            });
//        }
//    }

//
//    public void deleteTournament(final int tourId){
//
//        apiService = ApiUtils.getAPIService(token);
//
//        apiService.deleteTournament(tourId).enqueue(new Callback<String>() {
//            public static final String TAG = "";
//
//            @Override
//            public void onResponse(Call<String> call, Response<String> response) {
//                if (response.isSuccessful()) {
//
//                    inactiveAdapter.deleteItem(tourId);
//                    Toast.makeText(getContext(), response.body().toString(), Toast.LENGTH_SHORT).show();
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<String> call, Throwable t) {
//                Toast.makeText(getContext(), "An error has occured", Toast.LENGTH_SHORT).show();
//                Log.e(TAG, "Unable to submit post to API.");
//            }
//        });
//
//    }


    public void createDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Confirm Delete...");
        alertDialog.setMessage("Are you sure you want delete this?");

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }

        });
        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        alertDialog.show();
    }

    public static Fragment newInstance(int type, int id, List<TournamentsEntity> tournamentsEntityList) {
        FragmentOne fragmentOne = new FragmentOne();
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_TYPE, type);
        bundle.putInt(EXTRA_ID, id);
        bundle.putParcelableArrayList(EXTRA_DATA, (ArrayList<? extends Parcelable>) tournamentsEntityList);
        fragmentOne.setArguments(bundle);
        return fragmentOne;
    }

    public static Fragment newInstances(int type,int refreshContent, List<TournamentsEntity> tournamentsEntityList) {
        FragmentOne fragmentOne = new FragmentOne();
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_TYPE, type);
        bundle.putInt(EXTRA_REFRESH, refreshContent);
        bundle.putParcelableArrayList(EXTRA_DATA, (ArrayList<? extends Parcelable>) tournamentsEntityList);
        fragmentOne.setArguments(bundle);
        return fragmentOne;
    }


}
