package com.example.desk3.tournaments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by Desk 3 on 6/19/2018.
 */


public class BracketsFragment extends Fragment implements ViewPager.OnPageChangeListener {

    private WrapContentHeightViewPager viewPager;
    private BracketsSectionAdapter sectionAdapter;
    private ArrayList<ColomnData> sectionList;
    private int mNextSelectedScreen;
    private int mCurrentPagerState;
    private ApiService apiService;
    private String ttoken;
    private TournamentsEntity tournamentsEntity;
    private int tid;
    private TextView message;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_brackts, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        message = getView().findViewById(R.id.no_content_text);
        sectionList = new ArrayList<>();
        initViews();
        intialiseViewPagerAdapter();
        ttoken =SharedPrefUtils.getStringPreference(getContext(), VerificationPageActivity.TOKEN);
        tournamentsEntity = (TournamentsEntity) getArguments().get("tournois");
        tid = tournamentsEntity.getID();
        getTournamentByID(ttoken, tid);




    }


    public void getTournamentByID(String ttoken, int tid) {
        apiService = ApiUtils.getAPIService(ttoken);
//        Locale locale = new Locale(language);
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        config.locale = locale;
//        getBaseContext().getResources().updateConfiguration(config,
//                getBaseContext().getResources().getDisplayMetrics());
        Call<TournamentDetails> call = apiService.getTournamentCompetitorsByID(tid);

        call.enqueue(new Callback<TournamentDetails>() {
            @Override
            public void onResponse(Call<TournamentDetails> call, Response<TournamentDetails> response) {

                if (response.isSuccessful()) {

                    LinkedHashMap<Integer, ArrayList<MatchData>> map = new LinkedHashMap<>();

                    for (TournamentDetails.CompetitorsEntity competitorsEntity : response.body().getData().getCompetitors())

                        addNewMatch(map, competitorsEntity);
                    sectionAdapter.addItems(flattenMap(map));

                    if (map.isEmpty()){
//                        message.setText("Sorry! Error loading tournament details");
                    }




                }

//                else if(response.code()==401){
//                    Intent intent = new Intent(getContext(), LoginActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                }

                else {
                    Log.e(TAG, "On Response :" + response.errorBody());
//                    message.setText("Sorry! Error loading tournament details");
//                    message.setVisibility(View.VISIBLE);


                }
            }


            @Override
            public void onFailure(Call<TournamentDetails> call, Throwable t) {
                Log.e(TAG, "On Failure :" + t.getMessage());
            }
        });

    }

    public void RefreshContent(boolean update){
      getTournamentByID(ttoken,tid);
    }

    private List<ColomnData> flattenMap(LinkedHashMap<Integer, ArrayList<MatchData>> map) {
        List<ColomnData> colomnData = new ArrayList<>();
        for (Map.Entry<Integer, ArrayList<MatchData>> entry : map.entrySet()) {
            colomnData.add(new ColomnData(entry.getValue()));
        }
        return colomnData;
    }

    private void addNewMatch(LinkedHashMap<Integer, ArrayList<MatchData>> map, TournamentDetails.CompetitorsEntity competitorsEntity) {
         if (map.containsKey(competitorsEntity.getRoundNumber())) {
            map.get(competitorsEntity.getRoundNumber()).add(TournamentDetails.convertToMatchData(competitorsEntity));
        } else {
            ArrayList<MatchData> matchData = new ArrayList<>();
            matchData.add(TournamentDetails.convertToMatchData(competitorsEntity));
            map.put(competitorsEntity.getRoundNumber(), matchData);
        }

    }


    private void intialiseViewPagerAdapter() {

        sectionAdapter = new BracketsSectionAdapter(getChildFragmentManager(), this.sectionList);
        viewPager.setOffscreenPageLimit(10);
        viewPager.setAdapter(sectionAdapter);
        viewPager.setCurrentItem(0);
        viewPager.setPageMargin(-200);
        viewPager.setHorizontalFadingEdgeEnabled(true);
        viewPager.setFadingEdgeLength(50);

        viewPager.addOnPageChangeListener(this);
    }

    private void initViews() {

        viewPager = (WrapContentHeightViewPager) getView().findViewById(R.id.container);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        if (mCurrentPagerState != ViewPager.SCROLL_STATE_SETTLING) {
            // We are moving to next screen on right side
            if (positionOffset > 0.5) {
                // Closer to next screen than to current
                if (position + 1 != mNextSelectedScreen) {
                    mNextSelectedScreen = position + 1;
                    //update view here
                    if (getBracketsFragment(position).getColomnList().get(0).getHeight()
                            != BracketsUtility.dpToPx(131))
                        getBracketsFragment(position).shrinkView(BracketsUtility.dpToPx(131));
                    if (getBracketsFragment(position + 1).getColomnList().get(0).getHeight()
                            != BracketsUtility.dpToPx(131))
                        getBracketsFragment(position + 1).shrinkView(BracketsUtility.dpToPx(131));
                }
            } else {
                // Closer to current screen than to next
                if (position != mNextSelectedScreen) {
                    mNextSelectedScreen = position;
                    //updateViewhere

                    if (getBracketsFragment(position + 1).getCurrentBracketSize() ==
                            getBracketsFragment(position + 1).getPreviousBracketSize()) {
                        getBracketsFragment(position + 1).shrinkView(BracketsUtility.dpToPx(131));
                        getBracketsFragment(position).shrinkView(BracketsUtility.dpToPx(131));
                    } else {
                        int currentFragmentSize = getBracketsFragment(position + 1).getCurrentBracketSize();
                        int previousFragmentSize = getBracketsFragment(position + 1).getPreviousBracketSize();
                        if (currentFragmentSize != previousFragmentSize) {
                            getBracketsFragment(position + 1).expandHeight(BracketsUtility.dpToPx(262));
                            getBracketsFragment(position).shrinkView(BracketsUtility.dpToPx(131));
                        }
                    }
                }
            }
        } else {
            // We are moving to next screen left side
            if (positionOffset > 0.5) {
                // Closer to current screen than to next
                if (position + 1 != mNextSelectedScreen) {
                    mNextSelectedScreen = position + 1;
                    //update view for screen

                }
            } else {
                // Closer to next screen than to current
                if (position != mNextSelectedScreen) {
                    mNextSelectedScreen = position;
                    //updateviewfor screem
                }
            }
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public BracketsColomnFragment getBracketsFragment(int position) {
        BracketsColomnFragment bracktsFrgmnt = null;
        if (getChildFragmentManager() != null) {
            List<Fragment> fragments = getChildFragmentManager().getFragments();
            if (fragments != null) {
                for (Fragment fragment : fragments) {
                    if (fragment instanceof BracketsColomnFragment) {
                        bracktsFrgmnt = (BracketsColomnFragment) fragment;
                        if (bracktsFrgmnt.getSectionNumber() == position)
                            break;
                    }
                }
            }
        }
        return bracktsFrgmnt;
    }
}

