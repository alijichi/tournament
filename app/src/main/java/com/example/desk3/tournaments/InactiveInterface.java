package com.example.desk3.tournaments;

import android.view.View;

/**
 * Created by Desk 3 on 6/1/2018.
 */

public interface InactiveInterface {
     void onClick(TournamentsEntity tournamentsEntity);
     void OnLongItemClick(TournamentsEntity tournamentsEntity, View view);


}
