package com.example.desk3.tournaments;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desk 3 on 8/7/2018.
 */

public class PendingAdapter extends RecyclerView.Adapter<PendingAdapter.MyViewHolder> {
    private List<TournamentsEntity> tournamentsEntities;
    private PendingInterface pendingInterface;


    public PendingAdapter(PendingInterface pendingInterface) {
        tournamentsEntities = new ArrayList<>();
        this.pendingInterface = pendingInterface;
    }

    @NonNull
    @Override
    public PendingAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tournament_card, parent, false);

        return new PendingAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PendingAdapter.MyViewHolder holder, final int position) {
        holder.Pendingname.setText(tournamentsEntities.get(position).getTRN_Name());
        if(tournamentsEntities.get(position).getPrizes() == null ) {
            holder.Pendingprize.setText("-");
        }
        else if(tournamentsEntities.get(position).getPrizes().isEmpty()){
            holder.Pendingprize.setText("-");
        }
        else{
            holder.Pendingprize.setText(tournamentsEntities.get(position).getPrizes().get(0).getName().toString());
        }

        holder.Pendingcost.setText(String.valueOf(tournamentsEntities.get(position).getTRNCost()));
        holder.Pendingplayers.setText(String.valueOf(tournamentsEntities.get(position).getMaxPlayers()));
        holder.Pendingcolor.setBackgroundResource(R.color.colorPrimary);
        String date20 = tournamentsEntities.get(position).getTRN_SubsStartDate();

        holder.Pendingsubs.setText(DateUtils.convertDate(date20));

        String date22 = tournamentsEntities.get(position).getTRNEndDate();
        holder.Pendingsube.setText(DateUtils.convertDate(date22));

        String date = tournamentsEntities.get(position).getTRNStartDate();
        holder.Pendingartdate.setText(DateUtils.convertDate(date));

        String date2 = tournamentsEntities.get(position).getTRNEndDate();
        holder.Pendingenddate.setText(DateUtils.convertDate(date2));
        holder.Pendingcity.setText(tournamentsEntities.get(position).getCategoryName());
//                    ((InactiveViewHolder)holder).InActivestreet.setText(tournaments.get(position).getStreet());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pendingInterface.OnClick(tournamentsEntities.get(position));
            }
        });


        String subdate1 = tournamentsEntities.get(position).getTRN_SubsStartDate();
        String subdate11 = DateUtils.convertDate(subdate1);
        holder.Pendingsubs.setText(subdate11);
        String subdate2 = tournamentsEntities.get(position).getTRN_SubsEndDate();
        String subdate12 =DateUtils.convertDate(subdate1);
        holder.Pendingsube.setText(subdate12);

    }

    @Override
    public int getItemCount() {
        return tournamentsEntities.size();
    }





    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView Pendingname;
        private TextView Pendingartdate;
        private TextView Pendingenddate;
        private TextView Pendingcity;
        private TextView Pendingtreet;
        private TextView Pendingprize;
        private TextView Pendingcolor;
        private TextView Pendingsubs;
        private TextView Pendingsube;
        private TextView Pendingcost;
        private TextView Pendingplayers;



        public MyViewHolder(View itemView) {
            super(itemView);
            Pendingname = (TextView) itemView.findViewById(R.id.card_name);
            Pendingprize = (TextView) itemView.findViewById(R.id.card_prize);
            Pendingartdate = (TextView) itemView.findViewById(R.id.start_date_tour);
            Pendingenddate = (TextView) itemView.findViewById(R.id.end_date_tour);
            Pendingcity = (TextView) itemView.findViewById(R.id.city);
            Pendingcolor = itemView.findViewById(R.id.color_type);
            Pendingsubs = (TextView) itemView.findViewById(R.id.start_date);
            Pendingsube = itemView.findViewById(R.id.end_date);
            Pendingcost = (TextView) itemView.findViewById(R.id.cost);
            Pendingplayers = itemView.findViewById(R.id.players);

        }
    }

    public void addPending(List<TournamentsEntity> tournamentsEntityList) {
//        tournamentsEntityList.clear();
        tournamentsEntities.addAll(tournamentsEntityList);
        notifyDataSetChanged();

    }

    public void deleteItem(int id) {
        for (int i = 0; i < tournamentsEntities.size(); i++) {
            if (tournamentsEntities.get(i).getID() == id) {
                tournamentsEntities.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }
}
