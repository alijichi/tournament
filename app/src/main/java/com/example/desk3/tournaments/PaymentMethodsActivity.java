package com.example.desk3.tournaments;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class PaymentMethodsActivity extends AppCompatActivity {

    private ImageView add_card;
    private TournamentsEntity tournamentsEntity;
    private int category_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_activity);

        tournamentsEntity = getIntent().getParcelableExtra("pay_tournament");
        category_id = getIntent().getIntExtra("category_pay_id",0);



        add_card = findViewById(R.id.add_card);


        add_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PaymentMethodsActivity.this, PaymentActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
