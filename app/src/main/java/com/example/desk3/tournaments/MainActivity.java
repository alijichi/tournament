package com.example.desk3.tournaments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements CategoryInterface {
    private static final String TAG = "message";
    public static final String NAME = "NAME";
    public static final String EMAIL = "EMAIL";
    public static final String IMAGEURL = "IMAGEURL";
    private RecyclerView recyclerView;
    private CategoryAdapter categoryAdapter;
    private String token;
    private ApiService apiService;
    private ImageView user_img;
    public static final int ACTIVITY_REQUEST_CODE = 1;
    public static final String UserLogged = "isLogged";
    private List<DataEntity> cats = new ArrayList<>();
    private String returned;
    private ProgressBar progressBar;
    private List<Interest> interests = new ArrayList<>();
    private List<TournamentsEntity> tournamentsEntities = new ArrayList<>();
    private String listString = "";
    private String listNames = "";
    private ArrayList<String> listIds = new ArrayList<>();
    public static final String USER_TYPE = "user_type";
    private int userType;
    private ImageView settings;
    private ImageView appLogo;
    private RoomDatabaseCategory roomDatabaseCategory;
    private CategoryDao categoryDao;
    private boolean checked = true;

//    public static final String USER = "user";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        roomDatabaseCategory = RoomDatabaseCategory.getDatabase(this);
        categoryDao = roomDatabaseCategory.categoryDao();
        recyclerView = findViewById(R.id.category_recycler);
        user_img = findViewById(R.id.user_image);
        settings = findViewById(R.id.settings);

//        progressBar = findViewById(R.id.progressBar_main);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        user_img.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        appLogo = findViewById(R.id.logo_app);

//        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
//                AppDatabase.class, "categories").build();

//        userType = getIntent().getIntExtra(USER_TYPE, 0);

        appLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PaymentActivity.class);
                startActivity(intent);
            }
        });

        SharedPrefUtils.setBooleanPreference(MainActivity.this, UserLogged, true);
        userType = SharedPrefUtils.getIntegerPreference(this, MainActivity.USER_TYPE, 0);

        if (userType == 1) {
            settings.setVisibility(View.VISIBLE);
        } else {
            settings.setVisibility(View.GONE);
        }


        boolean isUseerLogged = SharedPrefUtils.getBooleanPreference(MainActivity.this, UserLogged, false);
        if (isUseerLogged) {
            String img = SharedPrefUtils.getStringPreference(MainActivity.this, IMAGEURL);
            Glide.with(this).load(img).into(user_img);
        } else {
            Glide.with(this).load(R.drawable.profile).into(user_img);
        }

//        SharedPreferences prefs = getSharedPreferences("save_pic", MODE_PRIVATE);
//        final String imageUSer = prefs.getString("user_photo", "No image found");
//
//        if(imageUSer.isEmpty()) {
//            Glide.with(this).load(imageUSer).into(user_img);
//        }
//        else{
//            Glide.with(this).load(R.drawable.profile).into(user_img);
//        }
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                DataEntity dataEntity =new DataEntity();
//                List<DataEntity> dataEntitiesdatabase = db.categoryDao().getAll();
//            }
//        }) .start();

        categoryAdapter = new CategoryAdapter(this);
        recyclerView.setAdapter(categoryAdapter);
        token = SharedPrefUtils.getStringPreference(MainActivity.this, VerificationPageActivity.TOKEN);

        // init();
        getCategoryFromLocal();
        getUSerProfile(token);




        user_img.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                intent.putExtra("Category_list", listNames);
                intent.putExtra("Category_list_ids", listIds);
                intent.putExtra("returned", returned);
                startActivityForResult(intent, ACTIVITY_REQUEST_CODE);
            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AdminActivity.class);
                intent.putParcelableArrayListExtra("cats", (ArrayList<? extends Parcelable>) cats);
                startActivity(intent);
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                int key;
                key = getIntent().getIntExtra("image_key", 0);
                if (key == 1) {
                    Glide.with(this).load(R.drawable.profile).into(user_img);
                } else {
                    String image = SharedPrefUtils.getStringPreference(MainActivity.this, IMAGEURL);
                    Glide.with(this).load(image).into(user_img);
                }
               /* returned = data.getStringExtra(ProfileActivity.IMAGE_RETURNED);
                Glide.with(this).load(returned).into(user_img);*/
//                SharedPreferences.Editor editor = getSharedPreferences("save_pic", MODE_PRIVATE).edit();
//                editor.putString("user_photo", returned);
//                editor.apply();
            }
        }
    }

    private void getCategoryFromLocal() {
        categoryDao.getAll().subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataEntities -> {
                    updateUi(dataEntities);
                    getData(token);
                });
    }


    private void getData(String token) {


        apiService = ApiUtils.getAPIServices(token);
        Call<CategoryContainer> call = apiService.getData(token);

        call.enqueue(new Callback<CategoryContainer>() {
            @Override
            public void onResponse(Call<CategoryContainer> call, Response<CategoryContainer> response) {
                if (response.isSuccessful()) {

                    if (response.body().getData() != null) {
                        checked = false;
                        saveToLocal(response.body().getData());
                       /* cats = response.body().getData();
                        categoryAdapter.addCategories(cats);*/
//                        progressBar.setVisibility(View.GONE);

                    }


                } else if (response.code() == 401) {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Log.e(TAG, "On Response :" + response.errorBody());

                }
            }


            @Override
            public void onFailure(Call<CategoryContainer> call, Throwable t) {
                Log.e(TAG, "On Failure :" + t.getMessage());
//                progressBar.setVisibility(View.GONE);
            }
        });

    }

    private void saveToLocal(List<DataEntity> data) {
        deleteAllFromLocalCompletable().andThen(insertToLocalCompletable(data)).andThen(categoryDao.getAll()).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataEntities -> updateUi(dataEntities));
    }

    private Completable deleteAllFromLocalCompletable() {
        return Completable.fromAction(() -> categoryDao.deleteAll()).subscribeOn(Schedulers.computation());
    }

    private Completable insertToLocalCompletable(List<DataEntity> dataEntities) {
        return Completable.fromAction(() -> categoryDao.insert(dataEntities)).subscribeOn(Schedulers.computation());
    }


    private void updateUi(List<DataEntity> dataEntities) {
        cats = dataEntities;
        if (checked){
        for (int i = 0; i < cats.size(); i++) {
            listNames += cats.get(i).getCategory_Name() + ",";
            listIds.add(String.valueOf(cats.get(i).getID()));
        }
        }
        categoryAdapter.addCategories(cats);
    }


    private void init() {

        DataEntity dataEntity1 = new DataEntity("backgammon", 1);
        cats.add(dataEntity1);
        DataEntity dataEntity2 = new DataEntity("Frenjiyeh", 2);
        cats.add(dataEntity2);
        DataEntity dataEntity3 = new DataEntity("Playing Cards", 3);
        cats.add(dataEntity3);
        DataEntity dataEntity4 = new DataEntity("PlayStation", 4);
        cats.add(dataEntity4);
        categoryAdapter.addCategories(cats);

    }

    private void getUSerProfile(String token) {
        apiService = ApiUtils.getAPIServices(token);
//        Locale locale = new Locale(language);
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        config.locale = locale;
//        getBaseContext().getResources().updateConfiguration(config,
//                getBaseContext().getResources().getDisplayMetrics());
        Call<ProfileData> call = apiService.getPrivateUserProfile(token);

        call.enqueue(new Callback<ProfileData>() {
            @Override
            public void onResponse(Call<ProfileData> call, Response<ProfileData> response) {

                if (response.isSuccessful()) {

                    SharedPrefUtils.setStringPreference(MainActivity.this, IMAGEURL, response.body().getData().getImageURl());
                    Glide.with(getApplicationContext()).load(response.body().getData().getImageURl()).into(user_img);
                    SharedPrefUtils.setStringPreference(MainActivity.this, "name", response.body().getData().getNickName());
                    SharedPrefUtils.setStringPreference(MainActivity.this, "email", response.body().getData().getEmail());

                    if (response.body().getData().getPhoneNumber() != null) {
                        SharedPrefUtils.setStringPreference(MainActivity.this, "phonenbr", response.body().getData().getPhoneNumber());
                    }
                    if (response.body().getData().getDateOfBirth() != null) {
                        SharedPrefUtils.setStringPreference(MainActivity.this, "birthday", DateUtils.formatDate("yyyy-MM-dd'T'hh:mm:ss", "dd-MM-yyyy", response.body().getData().getDateOfBirth()));
                    }
                    List<String> strings = response.body().getData().getInterestedIN();

                    if (strings != null) {
                        for (String s : response.body().getData().getInterestedIN()) {
                            listString += s + ",";

                        }
                        Log.i("message", listString);
                        SharedPrefUtils.setStringPreference(MainActivity.this, "interests", listString);
                    }


                } else {
                    Log.e(TAG, "On Response :" + response.errorBody());

                }
            }


            @Override
            public void onFailure(Call<ProfileData> call, Throwable t) {
                Log.e(TAG, "On Failure :" + t.getMessage());
                // showDialog();
            }
        });

    }


    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
//                        System.exit(0);
                    }
                }).setNegativeButton("No", null).show();
    }

    @Override
    public void OnCategoryClick(DataEntity category) {
        if (category.getFinished() != null || category.getActive() != null || category.getInActive() != null) {
//            if (userType == 2 && category.isCanBeAccessed() == true) {
//                Intent intent = new Intent(MainActivity.this, TournamentActivity.class);
//                intent.putExtra(TournamentActivity.EXTRA_CATEGORY, category);
////            intent.putExtra(TournamentActivity.TYPE_USER, userType);
//                startActivity(intent);
//            }

            if (userType == 0) {
                Intent intent = new Intent(MainActivity.this, TournamentActivity.class);
                intent.putExtra(TournamentActivity.EXTRA_CATEGORY, category);
//            intent.putExtra(TournamentActivity.TYPE_USER, userType);
                startActivity(intent);

            } else {
                Intent intent = new Intent(MainActivity.this, TournamentActivity.class);
                intent.putExtra(TournamentActivity.EXTRA_CATEGORY, category);
//            intent.putExtra(TournamentActivity.TYPE_USER, userType);
                startActivity(intent);
            }
//            } else {
//                Snackbar snackbar = Snackbar.make(recyclerView, "Sorry !! you cannot access " + category.getCategory_Name() + " category", Snackbar.LENGTH_SHORT).setAction("Action", null);
//                View sbView = snackbar.getView();
//                sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
//                snackbar.show();
//            }
        }


    }

    @Override
    public void OnInterestClick(DataEntity interest, boolean isPressed) {

    }

    public void showDialog() {

        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                .setMessage("Connection Error")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getData(token);
                    }
                }).show();
    }


//
//    public void init(){
//        Category category = new Category("backgammon",R.drawable.backgammon,1);
//        categoryAdapter.addCategory(category);
//
//        Category category1 = new Category("Franjiyeh",R.drawable.frenjiyeh,2);
//        categoryAdapter.addCategory(category1);
//
//        Category category2 = new Category("Playing cards",R.drawable.cards,3);
//        categoryAdapter.addCategory(category2);
//
//        Category category3 = new Category("Playstation",R.drawable.playstation,4);
//        categoryAdapter.addCategory(category3);
//
//    }
}

