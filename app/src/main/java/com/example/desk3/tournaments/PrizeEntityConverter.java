package com.example.desk3.tournaments;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Created by Desk 3 on 8/16/2018.
 */

public class PrizeEntityConverter {
    static Gson gson = new Gson();

    @TypeConverter
    public static List<PrizeEntity> stringToSomeObjectList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<PrizeEntity>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(List<PrizeEntity> someObjects) {
        return gson.toJson(someObjects);
    }

}
