package com.example.desk3.tournaments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Desk 3 on 7/16/2018.
 */

public class MyCustomDialogFragment extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.notification_dialog, container, false);
        getDialog().setTitle("Simple Dialog");
        return rootView;
    }

}

