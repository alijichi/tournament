package com.example.desk3.tournaments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.desk3.tournaments.LoginActivity.CONST_FIRST_TIME_LOGIN;


public class VerificationPageActivity extends AppCompatActivity {

    private Button submit;
    private ApiService apiService;
    public static final String TOKEN = "token";
    private String PIN = "";
    private String mobileNB;
    private TextView responsetv;
    private EditText pinCode;
    private ProgressBar loading;
    private LinearLayout linearLayout;
    public static final int TYPE_ORGANIZER = 2;
    public static final int TYPE_ADMIN = 1;
    public static final int TYPE_USER = 0;
    private TextView resend;
    private String deviceId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_page);


        submit = findViewById(R.id.submit);
        responsetv = findViewById(R.id.response);
        loading = findViewById(R.id.progressVerification);
        linearLayout = findViewById(R.id.verification_container);
        pinCode = findViewById(R.id.pin_code);
        resend = findViewById(R.id.resend);

        PIN = SharedPrefUtils.getStringPreference(VerificationPageActivity.this, LoginActivity.USERCODE);
        mobileNB = getIntent().getStringExtra("mobilenb");
        deviceId = getIntent().getStringExtra("deviceID");
        responsetv.setText(PIN);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(submit.getWindowToken(), 0);
                if (isValid()) {

                    linearLayout.setVisibility(View.GONE);

                    VerifyPhoneNumber(mobileNB, pinCode.getText().toString().trim());

                }

            }
        });


        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResendPIN(mobileNB);

            }
        });

    }


    public void VerifyPhoneNumber(String mobile, String PIN) {

        apiService = ApiUtils.getAPIService(null);

        Call<GenerateToken> call = apiService.VerifyPhoneNumber(mobile, PIN, "1");
        call.enqueue(new Callback<GenerateToken>() {
            public static final String TAG = "Hello";

            @Override
            public void onResponse(Call<GenerateToken> call, Response<GenerateToken> response) {

                if (response.isSuccessful()) {

                    if (getIntent().getBooleanExtra(CONST_FIRST_TIME_LOGIN, false)) {
                        SharedPrefUtils.setStringPreference(VerificationPageActivity.this, TOKEN, response.body().getToken());
                        loading.setVisibility(View.GONE);

                        if (response.body().getUserType() == TYPE_ORGANIZER) {
                            SharedPrefUtils.setStringPreference(VerificationPageActivity.this, TOKEN, response.body().getToken());
                            loading.setVisibility(View.GONE);
                            SharedPrefUtils.setIntegerPreference(VerificationPageActivity.this, MainActivity.USER_TYPE, TYPE_ORGANIZER);

                            Intent intent1 = new Intent(VerificationPageActivity.this, MainActivity.class);
                            startActivity(intent1);
                            finish();

                        }

                        else if(response.body().getUserType() == TYPE_ADMIN){

                            SharedPrefUtils.setStringPreference(VerificationPageActivity.this, TOKEN, response.body().getToken());
                            loading.setVisibility(View.GONE);
                            SharedPrefUtils.setIntegerPreference(VerificationPageActivity.this, MainActivity.USER_TYPE, TYPE_ADMIN);
                            Intent intent1 = new Intent(VerificationPageActivity.this, MainActivity.class);
                            startActivity(intent1);
                            finish();
                        }

                        else {
                            SharedPrefUtils.setIntegerPreference(VerificationPageActivity.this, MainActivity.USER_TYPE, TYPE_USER);
                            Intent intent = new Intent(VerificationPageActivity.this, MainActivity.class);
//                        intent.putExtra(MainActivity.USER_TYPE, TYPE_ORGANIZER);
                            startActivity(intent);
                            finish();
                        }
                    } else if (response.body().getUserType() == TYPE_USER) {
                        SharedPrefUtils.setStringPreference(VerificationPageActivity.this, TOKEN, response.body().getToken());
                        loading.setVisibility(View.GONE);
                        SharedPrefUtils.setIntegerPreference(VerificationPageActivity.this, MainActivity.USER_TYPE, TYPE_USER);
                        Intent intent = new Intent(VerificationPageActivity.this, ProfileActivity.class);
//                        intent.putExtra(MainActivity.USER_TYPE, TYPE_USER);
                        intent.putExtra(CONST_FIRST_TIME_LOGIN, true);
                        startActivity(intent);
                        finish();

                    }

                    Log.i(TAG, "Your item added successful" + response.body().getToken().toString());
                } else {
                    Toast.makeText(getBaseContext(), "Invalid code", Toast.LENGTH_SHORT).show();
                    linearLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<GenerateToken> call, Throwable t) {
                Log.e(TAG, "Wrong PIN code");

                showDialog();
//                loading.setVisibility(View.GONE);
                linearLayout.setVisibility(View.VISIBLE);
            }
        });


    }

    public void ResendPIN(final String mobile) {
        apiService = ApiUtils.getAPIService(null);

        Call<CodeResponse> call = apiService.GetVerificationCode(mobile, "1",deviceId,"android");
        call.enqueue(new Callback<CodeResponse>() {
            public static final String TAG = "No problem";

            @Override
            public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {

                if (response.isSuccessful()) {
                    responsetv.setText(response.body().getCode().toString());
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<CodeResponse> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    private boolean isValid() {
        boolean valid = true;
        if (pinCode.getText().toString().trim().isEmpty()) {
            valid = false;
            pinCode.setError("Invalid Code");
        } else {

        }

//        else if (interests.isEmpty()){
//
//            valid =false;
//            Toast.makeText(this,"Please choose your interests",Toast.LENGTH_SHORT);
//
//        }

        return valid;

    }


    public void showDialog() {

        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                .setMessage("Connection Error")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }
}

