package com.example.desk3.tournaments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TournamentActivity extends AppCompatActivity {
    private static final int CREATE_TOURNAMENT_CODE = 111;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private DataEntity category;
    private  HashMap<Integer,List<TournamentsEntity>> map;
    private List<TournamentsEntity> inactiveTournaments = new ArrayList<>();
    private List<TournamentsEntity> finishedTournaments = new ArrayList<>();
    private List<TournamentsEntity> activeTournaments = new ArrayList<>();
//    public static final String TYPE_USER = "type_user";
    public static final String CATEGORY_ID = "CATEGORY_ID";
    private TextView title;

    public static final String EXTRA_CATEGORY = "extra_category";
    private Toolbar toolbar;
    private String name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tournament);
        map = new HashMap<>();
        category = getIntent().getParcelableExtra(EXTRA_CATEGORY);
//

//        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
//        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            viewPager = (ViewPager) findViewById(R.id.viewpager);
            setupViewPager(viewPager);
            viewPager.setOffscreenPageLimit(3);

            tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.isSmoothScrollingEnabled();
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);





            // preparing list data
//            prepareListData();


//        tabLayout.setTabTextColors(
//                ContextCompat.getColor(AddItemsActivity.this,R.color.colorPrimary),
//                ContextCompat.getColor(AddItemsActivity.this,R.color.colorAccent)
//        );


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Open Online Chat", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


        }

        @Override
        protected void onResume() {
            super.onResume();
        }


//
//
//    void prepareListData() {
//    }

    private void setupViewPager(ViewPager viewPager) {


        name =category.getCategory_Name();
        getSupportActionBar().setTitle(name);
//        title.setText(name);

        finishedTournaments = category.getFinished();
        inactiveTournaments = category.getInActive();
        activeTournaments = category.getActive();


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

//        for(TournamentsEntity tournamentsEntity : tournamentsEntities1){
//            if(map.containsKey(tournamentsEntity.getStatus())){
//                map.get(tournamentsEntity.getStatus()).add(tournamentsEntity);
//            }
//            else {
//                List<TournamentsEntity> tournamentsEntities = new ArrayList<>();
//                tournamentsEntities.add(tournamentsEntity);
//                map.put(tournamentsEntity.getStatus(),  tournamentsEntities);
//
//            }

//       }

        adapter.addFragment(FragmentOne.newInstance(2,category.getID(),inactiveTournaments), "Upcoming");
        adapter.addFragment(FragmentOne.newInstance(1,category.getID(),activeTournaments), "Live");
        adapter.addFragment(FragmentOne.newInstance(0,category.getID(),finishedTournaments), "Finished");
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
      int type =   SharedPrefUtils.getIntegerPreference(this,MainActivity.USER_TYPE,0);

        if(type != 0)
        getMenuInflater().inflate(R.menu.add_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeAdapter/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if(id== R.id.action_add){
            int catID = category.getID();
            Intent i = new Intent(TournamentActivity.this, CreateTournamentInfoActivity.class);
            i.putExtra(CATEGORY_ID,catID);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}


