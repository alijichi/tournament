package com.example.desk3.tournaments;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Shader;
import android.util.AttributeSet;

import com.hanks.htextview.base.AnimationListener;
import com.hanks.htextview.base.DisplayUtils;
import com.hanks.htextview.base.HTextView;

/**
 * Created by Desk 3 on 8/13/2018.
 */

    public class RainbowView extends HTextView {

        private Matrix mMatrix;
        private float mTranslate;
        private float colorSpeed;
        private float colorSpace;
        private int[] colors = {  0xFF5CD918, 0xFF22F4FF, 0xFF288EC1, 0xFF5400F7};
        private LinearGradient mLinearGradient;

        public RainbowView(Context context) {
            this(context, null);
        }

        public RainbowView(Context context, AttributeSet attrs) {
            this(context, attrs, 0);
        }

        public RainbowView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            init(attrs, defStyleAttr);
        }

        @Override
        public void setAnimationListener(AnimationListener listener) {
            throw new UnsupportedOperationException("Invalid operation for rainbow");
        }

        private void init(AttributeSet attrs, int defStyleAttr) {

            TypedArray typedArray = getContext().obtainStyledAttributes(attrs, com.hanks.htextview.rainbow.R.styleable.RainbowTextView);
            colorSpace = typedArray.getDimension(com.hanks.htextview.rainbow.R.styleable.RainbowTextView_colorSpace, DisplayUtils.dp2px(150));
            colorSpeed = typedArray.getDimension(com.hanks.htextview.rainbow.R.styleable.RainbowTextView_colorSpeed, DisplayUtils.dp2px(5));
            typedArray.recycle();

            mMatrix = new Matrix();
            initPaint();
        }

        public float getColorSpace() {
            return colorSpace;
        }

        public void setColorSpace(float colorSpace) {
            this.colorSpace = colorSpace;
        }

        public float getColorSpeed() {
            return colorSpeed;
        }

        public void setColorSpeed(float colorSpeed) {
            this.colorSpeed = colorSpeed;
        }

        public void setColors(int... colors) {
            this.colors = colors;
            initPaint();
        }

        private void initPaint() {
            mLinearGradient = new LinearGradient(0, 0, colorSpace, 0, colors, null, Shader.TileMode.MIRROR);
            getPaint().setShader(mLinearGradient);
        }

        @Override
        public void setProgress(float progress) {
        }

        @Override
        public void animateText(CharSequence text) {
            setText(text);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            if (mMatrix == null) {
                mMatrix = new Matrix();
            }
            mTranslate += colorSpeed;
            mMatrix.setTranslate(mTranslate, 0);
            mLinearGradient.setLocalMatrix(mMatrix);
            super.onDraw(canvas);
            postInvalidateDelayed(100);
        }

    }
