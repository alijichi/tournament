package com.example.desk3.tournaments;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Created by Desk 3 on 8/16/2018.
 */

public class TournamentDateConverter {
    static Gson gson = new Gson();

    @TypeConverter
    public static List<TournamentDate> stringToSomeObjectList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<TournamentDate>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(List<TournamentDate> someObjects) {
        return gson.toJson(someObjects);
    }

}
