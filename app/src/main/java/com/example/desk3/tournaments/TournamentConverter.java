package com.example.desk3.tournaments;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Created by Desk 3 on 8/16/2018.
 */

public class TournamentConverter {
    static Gson gson = new Gson();

    @TypeConverter
    public static List<TournamentsEntity> stringToSomeObjectList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<TournamentsEntity>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(List<TournamentsEntity> someObjects) {
        return gson.toJson(someObjects);
    }

}
