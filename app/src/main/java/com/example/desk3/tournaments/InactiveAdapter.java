package com.example.desk3.tournaments;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desk 3 on 6/1/2018.
 */

public class InactiveAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<TournamentsEntity> tournaments;
    private InactiveInterface inactiveInterface;
    private static final int TYPE_INACTIVE = 2;
    private static final int TYPE_ACTIVE = 1;
    private static final int TYPE_FINISHED = 0;


    public InactiveAdapter(InactiveInterface inactiveInterface) {
        this.tournaments = new ArrayList<>();
        this.inactiveInterface = inactiveInterface;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_INACTIVE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tournament_card, parent, false);
                return new InactiveViewHolder(view);

            case TYPE_ACTIVE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tournament_live_card, parent, false);
                return new ActiveViewHolder(view);

            case TYPE_FINISHED:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tournament_finished_card, parent, false);
                return new FinishedViewHolder(view);
        }
        return null;
    }


    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        TournamentsEntity tournamentsEntity = tournaments.get(position);
        if (tournamentsEntity != null) {
            switch (tournamentsEntity.getStatus()) {

                case 2:
                    ((InactiveViewHolder) holder).InActivename.setText(tournaments.get(position).getTRN_Name());
                    if(tournamentsEntity.getPrizes() == null ) {
                        ((InactiveViewHolder) holder).InActiveprize.setText("-");
                    }
                    else if(tournamentsEntity.getPrizes().isEmpty()){
                        ((InactiveViewHolder) holder).InActiveprize.setText("-");
                    }
                        else{
                        ((InactiveViewHolder) holder).InActiveprize.setText(tournamentsEntity.getPrizes().get(0).getName().toString());
                    }

                    ((InactiveViewHolder) holder).InActivecost.setText(String.valueOf(tournaments.get(position).getTRNCost()));
                    ((InactiveViewHolder) holder).InActiveplayers.setText(String.valueOf(tournaments.get(position).getNumberOfPlayers()));
                    ((InactiveViewHolder) holder).InActivecolor.setBackgroundResource(R.color.colorPrimary);
                    String date20 = tournaments.get(position).getTRN_SubsStartDate();

                    ((InactiveViewHolder) holder).InActivesubs.setText(DateUtils.convertDate(date20));

                    String date22 = tournaments.get(position).getTRNEndDate();
                    ((InactiveViewHolder) holder).InActivesube.setText(DateUtils.convertDate(date22));

                    String date = tournaments.get(position).getTRNStartDate();
                    ((InactiveViewHolder) holder).InActivestartdate.setText(DateUtils.convertDate(date));

                    String date2 = tournaments.get(position).getTRNEndDate();
                    ((InactiveViewHolder) holder).InActiveenddate.setText(DateUtils.convertDate(date2));
                    ((InactiveViewHolder) holder).InActivecity.setText(tournaments.get(position).getCity());
//                    ((InactiveViewHolder)holder).InActivestreet.setText(tournaments.get(position).getStreet());
                    ((InactiveViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            inactiveInterface.onClick(tournaments.get(position));
                        }
                    });

                    ((InactiveViewHolder) holder).itemView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            inactiveInterface.OnLongItemClick(tournaments.get(position),view);
                            return true;
                        }
                    });


                    String subdate1 = tournaments.get(position).getTRN_SubsStartDate();
                    String subdate11 = DateUtils.convertDate(subdate1);
                    ((InactiveViewHolder) holder).InActivesubs.setText(subdate11);
                    String subdate2 = tournaments.get(position).getTRN_SubsEndDate();
                    String subdate12 =DateUtils.convertDate(subdate1);
                    ((InactiveViewHolder) holder).InActivesube.setText(subdate12);


                    break;

                case 1:
                    ((ActiveViewHolder) holder).Activename.setText(tournaments.get(position).getTRN_Name());



                    if(tournamentsEntity.getPrizes() == null ) {
                        ((ActiveViewHolder) holder).Activeprize.setText("-");
                    }
                    else if(tournamentsEntity.getPrizes().isEmpty()){
                        ((ActiveViewHolder) holder).Activeprize.setText("-");
                    }
                    else{
                        ((ActiveViewHolder) holder).Activeprize.setText(tournamentsEntity.getPrizes().get(0).getName().toString());
                    }



                    ((ActiveViewHolder) holder).Activeplayers.setText(String.valueOf(tournaments.get(position).getNumberOfPlayers()));
                    ((ActiveViewHolder) holder).Activecost.setText(String.valueOf(tournaments.get(position).getTRNCost()));

                    ((ActiveViewHolder) holder).Activemessage.setText("Live");
                    ((ActiveViewHolder) holder).Activecolor.setBackgroundResource(R.color.green);
                    String date3 = tournaments.get(position).getTRNStartDate();
                    ((ActiveViewHolder) holder).Activestartdate.setText(DateUtils.convertDate(date3));

                    String date4 = tournaments.get(position).getTRNEndDate();
                    ((ActiveViewHolder) holder).Activeenddate.setText(DateUtils.convertDate(date4));
                    ((ActiveViewHolder) holder).Activecity.setText(tournaments.get(position).getCity());
//                    ((ActiveViewHolder)holder).Activestreet.setText(tournaments.get(position).getStreet());
                    ((ActiveViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            inactiveInterface.onClick(tournaments.get(position));
                        }
                    });
//                    String subdate3 = tournaments.get(position).getTRN_SubsStartDate();
//                    String subdate13 = convertDate(subdate3);
//                    ((ActiveViewHolder)holder).Activesubs.setText(subdate13);
//                    String subdate4 = tournaments.get(position).getTRN_SubsEndDate();
//                    String subdate14 = convertDate(subdate4);
//                    ((ActiveViewHolder)holder).Activesube.setText(subdate14);

                    break;

                case 0:
                    ((FinishedViewHolder) holder).Finishedname.setText(tournaments.get(position).getTRN_Name());
                    ((FinishedViewHolder)holder).Finishedname.setTextColor(R.color.grey);


                    if(tournamentsEntity.getPrizes() == null ) {
                        ((FinishedViewHolder) holder).Finishedprize.setText("-");
                    }
                    else if(tournamentsEntity.getPrizes().isEmpty()){
                        ((FinishedViewHolder) holder).Finishedprize.setText("-");
                    }
                    else{
                        ((FinishedViewHolder) holder).Finishedprize.setText(tournamentsEntity.getPrizes().get(0).getName().toString());
                    }
//                    ((FinishedViewHolder)holder).Finishedprize.setTextColor(R.color.grey);
                    ((FinishedViewHolder) holder).Finishedcost.setText(String.valueOf(tournaments.get(position).getTRNCost()));
                    ((FinishedViewHolder) holder).Finishedplayers.setText(String.valueOf(tournaments.get(position).getNumberOfPlayers()));
                    ((FinishedViewHolder) holder).Finishedmessage.setText("Winner is "+ tournaments.get(position).getWinnerName());
//                    ((FinishedViewHolder)holder).Finishedmessage.setTextColor(R.color.blue);
                    ((FinishedViewHolder) holder).FinishedColor.setBackgroundResource(R.color.violet);
                    String date5 = tournaments.get(position).getTRNStartDate();
                    ((FinishedViewHolder) holder).Finishedstartdate.setText(DateUtils.convertDate(date5));

                    String date6 = tournaments.get(position).getTRNEndDate();
                    ((FinishedViewHolder) holder).Finishedenddate.setText(DateUtils.convertDate(date6));
                    String dateInString1 = tournaments.get(position).getTRNEndDate();
                    ((FinishedViewHolder) holder).Finishedcity.setText(tournaments.get(position).getCity());
//                    ((FinishedViewHolder)holder).Finishedstreet.setText(tournaments.get(position).getStreet());
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            inactiveInterface.onClick(tournaments.get(position));
                        }
                    });

//                    String subdate5 = tournaments.get(position).getTRN_SubsStartDate();
//                    String subdate15 = convertDate(subdate5);
//                    ((FinishedViewHolder)holder).Finishedsubs.setText(subdate15);
//                    String subdate6 = tournaments.get(position).getTRN_SubsEndDate();
//                    String subdate16 = convertDate(subdate6);
//                    ((FinishedViewHolder)holder).Finishedsube.setText(subdate16);


            }
        }


    }

    @Override
    public int getItemViewType(int position) {
        int type = tournaments.get(position).getStatus();
        switch (type) {
            case 2:
                return 2;
            case 1:
                return 1;
            case 0:
                return 0;
        }
        return super.getItemViewType(position);
    }





    public class InactiveViewHolder extends RecyclerView.ViewHolder {
        private TextView InActivename;
        private TextView InActivestartdate;
        private TextView InActiveenddate;
        private TextView InActivecity;
        private TextView InActivestreet;
        private TextView InActiveprize;
        private TextView InActivecolor;
        private TextView InActivesubs;
        private TextView InActivesube;
        private TextView InActivecost;
        private TextView InActiveplayers;


        public InactiveViewHolder(View itemView) {
            super(itemView);
            InActivename = (TextView) itemView.findViewById(R.id.card_name);
            InActiveprize = (TextView) itemView.findViewById(R.id.card_prize);
            InActivestartdate = (TextView) itemView.findViewById(R.id.start_date_tour);
            InActiveenddate = (TextView) itemView.findViewById(R.id.end_date_tour);
            InActivecity = (TextView) itemView.findViewById(R.id.city);
            InActivecolor = itemView.findViewById(R.id.color_type);
            InActivesubs = (TextView) itemView.findViewById(R.id.start_date);
            InActivesube = itemView.findViewById(R.id.end_date);
            InActivecost = (TextView) itemView.findViewById(R.id.cost);
            InActiveplayers = itemView.findViewById(R.id.players);
//            InActivestreet = (TextView) itemView.findViewById(R.id.street);

        }
    }

    public class ActiveViewHolder extends RecyclerView.ViewHolder {
        private TextView Activename;
        private TextView Activestartdate;
        private TextView Activeenddate;
        private TextView Activecity;
        private TextView Activestreet;
        private TextView Activecountry;
        private TextView Activeprize;
        private TextView Activecolor;
        private TextView Activesubs;
        private TextView Activesube;
        private TextView Activecost;
        private TextView Activeplayers;
        private TextView Activemessage;


        public ActiveViewHolder(View itemView) {
            super(itemView);
            Activename = (TextView) itemView.findViewById(R.id.card_name12);
            Activeprize = (TextView) itemView.findViewById(R.id.card_prize1);
            Activestartdate = (TextView) itemView.findViewById(R.id.start_date_tour1);
            Activeenddate = (TextView) itemView.findViewById(R.id.end_date_tour1);
            Activecity = (TextView) itemView.findViewById(R.id.city1);
            Activecolor = itemView.findViewById(R.id.color_type1);
//            Activesubs = (TextView) itemView.findViewById(R.id.start_date_tour1);
//            Activesube = itemView.findViewById(R.id.end_date_tour1);
//            Activestreet = (TextView) itemView.findViewById(R.id.street);
            Activecost = (TextView) itemView.findViewById(R.id.cost1);
            Activeplayers = itemView.findViewById(R.id.players1);
            Activemessage = itemView.findViewById(R.id.message1);

        }
    }

    public class FinishedViewHolder extends RecyclerView.ViewHolder {
        private TextView Finishedname;
        private TextView Finishedstartdate;
        private TextView Finishedenddate;
        private TextView Finishedcity;
        private TextView Finishedstreet;
        private TextView Finishedcountry;
        private TextView Finishedprize;
        private TextView FinishedColor;
        private TextView Finishedsubs;
        private TextView Finishedsube;
        private TextView Finishedcost;
        private TextView Finishedplayers;
        private TextView Finishedmessage;



        public FinishedViewHolder(View itemView) {
            super(itemView);
            Finishedname = (TextView) itemView.findViewById(R.id.card_name2);
            Finishedprize = (TextView) itemView.findViewById(R.id.card_prize2);
            Finishedstartdate = (TextView) itemView.findViewById(R.id.start_date_tour2);
            Finishedenddate = (TextView) itemView.findViewById(R.id.end_date_tour2);
            Finishedcity = (TextView) itemView.findViewById(R.id.city2);
//            Finishedstreet = (TextView) itemView.findViewById(R.id.street);
            FinishedColor = itemView.findViewById(R.id.color_type2);
//            Finishedsubs = (TextView) itemView.findViewById(R.id.start_date_tour1);
//            Finishedsube = itemView.findViewById(R.id.end_date_tour1);
            Finishedcost = (TextView) itemView.findViewById(R.id.cost2);
            Finishedplayers = itemView.findViewById(R.id.players2);
            Finishedmessage = itemView.findViewById(R.id.message2);


        }
    }


    @Override
    public int getItemCount() {
        return tournaments.size();
    }

    public void addTournaments(List<TournamentsEntity> tournamentsEntities) {
        //tournaments.clear();
        tournaments.addAll(tournamentsEntities);
        notifyItemRangeChanged(0,tournamentsEntities.size());

    }

    public void addTour(List<TournamentsEntity> entities) {
        tournaments.clear();
        for (TournamentsEntity tournamentsEntity : entities)
            tournaments.add(tournamentsEntity);
        notifyDataSetChanged();
    }


    public void cleartItems() {
        tournaments.clear();
        notifyDataSetChanged();
    }


    public void deleteItem(int id){
        for(int i =0; i< tournaments.size(); i++) {
            if(tournaments.get(i).getID() == id){
                tournaments.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }


    public void addHistories(List<TournamentsEntity> tournamentsEntityList) {
//        tournamentsEntityList.clear();
        tournaments.addAll(tournamentsEntityList);
        notifyDataSetChanged();

    }

    }
