package com.example.desk3.tournaments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Desk 3 on 7/16/2018.
 */

public class Notification implements Parcelable {

    @Expose
    @SerializedName("Readed")
    private boolean Readed;
    @Expose
    @SerializedName("isBroadcast")
    private boolean isBroadcast;
    @Expose
    @SerializedName("CreatedDate")
    private String CreatedDate;
    @Expose
    @SerializedName("TRN_ID")
    private int TRN_ID;
    @Expose
    @SerializedName("Category_ID")
    private int Category_ID;
    @Expose
    @SerializedName("User_ID")
    private int User_ID;
    @Expose
    @SerializedName("Type")
    private String Type;
    @Expose
    @SerializedName("Text")
    private String Text;
    @Expose
    @SerializedName("ID")
    private int ID;

    public boolean getReaded() {
        return Readed;
    }

    public void setReaded(boolean Readed) {
        this.Readed = Readed;
    }

    public boolean getIsBroadcast() {
        return isBroadcast;
    }

    public void setIsBroadcast(boolean isBroadcast) {
        this.isBroadcast = isBroadcast;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    public int getTRN_ID() {
        return TRN_ID;
    }

    public void setTRN_ID(int TRN_ID) {
        this.TRN_ID = TRN_ID;
    }

    public int getCategory_ID() {
        return Category_ID;
    }

    public void setCategory_ID(int Category_ID) {
        this.Category_ID = Category_ID;
    }

    public int getUser_ID() {
        return User_ID;
    }

    public void setUser_ID(int User_ID) {
        this.User_ID = User_ID;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getText() {
        return Text;
    }

    public void setText(String Text) {
        this.Text = Text;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.Readed ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isBroadcast ? (byte) 1 : (byte) 0);
        dest.writeString(this.CreatedDate);
        dest.writeInt(this.TRN_ID);
        dest.writeInt(this.Category_ID);
        dest.writeInt(this.User_ID);
        dest.writeString(this.Type);
        dest.writeString(this.Text);
        dest.writeInt(this.ID);
    }

    public Notification() {
    }

    protected Notification(Parcel in) {
        this.Readed = in.readByte() != 0;
        this.isBroadcast = in.readByte() != 0;
        this.CreatedDate = in.readString();
        this.TRN_ID = in.readInt();
        this.Category_ID = in.readInt();
        this.User_ID = in.readInt();
        this.Type = in.readString();
        this.Text = in.readString();
        this.ID = in.readInt();
    }

    public static final Parcelable.Creator<Notification> CREATOR = new Parcelable.Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel source) {
            return new Notification(source);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };
}
