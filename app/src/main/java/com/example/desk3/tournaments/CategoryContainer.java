package com.example.desk3.tournaments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desk 3 on 5/29/2018.
 */

public class CategoryContainer implements Parcelable {


    @Expose
    @SerializedName("Data")
    private List<DataEntity> Data;

    public List<DataEntity> getData() {
        return Data;
    }

    public void setData(List<DataEntity> Data) {
        this.Data = Data;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.Data);
    }

    public CategoryContainer() {
    }

    protected CategoryContainer(Parcel in) {
        this.Data = new ArrayList<DataEntity>();
        in.readList(this.Data, DataEntity.class.getClassLoader());
    }

    public static final Creator<CategoryContainer> CREATOR = new Creator<CategoryContainer>() {
        @Override
        public CategoryContainer createFromParcel(Parcel source) {
            return new CategoryContainer(source);
        }

        @Override
        public CategoryContainer[] newArray(int size) {
            return new CategoryContainer[size];
        }
    };
}
