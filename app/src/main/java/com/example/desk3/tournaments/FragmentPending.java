package com.example.desk3.tournaments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by Desk 3 on 8/7/2018.
 */

public class FragmentPending extends Fragment implements PendingInterface {
    private RecyclerView recyclerView;
    private PendingAdapter pendingAdapter;
    private List<TournamentsEntity> tournamentsEntities = new ArrayList<>();
    private String token;
    private ApiService apiService;
    private int offset =0;
    private static final int ACTIVITY_REQUEST_CODE_TOURNAMENT_REQUEST = 3;
    private int limit;
    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pending, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        recyclerView = view.findViewById(R.id.pending_recycler);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        token = SharedPrefUtils.getStringPreference(getContext(), VerificationPageActivity.TOKEN);
        pendingAdapter = new PendingAdapter(this);
        getUSerTours(offset,limit);
        recyclerView.setAdapter(pendingAdapter);


        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                offset += 1;
                getUSerTours(offset, limit);

            }
        };
        // Adds the scroll listener to RecyclerView
        recyclerView.addOnScrollListener(scrollListener);

    }

    private void getUSerTours(int offset, int limit) {
        apiService = ApiUtils.getAPIService(token);

        Call<TournamentResponse> call = apiService.getUserTournaments(true, offset, 4);


        call.enqueue(new Callback<TournamentResponse>() {
            @Override
            public void onResponse(Call<TournamentResponse> call, Response<TournamentResponse> response) {
                if (response.isSuccessful()) {

                    tournamentsEntities = response.body().getTournaments();
                    pendingAdapter.addPending(tournamentsEntities);


                } else if (response.code() == 401) {
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Log.e(TAG, "On Response :" + response.errorBody());

                }
            }


            @Override
            public void onFailure(Call<TournamentResponse> call, Throwable t) {
                Log.e(TAG, "On Failure :" + t.getMessage());
            }
        });
    }

    @Override
    public void OnClick(TournamentsEntity tournamentsEntity) {
        Intent intent = new Intent(getContext(), UpcomingTournamentsDetailsActivity.class);
        intent.putExtra("tour", tournamentsEntity);
        intent.putExtra("key", 3);
        startActivity(intent);

    }
}
