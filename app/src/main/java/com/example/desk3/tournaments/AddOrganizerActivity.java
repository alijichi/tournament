package com.example.desk3.tournaments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddOrganizerActivity extends AppCompatActivity implements SelectionInterface {
    private TextView categoriesSelection;
    private RecyclerView recyclerView;
    private SelectionAdapter selectionAdapter;
    private ApiService apiService;
    private String token;
    private EditText organizerName;
    private EditText organizerPhoneNb;
    private EditText organizerEmail;
    private ProgressBar progressBar;
    private List<DataEntity> dataEntityList = new ArrayList<>();
    private List<Integer> categoriesId = new ArrayList<>();
    private Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_organizer);
        categoriesSelection = findViewById(R.id.categories_selection);
        recyclerView = findViewById(R.id.recycler_selection);
        organizerName = findViewById(R.id.organizer_name);
        organizerPhoneNb = findViewById(R.id.organizer_phonenb);
        progressBar = findViewById(R.id.progress_organizer);
        save = findViewById(R.id.save_changes_organizer);
        organizerEmail = findViewById(R.id.organizer_email);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dataEntityList = getIntent().getParcelableArrayListExtra("cats");

        token = SharedPrefUtils.getStringPreference(AddOrganizerActivity.this, VerificationPageActivity.TOKEN);
        selectionAdapter = new SelectionAdapter(this, getBaseContext());
//        getCategoriesSelection(token);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        selectionAdapter.addCategories(dataEntityList);
        recyclerView.setAdapter(selectionAdapter);

        categoriesSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (recyclerView.getVisibility() == View.GONE) {
                    recyclerView.setVisibility(View.VISIBLE);
                } else
                    recyclerView.setVisibility(View.GONE);
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validate()) {
                    if (!categoriesId.isEmpty() && !categoriesId.equals("") && categoriesId != null ) {
                        progressBar.setVisibility(View.VISIBLE);
                        addOrganizer(organizerName.getText().toString(), organizerPhoneNb.getText().toString(), organizerEmail.getText().toString(), categoriesId);
                    }
                    else{
                        Toast.makeText(getBaseContext(),"Please select at least one category", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(getBaseContext(),"Please enter a valid data", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

//    private void getCategoriesSelection(String token) {
//        apiService = ApiUtils.getAPIServices(token);
//        Call<CategoryContainer> call = apiService.getData(token);
//
//        call.enqueue(new Callback<CategoryContainer>() {
//            public static final String TAG ="" ;
//
//            @Override
//            public void onResponse(Call<CategoryContainer> call, Response<CategoryContainer> response) {
//                if (response.isSuccessful()) {
//
//                    if (response.body().getData() != null) {
//
//
//                        dataEntityList = response.body().getData();
//                        selectionAdapter.addCategories(dataEntityList);
//
//                    }
//
//
//                } else {
//                    Log.e(TAG, "On Response :" + response.errorBody());
//
//                }
//            }
//
//
//            @Override
//            public void onFailure(Call<CategoryContainer> call, Throwable t) {
//                Log.e(TAG, "On Failure :" + t.getMessage());
//            }
//        });
//
//    }


    public void addOrganizer(String name, String phoneNb, String email, List<Integer> categoriesId) {
        apiService = ApiUtils.getAPIService(token);
        HashMap<String, Object> map = new HashMap<>();
        HashMap<String, Object> userProfileMap = new HashMap<>();
        userProfileMap.put("PhoneNumber", phoneNb);
        userProfileMap.put("Name", name);
        userProfileMap.put("Email", email);
        map.put("UserProfile", userProfileMap);
        HashMap<String, List<Integer>> ListMap = new HashMap<>();
        ListMap.put("categoryIDs", categoriesId);
        map.putAll(ListMap);
        apiService.RegisterOrganizer(map).enqueue(new Callback<String>() {
            public static final String TAG = "";

            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {


                    progressBar.setVisibility(View.GONE);
                    Intent intent = new Intent();
                    intent.putExtra("organizer_add", true);
                    setResult(RESULT_OK, intent);
                    finish();

                }

                else if(response.code()==401){
                    Intent intent = new Intent(AddOrganizerActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }else {
                    Toast.makeText(AddOrganizerActivity.this, "An error has occurred", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
                Toast.makeText(AddOrganizerActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeAdapter/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        } else {

            return super.onOptionsItemSelected(item);
        }
    }


    public boolean validate() {
        boolean valid = true;

        String organizeranme = organizerName.getText().toString();
        String organizerphone = organizerPhoneNb.getText().toString();


        if (organizeranme == null || organizeranme.isEmpty()) {
            organizerName.setError("enter a valid name");
            valid = false;
        }

        if (organizerphone == null || organizerphone.isEmpty() || (organizerphone.length() < 7 || organizerphone.length() > 8)) {
            organizerPhoneNb.setError("enter a valid phone number");
            valid = false;
        }
        return valid;
    }


    @Override
    public void onClick(DataEntity dataEntity, boolean pressed) {
        if (pressed) {
            categoriesId.add(dataEntity.getID());
        } else {
            for (int i = 0; i < categoriesId.size(); i++) {
                if (categoriesId.get(i) == dataEntity.getID()) {
                    categoriesId.remove(i);
                    break;
                }
            }
        }
    }
}
