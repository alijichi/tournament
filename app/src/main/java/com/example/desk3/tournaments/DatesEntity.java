package com.example.desk3.tournaments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Desk 3 on 8/10/2018.
 */

public class DatesEntity implements Parcelable {

    @Expose
    @SerializedName("ID")
    private int ID;
    @Expose
    @SerializedName("Trn_ID")
    private int Trn_ID;
    @Expose
    @SerializedName("TourDate")
    private String TourDate;


    public DatesEntity(int ID, int trn_ID, String tourDate) {
        this.ID = ID;
        Trn_ID = trn_ID;
        TourDate = tourDate;
    }

    protected DatesEntity(Parcel in) {
        ID = in.readInt();
        Trn_ID = in.readInt();
        TourDate = in.readString();
    }

    public static final Creator<DatesEntity> CREATOR = new Creator<DatesEntity>() {
        @Override
        public DatesEntity createFromParcel(Parcel in) {
            return new DatesEntity(in);
        }

        @Override
        public DatesEntity[] newArray(int size) {
            return new DatesEntity[size];
        }
    };

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getTrn_ID() {
        return Trn_ID;
    }

    public void setTrn_ID(int trn_ID) {
        Trn_ID = trn_ID;
    }

    public String getTourDate() {
        return TourDate;
    }

    public void setTourDate(String tourDate) {
        TourDate = tourDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ID);
        parcel.writeInt(Trn_ID);
        parcel.writeString(TourDate);
    }
}
