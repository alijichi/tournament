package com.example.desk3.tournaments;

import android.app.Application;
import android.content.ComponentCallbacks2;
import android.support.multidex.MultiDexApplication;

import com.google.firebase.messaging.FirebaseMessaging;

/**
 * Created by Desk 3 on 7/27/2018.
 */

public class ApplicationClass extends MultiDexApplication{
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseMessaging.getInstance().subscribeToTopic("topic_id");
    }
}
