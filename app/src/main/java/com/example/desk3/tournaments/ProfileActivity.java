package com.example.desk3.tournaments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.PorterDuffXfermode;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessaging;
import com.nex3z.flowlayout.FlowLayout;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.desk3.tournaments.LoginActivity.CONST_FIRST_TIME_LOGIN;

public class ProfileActivity extends AppCompatActivity implements CategoryInterface {
    private EditText et_name, birth;
    private Button submit;
    private ApiService apiService;
    private ImageView next;
    private ImageView add_img;
    private ImageView profile_image;
    private ArrayList<String> interests = new ArrayList<>();
    private static final int REQUEST_GALLERY_CODE = 200;
    final String TAG = ProfileActivity.class.getSimpleName();
    private String token;
    private DatePickerDialog datePickerDialog;
    private  int type;
    private EditText et_email;
    private String nickname;
    private String user_email;
    private Uri uri;
    private FlowLayout mFlowLayout;
    private InterestsAdapter interestsAdapter;
    private RecyclerView recyclerView;
    private List<DataEntity> categories;
    private String img;
    private ProgressBar progressBar;
    private String[] splited = null;
    private String selectedInterest = "";
    private String allInterest;
    private FrameLayout frameLayout;
    private TextView showInterests;
    public static final String IMAGE_RETURNED = "image_returned";
    private ArrayList<String> allInterestIds = new ArrayList<>();
    private EditText phoneNB;
    // String [] names = {"goods", "shops", "cars", "washing machine","blablabla","clothes","books"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        et_name = findViewById(R.id.username);
        submit = (Button) findViewById(R.id.submit);
        et_email = findViewById(R.id.email);
//        next = findViewById(R.id.next);
        add_img = findViewById(R.id.add_image);
        profile_image = findViewById(R.id.profile_image);
        progressBar = findViewById(R.id.progress);
        frameLayout = findViewById(R.id.image_frame);
        birth = findViewById(R.id.birth_date);
        phoneNB = findViewById(R.id.phone_nb);
        showInterests = findViewById(R.id.show_interests);


//        Toolbar toolbar = (Toolbar) findViewById(R.id.materialup_toolbar);
//        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        type = SharedPrefUtils.getIntegerPreference(ProfileActivity.this, MainActivity.USER_TYPE, 0);
        String name1 = SharedPrefUtils.getStringPreference(ProfileActivity.this, "name");
        String email1 = SharedPrefUtils.getStringPreference(ProfileActivity.this, "email");
        String imgurl1 = SharedPrefUtils.getStringPreference(ProfileActivity.this, "imgurl");
        String birthday = SharedPrefUtils.getStringPreference(ProfileActivity.this, "birthday");
        String phones = SharedPrefUtils.getStringPreference(ProfileActivity.this, "phonenbr");


        et_name.setText(name1);
        et_email.setText(email1);
        birth.setText(birthday);
        phoneNB.setText(phones);
        Glide.with(this).load(imgurl1).into(profile_image);

//        hideSoftKeyboard();

//        img = getIntent().getStringExtra("returned");
//        Glide.with(this).load(img).into(profile_image);

        interestsAdapter = new InterestsAdapter(this, this);

//        allInterest = SharedPrefUtils.getStringPreference(ProfileActivity.this, "list_string");


        allInterest = getIntent().getStringExtra("Category_list");
        allInterestIds = (ArrayList<String>) getIntent().getSerializableExtra("Category_list_ids");
        selectedInterest = SharedPrefUtils.getStringPreference(ProfileActivity.this, "interests");
        Log.i("new", selectedInterest);
        String img = SharedPrefUtils.getStringPreference(ProfileActivity.this, MainActivity.IMAGEURL);
        Glide.with(this).load(img).into(profile_image);


//        SharedPreferences prefs = getSharedPreferences("save", MODE_PRIVATE);
//        String name2 = prefs.getString("name", "");
//        String email2 = prefs.getString("email", "");
//        String interest = prefs.getString("interests", "");
//        String name2 = SharedPrefUtils.getStringPreference(ProfileActivity.this, "name");
//        String email2 = SharedPrefUtils.getStringPreference(ProfileActivity.this, "email");
//        et_name.setText(name2);
//        et_email.setText(email2);

        //    splited = interest.split(",");
        if (allInterest != null && allInterest.contains(",")) {
            List<Interest> interests = new ArrayList<>();
//            if (allInterest.contains(",")) {
            String[] arrayOfInterests = allInterest.split(",");
            for (int i = 0; i < arrayOfInterests.length; i++)
                interests.add(new Interest(arrayOfInterests[i], false, allInterestIds.get(i) == null ? "-1" : allInterestIds.get(i)));
//            } else {
//                interests.add(new Interest(allInterest, true));
            interestsAdapter.addInterests(interests);
        }

        if (selectedInterest != null) {

            List<Interest> interests1 = new ArrayList<>();
            if (selectedInterest.contains(",")) {
                String[] arrayOfInterestsStrings = selectedInterest.split(",");
                for (int i = 0; i < arrayOfInterestsStrings.length; i++)
                    interests1.add(new Interest(arrayOfInterestsStrings[i], true, ""));
            } else
                interests1.add(new Interest(selectedInterest, true, ""));
            interestsAdapter.updateInterests(interests1);
        }
        // List<String> stringList = new ArrayList<String>(Arrays.asList(splited));
//            List<TournamentDetails.DataEntity> objectList = new ArrayList<TournamentDetails.DataEntity>(stringList);


        // categories = (List<DataEntity>) getIntent().getSerializableExtra("Category_list");
//
//        TextView view = getLayoutInflater().inflate(R.layout.item_interest, null);
//        FlowLayout.LayoutParams p = new FlowLayout.LayoutParams(300, 150);
//        p.newLine = true;
//        view.setLayoutParams(p);


        recyclerView = (RecyclerView) findViewById(R.id.interests_recycler);
        int numberOfColumns = 3;
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));

//        List<Interest> categoryInterests = new ArrayList<>();
//        for (DataEntity dataEntity : categories)
//            categoryInterests.add(new Interest(dataEntity.getCategory_Name(), false));
//        interestsAdapter.addInterests(categoryInterests);
        recyclerView.setAdapter(interestsAdapter);


        token = SharedPrefUtils.getStringPreference(ProfileActivity.this, VerificationPageActivity.TOKEN);
        apiService = ApiUtils.getAPIService(token);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    progressBar.setVisibility(View.VISIBLE);
                    updateProfile(uri, et_name.getText().toString(), birth.getText().toString(),phoneNB.getText().toString(), et_email.getText().toString());
                } else {
                    Snackbar.make(view, "Please enter a valid data", Snackbar.LENGTH_LONG)
                            .setAction("CLOSE", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                }
                            })
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                            .show();
                }
            }
        });


        if (type == 1){
            recyclerView.setVisibility(View.GONE);
            submit.setVisibility(View.GONE);
            showInterests.setVisibility(View.GONE);
            et_name.setFocusable(false);
            et_name.setEnabled(false);
            et_email.setFocusable(false);
            et_email.setEnabled(false);
            phoneNB.setFocusable(false);
            phoneNB.setEnabled(false);
            birth.setFocusable(false);
            birth.setEnabled(false);
        }
        else if (type ==2){
            recyclerView.setVisibility(View.GONE);
            showInterests.setVisibility(View.GONE);
        }


        birth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            showDatePicker(new onBirthDateSelectedInterface() {
                @Override
                public void onBirthDateSet(String s) {
                    birth.setText(s);
                }
            });
            }
        });

//        next.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(ProfileActivity.this, PaymentActivity.class);
//                startActivity(intent);
//            }
//        });

        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage();
            }
        });

        if (!getIntent().getBooleanExtra(CONST_FIRST_TIME_LOGIN, false))
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        else
            getInterests(SharedPrefUtils.getStringPreference(this, VerificationPageActivity.TOKEN));
    }



    private boolean isValid() {
        boolean valid = true;
        if (et_name.getText().toString().trim().isEmpty()) {
            valid = false;
            et_name.setError(getString(R.string.error_field_required));
        } else if (!isEmailValid(et_email.getText().toString())) {
            valid = false;
            et_email.setError("invalid email");
        }
//        else if (interests.isEmpty()){
//
//            valid =false;
//            Toast.makeText(this,"Please choose your interests",Toast.LENGTH_SHORT);
//
//        }

        return valid;
    }


    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void pickImage() {
        /*Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
        openGalleryIntent.setType("image*//*");
        startActivityForResult(openGalleryIntent,REQUEST_GALLERY_CODE);*/
        CropImage.startPickImageActivity(this);
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.logout_menu, menu);
        return true;
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                finishActivity();
                return true;
            case R.id.action_logout:
                showDialog();
                return true;

            case R.id.action_history:
                Intent intent = new Intent(ProfileActivity.this, HistoryActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                uri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {
                // no permissions required or already granted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                uri = result.getUri();
                Glide.with(this).load(uri).into(profile_image);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (uri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                startCropImageActivity(uri);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setAspectRatio(46, 46)
                .start(this);
    }


    private void updateProfile(final Uri uri, String username,String birthDate,String phoneNumber, String email) {
        String interests = interestsAdapter.getInterests();
        if (uri == null) {
            sendPostWithoutImage(username, email, birthDate,phoneNumber, interests);
        } else {
//            sendPostWithoutImage(username, email, interests);
            RequestBody requestFile;
            MultipartBody.Part body = null;
            File file = new File(uri.getPath());
            requestFile =
                    RequestBody.create(
                            MediaType.parse("image/jpeg"),
                            file
                    );

            body =
                    MultipartBody.Part.createFormData("", file.getName(), requestFile);


//            RequestBody userNameBody =
//                    RequestBody.create(
//                            okhttp3.MultipartBody.FORM, username);
//            RequestBody userEmailBody =
//                    RequestBody.create(
//                            okhttp3.MultipartBody.FORM, email);
//
//            RequestBody interestsBody =
//                    RequestBody.create(
//                            okhttp3.MultipartBody.FORM, interests);

            sendPostWithImage(body, username, email,birthDate,phoneNumber, interests);

        }

    }


//    }


    public void sendPostWithoutImage(String username, String email, String birthDate, String phone, final String interests) {

        apiService.updateProfileWithoutImage(username, email,birthDate,phone, interests).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

//                    SharedPreferences.Editor editor = getSharedPreferences("save", MODE_PRIVATE).edit();
//                    editor.putString("name", et_name.getText().toString());
//                    editor.putString("email", et_email.getText().toString());

                    SharedPrefUtils.setStringPreference(ProfileActivity.this, "name", username);
                    SharedPrefUtils.setStringPreference(ProfileActivity.this, "email",email);
                    SharedPrefUtils.setStringPreference(ProfileActivity.this, "interests", interests);
                    SharedPrefUtils.setStringPreference(ProfileActivity.this, "birthday", birthDate);
                    SharedPrefUtils.setStringPreference(ProfileActivity.this, "phonenbr", phone);
//                    editor.apply();
                    progressBar.setVisibility(View.GONE);
                    finishActivity();
//

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    public void sendPostWithImage(MultipartBody.Part body, final String username, final String email,String birthDate,String phoneNb, final String interests) {

        apiService.updateProfile(body).enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                if (response.isSuccessful()) {
//                    nickname = response.body().getNickName();
//                    nickname = nickname.replace("\"", "");
//                    user_email = response.body().getEmail();
//                    user_email = user_email.replace("\"", "");

                    String imageURl = response.body().getImageURl();
//                    profile_image.setI
// mageResource(Integer.parseInt(imageURl));

                    SharedPrefUtils.setStringPreference(ProfileActivity.this, MainActivity.IMAGEURL, imageURl);
//                    editor.putString("name", nickname);
//                    editor.putString("email", user_email);
//                    editor.putString("imageresponse", String.valueOf(imageURl));
//                    editor.apply();
//                    Log.i(TAG, "post submitted to API." + response.body().toString());

                    progressBar.setVisibility(View.GONE);

                    sendPostWithoutImage(username, email,birthDate, phoneNb, interests);
                } else {
                    Toast.makeText(ProfileActivity.this, "An error has occurred", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
                Toast.makeText(ProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void showDialog() {

        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Logout")
                .setMessage("Are you sure you want to leave this page ?")
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       /* SharedPrefUtils.removeFromPrefs(ProfileActivity.this, "imgurl");
                        SharedPrefUtils.removeFromPrefs(ProfileActivity.this, "interests");
                        SharedPrefUtils.removeFromPrefs(ProfileActivity.this, "name");
                        SharedPrefUtils.removeFromPrefs(ProfileActivity.this, "email");*/
                        LogOut();
                        SharedPrefUtils.removeAll(ProfileActivity.this);


                        Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    }
                }).show();
    }


    @Override
    public void OnCategoryClick(DataEntity category) {

    }

    @Override
    public void OnInterestClick(DataEntity interest, boolean isPressed) {


    }


    public void LogOut() {
        apiService = ApiUtils.getAPIService(token);
        apiService.logOut().enqueue(new Callback<String>() {
            public static final String TAG = "";

            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    for (int i = 0; i< selectedInterest.length(); i ++) {
                        FirebaseMessaging.getInstance().unsubscribeFromTopic("topic_" + i);
                    }
                    Toast.makeText(ProfileActivity.this, "you logged out", Toast.LENGTH_SHORT).show();
                }
                else if(response.code()==401){
                    Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(ProfileActivity.this, "An error has occurred", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
                Toast.makeText(ProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    public void showDatePicker(final ProfileActivity.onBirthDateSelectedInterface onDateSelectedInterface) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, i);
                calendar.set(Calendar.MONTH, i1);
                calendar.set(Calendar.DAY_OF_MONTH, i2);
                onDateSelectedInterface.onBirthDateSet(DateUtils.convertDate(calendar.getTimeInMillis(), "dd-MM-yyyy"));


            }
        }, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private interface onBirthDateSelectedInterface {
        void onBirthDateSet(String s);
    }
    private void finishActivity() {


        if (getIntent().getBooleanExtra(CONST_FIRST_TIME_LOGIN, false)) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Intent intent = new Intent();
            intent.putExtra("image_key",1);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void getInterests(String token) {
        apiService = ApiUtils.getAPIServices(token);
        Call<CategoryContainer> call = apiService.getData(token);

        call.enqueue(new Callback<CategoryContainer>() {
            @Override
            public void onResponse(Call<CategoryContainer> call, Response<CategoryContainer> response) {
                if (response.isSuccessful()) {
                    List<Interest> interests = new ArrayList<>();
                    if (response.body().getData() != null) {
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            interests.add(new Interest(response.body().getData().get(i).getCategory_Name(), false, String.valueOf(response.body().getData().get(i).getID())));
                        }
                        interestsAdapter.addInterests(interests);
                    }


                } else {
                    Log.e(TAG, "On Response :" + response.errorBody());

                }
            }


            @Override
            public void onFailure(Call<CategoryContainer> call, Throwable t) {
                Log.e(TAG, "On Failure :" + t.getMessage());
            }
        });

    }
}

