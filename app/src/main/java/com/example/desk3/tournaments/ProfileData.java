package com.example.desk3.tournaments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Desk 3 on 6/26/2018.
 */

public class ProfileData {


    @Expose
    @SerializedName("Data")
    private DataEntity Data;

    public DataEntity getData() {
        return Data;
    }

    public void setData(DataEntity Data) {
        this.Data = Data;
    }

    public static class DataEntity implements Parcelable {
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("IsAdmin")
        private int IsAdmin;
        @Expose
        @SerializedName("ImageURl")
        private String ImageURl;
        @Expose
        @SerializedName("NickName")
        private String NickName;
        @Expose
        @SerializedName("Date")
        private String Date;
        @Expose
        @SerializedName("VersionID")
        private int VersionID;
        @Expose
        @SerializedName("Code")
        private int Code;
        @Expose
        @SerializedName("PhoneNumber")
        private String PhoneNumber;
        @Expose
        @SerializedName("ID")
        private int ID;
        @Expose
        @SerializedName("InterestedIN")
        private List<String> InterestedIN;

        @Expose
        @SerializedName("DateOfBirth")
        private String DateOfBirth;

        @Expose
        @SerializedName("numberOfWins")
        private int numberOfWins;

        @Expose
        @SerializedName("ActiveTour")
        private List<TournamentsEntity> Active;

        @Expose
        @SerializedName("InActive")
        private List<TournamentsEntity> InActive;

        @Expose
        @SerializedName("Finished")
        private List<TournamentsEntity> Finished;

        @Expose
        @SerializedName("Pending")
        private List<TournamentsEntity> Pending;


        @Expose
        @SerializedName("numberOFSubscriptions")
        private int numberOFSubscriptions;


        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public int getIsAdmin() {
            return IsAdmin;
        }

        public void setIsAdmin(int IsAdmin) {
            this.IsAdmin = IsAdmin;
        }

        public String getImageURl() {
            return ImageURl;
        }

        public void setImageURl(String ImageURl) {
            this.ImageURl = ImageURl;
        }

        public String getNickName() {
            return NickName;
        }

        public void setNickName(String NickName) {
            this.NickName = NickName;
        }

        public String getDate() {
            return Date;
        }

        public void setDate(String Date) {
            this.Date = Date;
        }

        public int getVersionID() {
            return VersionID;
        }

        public void setVersionID(int VersionID) {
            this.VersionID = VersionID;
        }


        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String PhoneNumber) {
            this.PhoneNumber = PhoneNumber;
        }

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public List<String> getInterestedIN() {
            return InterestedIN;
        }

        public void setInterestedIN(List<String> interestedIN) {
            InterestedIN = interestedIN;
        }

        public String getDateOfBirth() {
            return DateOfBirth;
        }

        public void setDateOfBirth(String dateOfBirth) {
            DateOfBirth = dateOfBirth;
        }

        public int getNumberOfWins() {
            return numberOfWins;
        }

        public void setNumberOfWins(int numberOfWins) {
            this.numberOfWins = numberOfWins;
        }

        public int getNumberOFSubscriptions() {
            return numberOFSubscriptions;
        }

        public void setNumberOFSubscriptions(int numberOFSubscriptions) {
            this.numberOFSubscriptions = numberOFSubscriptions;
        }

        public List<TournamentsEntity> getActive() {
            return Active;
        }

        public void setActive(List<TournamentsEntity> active) {
            Active = active;
        }

        public List<TournamentsEntity> getInActive() {
            return InActive;
        }

        public void setInActive(List<TournamentsEntity> inActive) {
            InActive = inActive;
        }

        public List<TournamentsEntity> getFinished() {
            return Finished;
        }

        public void setFinished(List<TournamentsEntity> finished) {
            Finished = finished;
        }

        public List<TournamentsEntity> getPending() {
            return Pending;
        }

        public void setPending(List<TournamentsEntity> pending) {
            Pending = pending;
        }

        public static Creator<DataEntity> getCREATOR() {
            return CREATOR;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.Email);
            dest.writeInt(this.IsAdmin);
            dest.writeString(this.ImageURl);
            dest.writeString(this.NickName);
            dest.writeString(this.Date);
            dest.writeInt(this.VersionID);
            dest.writeInt(this.Code);
            dest.writeString(this.PhoneNumber);
            dest.writeInt(this.ID);
            dest.writeStringList(this.InterestedIN);
            dest.writeString(this.DateOfBirth);
            dest.writeInt(this.numberOfWins);
            dest.writeTypedList(this.Active);
            dest.writeTypedList(this.InActive);
            dest.writeTypedList(this.Finished);
            dest.writeTypedList(this.Pending);
            dest.writeInt(this.numberOFSubscriptions);
        }

        public DataEntity() {
        }

        protected DataEntity(Parcel in) {
            this.Email = in.readString();
            this.IsAdmin = in.readInt();
            this.ImageURl = in.readString();
            this.NickName = in.readString();
            this.Date = in.readString();
            this.VersionID = in.readInt();
            this.Code = in.readInt();
            this.PhoneNumber = in.readString();
            this.ID = in.readInt();
            this.InterestedIN = in.createStringArrayList();
            this.DateOfBirth = in.readString();
            this.numberOfWins = in.readInt();
            this.Active = in.createTypedArrayList(TournamentsEntity.CREATOR);
            this.InActive = in.createTypedArrayList(TournamentsEntity.CREATOR);
            this.Finished = in.createTypedArrayList(TournamentsEntity.CREATOR);
            this.Pending = in.createTypedArrayList(TournamentsEntity.CREATOR);
            this.numberOFSubscriptions = in.readInt();
        }

        public static final Creator<DataEntity> CREATOR = new Creator<DataEntity>() {
            @Override
            public DataEntity createFromParcel(Parcel source) {
                return new DataEntity(source);
            }

            @Override
            public DataEntity[] newArray(int size) {
                return new DataEntity[size];
            }
        };
    }
}
