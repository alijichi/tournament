package com.example.desk3.tournaments;

import android.view.View;

/**
 * Created by Desk 3 on 7/16/2018.
 */

public interface NotificationInterface {
    void onClick(Notification notification);
}
