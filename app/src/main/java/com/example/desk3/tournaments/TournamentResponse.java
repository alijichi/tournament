package com.example.desk3.tournaments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Desk 3 on 6/4/2018.
 */

public class TournamentResponse {
    @Expose
    @SerializedName("Data")
    private List<TournamentsEntity> Tournaments;

    public List<TournamentsEntity> getTournaments() {
        return Tournaments;
    }

    public void setTournaments(List<TournamentsEntity> tournaments) {
        Tournaments = tournaments;
    }



}
