package com.example.desk3.tournaments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PreviewFragment.PreviewFragmentListener} interface
 * to handle interaction events.
 */
public class PreviewFragment extends Fragment  implements ListAdapter.ListAdapterInterface{

    private PreviewFragmentListener mListener;
    private InfoInterface infoInterface;
    private TextView txt_dummy;
    private TextView startdate;
    private TextView enddate;
    private TextView description;
    private TextView startsubdate;
    private TextView endsubdate;
    private TextView card_location;
    private TextView cost;
    private TextView players;
    private TextView country;
    private TextView city;
    private TextView street;
    private TextView name;
    private ImageView mapview;
    private Button next;
    private TextView previous;
    private ImageView imageView;
    private ImageView calendar_dates;
    private  TextView organizerr_name;
    private List<PrizeEntity> prizeEntities;
    private RecyclerView recyclerView;
    private ListAdapter listAdapter;
    private TextView lblListHeader;
    private List<TournamentDate> tournamentDates ;
    private TextView createdStart, createdEnd;
    private ProgressBar progressBar;
    private RequestOptions requestOptions;

    public PreviewFragment()  {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_preview, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txt_dummy = view.findViewById(R.id.txt_dummy);
        startdate = view.findViewById(R.id.create_startdate);
        enddate = view.findViewById(R.id.create_enddate);
        description = view.findViewById(R.id.create_desc);
        startsubdate = view.findViewById(R.id.create_start_sub_date);
        endsubdate = view.findViewById(R.id.create_end_sub_date);
        players = view.findViewById(R.id.create_playersnb);
        cost = view.findViewById(R.id.create_costamount);
        card_location = view.findViewById(R.id.create_card_location);
        country = view.findViewById(R.id.create_country);
        city = view.findViewById(R.id.create_city);
        street = view.findViewById(R.id.create_street);
        mapview = view.findViewById(R.id.create_map);
        next = view.findViewById(R.id.done_create);
        previous = view.findViewById(R.id.previous_preview);
        imageView = view.findViewById(R.id.create_image);
        calendar_dates = view.findViewById(R.id.create_calendar_dates);
        organizerr_name = view.findViewById(R.id.creates_organizer_name_tournament);
        recyclerView = view.findViewById(R.id.recyclerView_list_created);
        lblListHeader = view.findViewById(R.id.lblListHeader_created);
        createdStart = view.findViewById(R.id.created_starthourtime);
        createdEnd = view.findViewById(R.id.created_endhourtime);
        progressBar = view.findViewById(R.id.create_progress);

        requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));

        listAdapter = new ListAdapter(this, getContext());
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);



        lblListHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                lblListHeader.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (recyclerView.getVisibility() == View.GONE) {
                            recyclerView.setVisibility(View.VISIBLE);
                        } else
                            recyclerView.setVisibility(View.GONE);

                    }
                });
            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(getContext())
                        .title("Confirmation message")
                        .content("Are you sure you want to create this tournament ?")
                        .positiveText("yes")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                progressBar.setVisibility(View.VISIBLE);
                                mListener.OnClick();
                            }
                        })
                        .negativeText("No")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                            }
                        }).show();
            }
        });



        organizerr_name.setText("Tournament created by you");


        calendar_dates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CalendarActivity.class);
                intent.putParcelableArrayListExtra("tournament_dates_created", (ArrayList<? extends Parcelable>) tournamentDates);
                intent.putExtra("key_created", 1);
                startActivity(intent);
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mListener.showPreviousTitle("select location");
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PreviewFragmentListener) {
            mListener = (PreviewFragmentListener) context;
        } else if (context instanceof InfoInterface) {
            infoInterface = (InfoInterface) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(PrizeEntity prizeEntity, View view) {

    }

    @Override
    public void onError() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface PreviewFragmentListener {
         void showPreviousTitle(String title);

        void OnClick();
    }

    public void update(HashMap<String, Object> map) {
//        for (Map.Entry<String, Object> map1 : map.entrySet()) {
//            if (map1.getKey().equals("dates")) {





            if (map.containsKey("dates")){
                tournamentDates = new ArrayList<>();
                tournamentDates = (List<TournamentDate>) map.get("dates");
            }


                if (map.containsKey("players")) {
                    players.setText(map.get("players").toString());
                }
                else{
                    players.setText("");
                }
                if (map.containsKey("cost")) {
                    cost.setText(map.get("cost").toString());
                }
                else{
                    cost.setText("");
                }

                if (map.containsKey("description")) {
                    description.setText(map.get("description").toString());
                }
                else{
                    description.setText("");
                }

                if (map.containsKey("startDate")) {
                    startdate.setText(DateUtils.formatDate("yyyy-MM-dd'T'hh:mm:ss","dd-MM-yyyy",map.get("startDate").toString()));
                }
                else{
                    startdate.setText("");
                }

                if (map.containsKey("endDate")) {
                    enddate.setText(DateUtils.formatDate("yyyy-MM-dd'T'hh:mm:ss","dd-MM-yyyy",map.get("endDate").toString()));
                }
                else{
                    enddate.setText("");
                }

                if (map.containsKey("startSubDate")) {
                    startsubdate.setText(DateUtils.formatDate("yyyy-MM-dd'T'hh:mm:ss","dd-MM-yyyy",map.get("startSubDate").toString()));
                }
                else{
                    startsubdate.setText("");
                }

                if (map.containsKey("endSubDate")) {
                    endsubdate.setText(DateUtils.formatDate("yyyy-MM-dd'T'hh:mm:ss","dd-MM-yyyy",map.get("endSubDate").toString()));
                }
                else{
                    endsubdate.setText("");
                }

                    country.setText("Lebanon");

                if (map.containsKey("city")) {
                    city.setText(map.get("city").toString());
                }
                else{
                    city.setText("");
                }

                if (map.containsKey("street")) {
                    street.setText(map.get("street").toString());
                }
                else{
                    street.setText("");
                }




        if (map.containsKey("startHours")) {
            if (Integer.parseInt(map.get("startHours").toString()) >= 0 && Integer.parseInt(map.get("startHours").toString()) < 12) {
                createdStart.setText(map.get("startHours").toString() + ":00 AM");
            } else {
                createdStart.setText(map.get("startHours").toString() + ":00 PM");
            }
        }

        else {
            createdStart.setText("");
        }

        if (map.containsKey("endHours")) {
            if (Integer.parseInt(map.get("endHours").toString()) >= 12 && Integer.parseInt(map.get("endHours").toString()) <= 23) {
                createdEnd.setText(map.get("endHours").toString() + ":00 PM");
            } else {
                createdEnd.setText(map.get("endHours").toString() + ":00 AM");
            }
        }

        else {
            createdEnd.setText("");
        }

                if (map.containsKey("placeNames")) {
                    card_location.setText(map.get("placeNames").toString());
                }
                else{
                    card_location.setText("");
                }

            if (map.containsKey("imgUrl") && map.get("imgUrl") != null) {
                Glide.with(getContext()).load(map.get("imgUrl")).apply(requestOptions).into(imageView);
            }
            else{
                Glide.with(getContext()).load(R.drawable.tour).apply(requestOptions).into(imageView);
            }

            if (map.containsKey("prizes")) {
                prizeEntities = new ArrayList<>();
                prizeEntities.addAll((Collection<? extends PrizeEntity>) map.get("prizes"));
               listAdapter.addData(prizeEntities);
                recyclerView.setAdapter(listAdapter);

            }
            else{
            }


                if (map.containsKey("mapimage")) {
                    Glide.with(getContext()).load(map.get("mapimage")).apply(requestOptions).into(mapview);

                }
                else{
                   Glide.with(getContext()).load(R.drawable.rounded_cat).apply(requestOptions).into(mapview);
                }
//                                for (int i = 0; i < ((List<String>) map1.getValue()).size(); i++) {
//                    txt_dummy.append(map1.getKey() + "" + i + " :  " + ((List<String>) map1.getValue()).get(i) + "\n");
//                }
//            } else {
////                txt_dummy.append(map1.getKey() + " :  " + map1.getValue() + "\n");
//            }
//        }
    }
}
