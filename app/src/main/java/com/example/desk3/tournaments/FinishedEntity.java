package com.example.desk3.tournaments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FinishedEntity {
    @Expose
    @SerializedName("NumberOfPlayers")
    private int NumberOfPlayers;
    @Expose
    @SerializedName("isSubscribed")
    private boolean isSubscribed;
    @Expose
    @SerializedName("Status")
    private int Status;
    @Expose
    @SerializedName("TRNCost")
    private int TRNCost;
    @Expose
    @SerializedName("TRNEndHour")
    private int TRNEndHour;
    @Expose
    @SerializedName("TRNStartHour")
    private int TRNStartHour;
    @Expose
    @SerializedName("Description")
    private String Description;
    @Expose
    @SerializedName("TRN_SubsEndDate")
    private String TRN_SubsEndDate;
    @Expose
    @SerializedName("TRN_SubsStartDate")
    private String TRN_SubsStartDate;
    @Expose
    @SerializedName("TRNEndDate")
    private String TRNEndDate;
    @Expose
    @SerializedName("TRNStartDate")
    private String TRNStartDate;
    @Expose
    @SerializedName("Date")
    private String Date;
    @Expose
    @SerializedName("Street")
    private String Street;
    @Expose
    @SerializedName("City")
    private String City;
    @Expose
    @SerializedName("Country")
    private String Country;
    @Expose
    @SerializedName("Latitude")
    private double Latitude;
    @Expose
    @SerializedName("Longitude")
    private double Longitude;
    @Expose
    @SerializedName("Category_ID")
    private int Category_ID;
    @Expose
    @SerializedName("Admin_ID")
    private int Admin_ID;
    @Expose
    @SerializedName("TRN_Name")
    private String TRN_Name;
    @Expose
    @SerializedName("ID")
    private int ID;
    @Expose
    @SerializedName("Prizes")
    private List<String> Prizes;

    public int getNumberOfPlayers() {
        return NumberOfPlayers;
    }

    public void setNumberOfPlayers(int NumberOfPlayers) {
        this.NumberOfPlayers = NumberOfPlayers;
    }

    public boolean getIsSubscribed() {
        return isSubscribed;
    }

    public void setIsSubscribed(boolean isSubscribed) {
        this.isSubscribed = isSubscribed;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public int getTRNCost() {
        return TRNCost;
    }

    public void setTRNCost(int TRNCost) {
        this.TRNCost = TRNCost;
    }

    public int getTRNEndHour() {
        return TRNEndHour;
    }

    public void setTRNEndHour(int TRNEndHour) {
        this.TRNEndHour = TRNEndHour;
    }

    public int getTRNStartHour() {
        return TRNStartHour;
    }

    public void setTRNStartHour(int TRNStartHour) {
        this.TRNStartHour = TRNStartHour;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getTRN_SubsEndDate() {
        return TRN_SubsEndDate;
    }

    public void setTRN_SubsEndDate(String TRN_SubsEndDate) {
        this.TRN_SubsEndDate = TRN_SubsEndDate;
    }

    public String getTRN_SubsStartDate() {
        return TRN_SubsStartDate;
    }

    public void setTRN_SubsStartDate(String TRN_SubsStartDate) {
        this.TRN_SubsStartDate = TRN_SubsStartDate;
    }

    public String getTRNEndDate() {
        return TRNEndDate;
    }

    public void setTRNEndDate(String TRNEndDate) {
        this.TRNEndDate = TRNEndDate;
    }

    public String getTRNStartDate() {
        return TRNStartDate;
    }

    public void setTRNStartDate(String TRNStartDate) {
        this.TRNStartDate = TRNStartDate;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String Street) {
        this.Street = Street;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double Latitude) {
        this.Latitude = Latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double Longitude) {
        this.Longitude = Longitude;
    }

    public int getCategory_ID() {
        return Category_ID;
    }

    public void setCategory_ID(int Category_ID) {
        this.Category_ID = Category_ID;
    }

    public int getAdmin_ID() {
        return Admin_ID;
    }

    public void setAdmin_ID(int Admin_ID) {
        this.Admin_ID = Admin_ID;
    }

    public String getTRN_Name() {
        return TRN_Name;
    }

    public void setTRN_Name(String TRN_Name) {
        this.TRN_Name = TRN_Name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public List<String> getPrizes() {
        return Prizes;
    }

    public void setPrizes(List<String> Prizes) {
        this.Prizes = Prizes;
    }
}
