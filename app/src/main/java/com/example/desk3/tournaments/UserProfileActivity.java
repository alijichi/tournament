package com.example.desk3.tournaments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileActivity extends AppCompatActivity implements InactiveInterface,  AppBarLayout.OnOffsetChangedListener {

    private ImageView imageView;
    private TextView name;
    private ApiService apiService;
    private String token;
    private int offset = 0;
    private int playerID;
    private RecyclerView recyclerView;
    private InactiveAdapter userProfileAdapter;
    private int limit;
    private TextView phone;
    private TextView email, wins, subscriptions;
    private List<TournamentsEntity> tournamentsEntities = new ArrayList<>();
    private int verifyUser;
    private int active;
    private int inactive;
    private TextView sub, winss;
    private TabLayout tabLayout;
    private TextView birthDate;
    private ViewPager viewPager;
    private int mMaxScrollSize;
    private static final int PERCENTAGE_TO_ANIMATE_LAYOUT = 20;
    private boolean mIsAvatarShown = true;
    private List<TournamentsEntity> inactiveTournaments = new ArrayList<>();
    private List<TournamentsEntity> finishedTournaments = new ArrayList<>();
    private List<TournamentsEntity> activeTournaments = new ArrayList<>();
    private List<TournamentsEntity> pendingTournaments = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        name = findViewById(R.id.users_name);
        phone = findViewById(R.id.users_phone);
        email = findViewById(R.id.users_email);
        wins = findViewById(R.id.users_wins);
        subscriptions = findViewById(R.id.users_subscriptions);
        sub = findViewById(R.id.sub_txt);
        winss = findViewById(R.id.wins_txt);
        imageView = findViewById(R.id.users_image);
        birthDate = findViewById(R.id.dateofbirth);
        AppBarLayout appbarLayout = (AppBarLayout) findViewById(R.id.appbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        recyclerView = findViewById(R.id.users_recycler);
//
//        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(linearLayoutManager);
        token = SharedPrefUtils.getStringPreference(UserProfileActivity.this, VerificationPageActivity.TOKEN);

        viewPager = (ViewPager) findViewById(R.id.user_viewpager);
        tabLayout = (TabLayout) findViewById(R.id.user_tabs);

        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.isSmoothScrollingEnabled();

        playerID = getIntent().getIntExtra("playerID", 0);
        verifyUser = getIntent().getIntExtra("verifyUser", 0);
        active = getIntent().getIntExtra("tournament_active", 0);
        inactive = getIntent().getIntExtra("tournament_inactive", 0);



        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();

        userProfileAdapter = new InactiveAdapter(this);
//        recyclerView.setAdapter(userProfileAdapter);




        if (verifyUser == 0) {
            sub.setText("Subscriptions");
            winss.setText("Wins");
        }

        else{
            sub.setText("Tournaments");
            winss.setText("Live");
        }


        setupViewPager(viewPager);
    }

    private void getUSerProfile(String token, int offset, int limit, int playerId) {
        apiService = ApiUtils.getAPIServices(token);
//        Locale locale = new Locale(language);
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        config.locale = locale;
//        getBaseContext().getResources().updateConfiguration(config,
//                getBaseContext().getResources().getDisplayMetrics());
        Call<ProfileData> call = apiService.getUserProfile(token, offset, 10, playerId);

        call.enqueue(new Callback<ProfileData>() {
            public static final String TAG = "";

            @Override
            public void onResponse(Call<ProfileData> call, Response<ProfileData> response) {
                if (response.isSuccessful()) {

                    ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());


                    if (response.body().getData() != null)
                        name.setText(response.body().getData().getNickName().toString());
                    else{
                        name.setText("-");
                    }
                    phone.setText(response.body().getData().getPhoneNumber().toString());
                    if (response.body().getData().getEmail() != null){
                        email.setText(response.body().getData().getEmail().toString());
                    }
                    else{
                        email.setText("-");
                    }

                    if (response.body().getData().getDateOfBirth() != null){
                        birthDate.setText(DateUtils.formatDate("yyyy-MM-dd'T'hh:mm:ss","yyyy-MM-dd",response.body().getData().getDateOfBirth().toString()));
                    }
                    else{
                        birthDate.setText("-");
                    }
                    if (verifyUser == 0) {
                        wins.setText(String.valueOf(response.body().getData().getNumberOfWins()));
                        subscriptions.setText(String.valueOf(response.body().getData().getNumberOFSubscriptions()));
                    } else {
                        wins.setText(String.valueOf(response.body().getData().getNumberOfWins()));
                        subscriptions.setText(String.valueOf(inactive));
                        wins.setText(String.valueOf(active));
                    }
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));
                    if (response.body().getData().getImageURl() != null) {
                        Glide.with(getBaseContext()).load(response.body().getData().getImageURl()).apply(requestOptions).into(imageView);
                    }
                    else {
                        Glide.with(getBaseContext()).load(R.drawable.rounded_cat).into(imageView);
                    }
                    finishedTournaments = response.body().getData().getFinished();
//                    finishedTournaments.addAll(response.body().getData().getFinished());
                    activeTournaments = response.body().getData().getActive();
//                    activeTournaments.addAll(response.body().getData().getActive());
                    inactiveTournaments = response.body().getData().getInActive();
//                    inactiveTournaments.addAll(response.body().getData().getInActive());
                    pendingTournaments = response.body().getData().getPending();

                    adapter.addFragment(FragmentOne.newInstances(2,1,inactiveTournaments), "Upcoming");
                    adapter.addFragment(FragmentOne.newInstances(1,1,activeTournaments), "Live");
                    adapter.addFragment(FragmentOne.newInstances(0,1,finishedTournaments), "Finished");
                    adapter.addFragment(FragmentOne.newInstances(2,1,pendingTournaments), "Pending");
                    viewPager.setAdapter(adapter);
//                    userProfileAdapter.addHistories(tournamentsEntities);

                } else {
                    Log.e(TAG, "On Response :" + response.errorBody());

                }
            }


            @Override
            public void onFailure(Call<ProfileData> call, Throwable t) {
                Log.e(TAG, "On Failure :" + t.getMessage());
                // showDialog();
            }
        });

    }


    private void setupViewPager(ViewPager viewPager) {


//        finishedTournaments = category.getFinished();
//        inactiveTournaments = category.getInActive();
//        activeTournaments = category.getActive();
        getUSerProfile(token, offset, limit, playerID);



//        for(TournamentsEntity tournamentsEntity : tournamentsEntities1){
//            if(map.containsKey(tournamentsEntity.getStatus())){
//                map.get(tournamentsEntity.getStatus()).add(tournamentsEntity);
//            }
//            else {
//                List<TournamentsEntity> tournamentsEntities = new ArrayList<>();
//                tournamentsEntities.add(tournamentsEntity);
//                map.put(tournamentsEntity.getStatus(),  tournamentsEntities);
//
//            }

//       }


    }


    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

        if (percentage >= PERCENTAGE_TO_ANIMATE_LAYOUT && mIsAvatarShown) {
            mIsAvatarShown = false;

            imageView.animate()
                    .scaleY(0).scaleX(0)
                    .setDuration(200)
                    .start();
        }

        if (percentage <= PERCENTAGE_TO_ANIMATE_LAYOUT && !mIsAvatarShown) {
            mIsAvatarShown = true;

            imageView.animate()
                    .scaleY(1).scaleX(1)
                    .start();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeAdapter/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(TournamentsEntity tournamentsEntity) {
        Intent intent = new Intent(UserProfileActivity.this, UpcomingTournamentsDetailsActivity.class);
        intent.putExtra("tour",tournamentsEntity);
        startActivity(intent);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void OnLongItemClick(TournamentsEntity tournamentsEntity, View view) {

    }
}
