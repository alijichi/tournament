package com.example.desk3.tournaments;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Desk 3 on 8/3/2018.
 */

public class FragmentLocationInfo extends Fragment {
    private EditText city;
    private EditText street;
    private ImageView map;
    int PLACE_PICKER_REQUEST = 1;
    private double latitude, longitude;
    private EditText location;
    private String address;
    private String cityName;
    private String placeName;
    private Button next;
    private TextView previous;
    private TournamentsEntity tournamentsEntity;
    private InfoInterface infoInterface;
    private Uri imageUri;
    private ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_location_info, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Gson gson = new Gson();
        String json = SharedPrefUtils.getStringPreference(getContext(), "MyObject");
        TournamentsEntity tournamentsEntity1 = gson.fromJson(json, TournamentsEntity.class);

        city = view.findViewById(R.id.created_city);
        street = view.findViewById(R.id.created_street);
        location = view.findViewById(R.id.created_card_location);
        map = view.findViewById(R.id.created_map);
        next = view.findViewById(R.id.next_location);
        previous = view.findViewById(R.id.previous_location);
        progressBar = view.findViewById(R.id.progress_create);

        String imageUriMap = "https://maps.googleapis.com/maps/api/staticmap?center=" + 33.893472499999994+ "," +35.52008984375003+ "&zoom=15&size=400x400&key=AIzaSyDojYOvtzby9pBkDRXb8AMBattDiornKJ0" + "&markers=color:red%7Clabel:S%7C" + 33.893472499999994 + "," + 35.52008984375003;
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));
        Glide.with(getContext()).load(imageUriMap).apply(requestOptions).into(map);

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickPlace();
            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    tournamentsEntity = new TournamentsEntity();
                    tournamentsEntity.setLatitude(latitude);
                    tournamentsEntity.setLongitude(longitude);
                    tournamentsEntity.setCity(city.getText().toString());
                    tournamentsEntity.setStreet(street.getText().toString());
                    tournamentsEntity.setLocationName(location.getText().toString());
//                progressBar.setVisibility(View.VISIBLE);
                    infoInterface.onInfoSet(tournamentsEntity, 4);

                } else {
                    Toast.makeText(getContext(), "please enter a valid data", Toast.LENGTH_SHORT);
                }
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                infoInterface.onInfoSet(tournamentsEntity, 2);
            }
        });
    }


    private void pickPlace() {


        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        Intent intent = null;
        try {
            intent = builder.build(getActivity());
            startActivityForResult(intent, PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof InfoInterface)
            infoInterface = (InfoInterface) context;
        else throw new IllegalArgumentException("Main activity should implement table interface");
        super.onAttach(context);
    }


    public boolean validate() {
        boolean valid = true;

        String city_name = city.getText().toString();
        String street_name = street.getText().toString();
        String place_name = location.getText().toString();


        if (city_name.isEmpty() || city_name.isEmpty()) {
            city.setError("please enter a valid city");
            valid = false;
        }

        if (street_name.isEmpty() || street_name.isEmpty()) {
            street.setError("please enter a valid street");
            valid = false;
        }

        if (place_name.isEmpty() || place_name.isEmpty()) {
            location.setError("please enter a valid place name");
            valid = false;
        }

        if (map == null) {
            Toast.makeText(getContext(), "please add location", Toast.LENGTH_SHORT).show();
            valid = false;
        }

        return valid;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            com.google.android.gms.location.places.Place place = PlacePicker.getPlace(getContext(), data);
            latitude = place.getLatLng().latitude;
            longitude = place.getLatLng().longitude;
            String toastMsg = String.format("Place: %s", place.getName());
            String imageUri = "https://maps.googleapis.com/maps/api/staticmap?center=" + place.getLatLng().latitude + "," + place.getLatLng().longitude + "&zoom=17&size=400x400&key=AIzaSyDojYOvtzby9pBkDRXb8AMBattDiornKJ0" + "&markers=color:red%7Clabel:S%7C" + place.getLatLng().latitude + "," + place.getLatLng().longitude;
            RequestOptions requestOptions = new RequestOptions();
            requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));

            Geocoder geocoder = new Geocoder(getContext());
            try {
                List<Address> addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
                address = addresses.get(0).getAddressLine(0);
                String[] addressList = address.split(",");
                cityName = addressList[1];
                placeName = addressList[0];
//                    String country = addresses.get(0).getAddressLine(2);


            } catch (IOException e) {

                e.printStackTrace();
            }

            city.setText(cityName.toString());
            street.setText("");
            location.setText(placeName.toString());

            Glide.with(this).load(imageUri).apply(requestOptions).into(map);
        }
    }




}
