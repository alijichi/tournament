package com.example.desk3.tournaments;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Desk 3 on 8/16/2018.
 */
@Dao
public interface CategoryDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(DataEntity dataEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<DataEntity> dataEntity);

    @Query("DELETE FROM DataEntity")
    void deleteAll();

    @Query("SELECT * from DataEntity")
    Single<List<DataEntity>> getAll();
}

