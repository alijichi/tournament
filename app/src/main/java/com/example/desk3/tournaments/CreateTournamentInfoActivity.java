package com.example.desk3.tournaments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.layer_net.stepindicator.StepIndicator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateTournamentInfoActivity extends AppCompatActivity implements InfoInterface, PreviewFragment.PreviewFragmentListener, FragmentCalenderInfo.FragmentCalenderInfoInterface {
    private CustomViewPager pager;
    private ApiService apiService;
    private String token;
    private String imageURl;
    private int catId;
    private String name, description, imgUrl, startDate, endDate, startSubDate, endSubDate, city, street, placeNames;
    private String cost, players, startHours, endHours;
    private Uri uri;
    private List<PrizeEntity> prizes;
    private double latitude, longitude;
    private List<TournamentDate> dates = new ArrayList<>();
    private HashMap<String, Object> map = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tournament_info);



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        token = SharedPrefUtils.getStringPreference(CreateTournamentInfoActivity.this, VerificationPageActivity.TOKEN);


        //Set the pager with an adapter
        pager = (CustomViewPager) findViewById(R.id.create_viewpager);
        pager.setPagingEnabled(false);
        catId = getIntent().getIntExtra(TournamentActivity.CATEGORY_ID, 0);
        setupViewPager(pager);
        pager.setOffscreenPageLimit(4);

        //Bind the step indicator to the adapter
        StepIndicator stepIndicator = (StepIndicator) findViewById(R.id.step_indicator);
        stepIndicator.setupWithViewPager(pager);


    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void setupViewPager(ViewPager viewPager) {


        getSupportActionBar().setTitle("Create tournament");


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

//        for(TournamentsEntity tournamentsEntity : tournamentsEntities1){
//            if(map.containsKey(tournamentsEntity.getStatus())){
//                map.get(tournamentsEntity.getStatus()).add(tournamentsEntity);
//            }
//            else {
//                List<TournamentsEntity> tournamentsEntities = new ArrayList<>();
//                tournamentsEntities.add(tournamentsEntity);
//                map.put(tournamentsEntity.getStatus(),  tournamentsEntities);
//
//            }

//       }

        adapter.addFragment(new FragmentDetailsInfo(), "hello");
        adapter.addFragment(new FragmentTimeInfo(), "bye");
        adapter.addFragment(new FragmentCalenderInfo(), "");
        adapter.addFragment(new FragmentLocationInfo(), "");
        adapter.addFragment(new PreviewFragment(), "");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 4) {
                    Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.create_viewpager + ":" + viewPager.getCurrentItem());
                    if (page != null && page instanceof PreviewFragment && map.entrySet() != null )
                        ((PreviewFragment) page).update(map);

                }
                if (position == 2) {
                    Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.create_viewpager + ":" + viewPager.getCurrentItem());
                    if (page != null && page instanceof FragmentCalenderInfo && startDate != null && endDate != null)
                        ((FragmentCalenderInfo) page).updateCalendar(DateUtils.toMilli(startDate,"yyyy-MM-dd'T'hh:mm:ss"),DateUtils.toMilli(endDate,"yyyy-MM-dd'T'hh:mm:ss"));

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(this)
                .title("Confirmation message")
                .content("Are you sure you want to go back ? note that your data will be deleted")
                .positiveText("yes")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                })
                .negativeText("No")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).show();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeAdapter/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            new MaterialDialog.Builder(this)
                    .title("Confirmation message")
                    .content("Are you sure you want to go back ? note that your data will be deleted")
                    .positiveText("yes")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                         finish();
                        }
                    })
                    .negativeText("No")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    }).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void updateImage(MultipartBody.Part body) {
        apiService = ApiUtils.getAPIService(token);
        apiService.updateImage(body).enqueue(new Callback<ImageTournamentResponse>() {
            public static final String TAG = "";

            @Override
            public void onResponse(Call<ImageTournamentResponse> call, Response<ImageTournamentResponse> response) {
                if (response.isSuccessful()) {

                    imageURl = response.body().getPathUrl().toString();


                    sendTournamentWithoutImage(name.toString(), "Lebanon", city.toString(), street.toString(),
                            description.toString(), placeNames.toString(), Integer.valueOf(players.toString())
                            , startDate.toString()
                            , endDate.toString()
                            , startSubDate.toString()
                            , endSubDate.toString()
                            , latitude, longitude, Integer.valueOf(cost.toString()), catId, imageURl, Integer.valueOf(startHours.toString()), Integer.valueOf(endHours.toString()), dates);
                }
//                    Log.i("tag",DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", startDate.toString()));
//                    Log.i("tag",DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", endDate.toString()));
//                    Log.i("tag",DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", startSubDate.toString()));
//                    Log.i("tag",DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", endSubDate.toString()));
//                    sendTournamentWithoutImage(name.toString(),"Lebanon", city.toString(), street.toString(),
//                            description.toString(), placeNames.toString(), 15
//                            ,  DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", startDate.toString())
//                            ,  DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", endDate.toString())
//                            , "2018-08-31T12:00:00"
//                            , "2018-08-31T12:00:00"
//                            , 33.12345, 33.12345, 20, 3, "http://166.62.43.19:1797/TourImages/49bd67ec-f73e-43da-93d8-d20df4d5698d.png",10,2, null);
//                }


                else if (response.code() == 401) {
                    Intent intent = new Intent(CreateTournamentInfoActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(CreateTournamentInfoActivity.this, "An error has occurred", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ImageTournamentResponse> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
                Toast.makeText(CreateTournamentInfoActivity.this, "Error message", Toast.LENGTH_SHORT).show();

            }
        });
    }


    public void sendTournamentWithoutImage(String TRN_Name, String Country, String City, String Street, String Description, String LocationName, int numberOfPlayers, String TRNStartDate
            , String TRNEndDate, String TRN_SubsStartDate, String TRN_SubsEndDate, double latitude, double longitude, int cost, int categoryId, String Imgurl, int start_hour, int end_hour, List<TournamentDate> dates) {
        List<PrizeEntity> prizeEntityList = new ArrayList<>();
        prizeEntityList = this.prizes;
        HashMap<String, Object> map = new HashMap<>();
        map.put("TRN_Name", TRN_Name);
        map.put("Country", Country);
        map.put("City", City);
        map.put("Street", Street);
        map.put("Description", Description);
        map.put("LocationName", LocationName);
        map.put("MaxPlayers", numberOfPlayers);
        map.put("TRNStartDate", TRNStartDate);
        map.put("TRNEndDate", TRNEndDate);
        map.put("TRN_SubsStartDate", TRN_SubsStartDate);
        map.put("TRN_SubsEndDate", TRN_SubsEndDate);
        map.put("Latitude", latitude);
        map.put("Longitude", longitude);
        map.put("TRNCost", cost);
        map.put("Category_ID", categoryId);
        map.put("Prizes", prizeEntityList);
        map.put("Imgurl", Imgurl);
        map.put("TRNStartHour", start_hour);
        map.put("TRNEndHour", end_hour);
        map.put("TournamentDates", dates);
        apiService = ApiUtils.getAPIService(token);

        apiService.createTournamentWithoutImage(map).enqueue(new Callback<CreateTournamentResponse>() {
            public static final String TAG = "";

            @Override
            public void onResponse(Call<CreateTournamentResponse> call, Response<CreateTournamentResponse> response) {
                if (response.isSuccessful()) {


                    Toast.makeText(CreateTournamentInfoActivity.this, "Tournament added successfuly", Toast.LENGTH_SHORT).show();
                    finish();

                } else if (response.code() == 401) {
                    Intent intent = new Intent(CreateTournamentInfoActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(getBaseContext(), "Please enter tournament playing dates in the calendar view", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CreateTournamentResponse> call, Throwable t) {
                Toast.makeText(CreateTournamentInfoActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    @Override
    public void onInfoSet(TournamentsEntity tournamentsEntity, int pageNumber) {

        if (pageNumber == 0 && tournamentsEntity!= null){
            getSupportActionBar().setTitle("Tournament Info");
        }

        if (pageNumber == 1 && tournamentsEntity != null) {
            name = tournamentsEntity.getTRN_Name();
            players = String.valueOf(tournamentsEntity.getMaxPlayers());
            cost = String.valueOf(tournamentsEntity.getTRNCost());
            description = tournamentsEntity.getDescription();
            prizes = new ArrayList<>();
            prizes = tournamentsEntity.getPrizes();
            if (tournamentsEntity.getImgurl() != null) {
                imgUrl = tournamentsEntity.getImgurl();
                uri = Uri.parse(imgUrl);
            }

            map.put("name", name);
            map.put("players", players);
            map.put("cost", cost);
            map.put("description", description);
            map.put("prizes", prizes);

            if (imageURl != null) {
                map.put("imgUrl", imgUrl);
                map.put("uri", uri);
            }

            getSupportActionBar().setTitle("select dates");
        } else if (pageNumber == 2 && tournamentsEntity != null) {

            startDate = tournamentsEntity.getTRNStartDate();
            endDate = tournamentsEntity.getTRNEndDate();
            startSubDate = tournamentsEntity.getTRN_SubsStartDate();
            endSubDate = tournamentsEntity.getTRN_SubsEndDate();
            startHours = String.valueOf(tournamentsEntity.getTRNStartHour());
            endHours = String.valueOf(tournamentsEntity.getTRNEndHour());

            map.put("startDate", startDate);
            map.put("endDate", endDate);
            map.put("startSubDate", startSubDate);
            map.put("endSubDate", endSubDate);
            map.put("startHours", startHours);
            map.put("endHours", endHours);
            getSupportActionBar().setTitle("select tournament days");
        } else if (pageNumber == 3 && tournamentsEntity != null) {
        }

        if (pageNumber == 4 && tournamentsEntity != null) {

            city = tournamentsEntity.getCity();
            street = tournamentsEntity.getStreet();
            placeNames = tournamentsEntity.getLocationName();
            latitude = tournamentsEntity.getLatitude();
            longitude = tournamentsEntity.getLongitude();
            String imageUri = "https://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=17&size=400x400&key=AIzaSyDojYOvtzby9pBkDRXb8AMBattDiornKJ0" + "&markers=color:red%7Clabel:S%7C" + latitude+ "," + longitude;


            map.put("city", city);
            map.put("street", street);
            map.put("placeNames", placeNames);
            map.put("latitude", endSubDate);
            map.put("longitude", longitude);
            map.put("mapimage",imageUri);

            getSupportActionBar().setTitle("Tournament view");




        }/* else {*/
        pager.setCurrentItem(pageNumber);
        //}
    }

    @Override
    public void onDatesRangeSet(List<TournamentDate> dates1) {

        map.put("dates", dates1);
        dates =new ArrayList<>();
        this.dates = dates1;
        pager.setCurrentItem(pager.getCurrentItem() + 1);
        getSupportActionBar().setTitle("select location");
    }

    @Override
    public void showPrevious() {
        getSupportActionBar().setTitle("select dates");
        pager.setCurrentItem(pager.getCurrentItem() - 1);

    }


    @Override
    public void showPreviousTitle(String title) {
        pager.setCurrentItem(pager.getCurrentItem() - 1);
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void OnClick() {

        if (uri != null) {
            RequestBody requestFile;
            MultipartBody.Part body = null;
            File file = new File(uri.getPath());
            requestFile =
                    RequestBody.create(
                            MediaType.parse("image/jpeg"),
                            file
                    );

            body =
                    MultipartBody.Part.createFormData("cewef", file.getName(), requestFile);


            updateImage(body);
        }
        else{
            sendTournamentWithoutImage(name.toString(), "Lebanon", city.toString(), street.toString(),
                    description.toString(), placeNames.toString(), Integer.valueOf(players.toString())
                    , startDate.toString()
                    , endDate.toString()
                    , startSubDate.toString()
                    , endSubDate.toString()
                    , latitude, longitude, Integer.valueOf(cost.toString()), catId, imageURl, Integer.valueOf(startHours.toString()), Integer.valueOf(endHours.toString()), dates);
        }
    }




    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }
}
