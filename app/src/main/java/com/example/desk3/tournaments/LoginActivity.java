package com.example.desk3.tournaments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.firebase.iid.FirebaseInstanceId;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    public static final String CONST_FIRST_TIME_LOGIN = "CONST_FIRST_TIME_LOGIN";
    private EditText mobile_nb;
    private Button submit_mobile_nb;
    private ApiService mAPIService;
    private ProgressBar loading;
    private LinearLayout linearLayout;
    private ProgressBar progressBar;
    public static final String USERCODE = "usercode";
    public static final String USERDEVICECODE = "userdevicecode";

    private String android_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mobile_nb = findViewById(R.id.mobile_nb);
        submit_mobile_nb = findViewById(R.id.submit_phn);
        linearLayout = findViewById(R.id.login_container);
        progressBar = findViewById(R.id.progressLogin);
        android_id = FirebaseInstanceId.getInstance().getToken();


        submit_mobile_nb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mobile_nb.getWindowToken(), 0);
                String mobile = mobile_nb.getText().toString().trim();
                if (mobile.isEmpty() || (mobile.length() < 7 || mobile.length() > 8) || mobile == null) {
                    Snackbar.make(v, "Sorry, You entered a wrong number", Snackbar.LENGTH_SHORT).show();
                } else {
                    linearLayout.setVisibility(View.GONE);

//                    loading.setVisibility(View.VISIBLE);

                    GetVerificationCode(mobile, android_id);

//                    submit.setEnabled(false);
                }
            }
        });


    }

    public void GetVerificationCode(final String mobile, final String deviceId) {

        mAPIService = ApiUtils.getAPIService(null);

        Call<CodeResponse> call = mAPIService.GetVerificationCode(mobile, "1", deviceId, "android");
        call.enqueue(new Callback<CodeResponse>() {
            public static final String TAG = "No problem";

            @Override
            public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {

                if (response.isSuccessful()) {
                    SharedPrefUtils.setStringPreference(LoginActivity.this, USERCODE, response.body().getCode());
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                    progressBar.setVisibility(View.GONE);
                    Intent intent = new Intent(LoginActivity.this, VerificationPageActivity.class);
                    intent.putExtra("mobilenb", mobile);
                    intent.putExtra("deviceID", deviceId);
                    intent.putExtra(CONST_FIRST_TIME_LOGIN, response.body().isExist());
                    startActivity(intent);
                    finish();
                }

                else if(response.code()==401){
                    Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<CodeResponse> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
                showDialog();
//                progressBar.setVisibility(View.GONE);
                linearLayout.setVisibility(View.VISIBLE);

            }
        });
    }

    public void showDialog() {

        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                .setMessage("Connection Error")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

}

