package com.example.desk3.tournaments;

import java.io.Serializable;

/**
 * Created by Desk 3 on 6/19/2018.
 */

    public class CompetitorData implements Serializable {

        private String name;
        private String score;
        private String ImageURl;
        private int id;

    public CompetitorData(String name, String score, String imageURl, int id) {
        this.name = name;
        this.score = score;
        ImageURl = imageURl;
        this.id = id;
    }

    public CompetitorData() {
    }

    public String getScore() {
            return score;
        }

        public void setScore(String score) {
            this.score = score;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    public String getImageURl() {
        return ImageURl;
    }

    public void setImageURl(String imageURl) {
        ImageURl = imageURl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

