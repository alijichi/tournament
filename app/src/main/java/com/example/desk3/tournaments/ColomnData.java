package com.example.desk3.tournaments;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Desk 3 on 6/19/2018.
 */

public class ColomnData implements Serializable {

    public ColomnData(ArrayList<MatchData> matches) {
        this.matches = matches;
    }

    private ArrayList<MatchData> matches;

    public void setMatches(ArrayList<MatchData> matches) {
        this.matches = matches;
    }

    public ArrayList<MatchData> getMatches() {
        return matches;
    }
}
