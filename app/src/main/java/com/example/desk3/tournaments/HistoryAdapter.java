package com.example.desk3.tournaments;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desk 3 on 7/13/2018.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {
    private List<TournamentsEntity> tournamentsEntities;
    private HistoryInterface historyInterface;


    public HistoryAdapter(HistoryInterface historyInterface) {
        tournamentsEntities = new ArrayList<>();
        this.historyInterface = historyInterface;
    }

    @NonNull
    @Override
    public HistoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tournament_card, parent, false);

        return new HistoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryAdapter.MyViewHolder holder, int position) {

        holder.name.setText(tournamentsEntities.get(position).getTRN_Name());

    }

    @Override
    public int getItemCount() {
        return tournamentsEntities.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView name;



        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.card_name);

        }
    }

    public void addHistories(List<TournamentsEntity> tournamentsEntityList) {
//        tournamentsEntityList.clear();
        tournamentsEntities.addAll(tournamentsEntityList);
        notifyDataSetChanged();

    }
}
