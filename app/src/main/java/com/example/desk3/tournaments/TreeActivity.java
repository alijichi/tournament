package com.example.desk3.tournaments;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class TreeActivity extends AppCompatActivity implements BracketsColomnFragment.UpdateScoresInterface {

    private BracketsFragment bracketFragment;
    private String token;
    private TournamentsEntity tournamentsEntity;
    private ImageView close;
    private TextView tittle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tree_activity);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tittle = findViewById(R.id.tittle);

        token = getIntent().getStringExtra("tokentour");
        tournamentsEntity = getIntent().getParcelableExtra("tournois");
        tittle.setText(tournamentsEntity.getTRN_Name());


        initialiseBracketsFragment();
    }

    private void initialiseBracketsFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("token1", token);
        bundle.putParcelable("tournois", tournamentsEntity);
        close = findViewById(R.id.close);
        bracketFragment = new BracketsFragment();
        bracketFragment.setArguments(bundle);
        android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, bracketFragment, "brackets_home_fragment");
        transaction.commit();
        manager.executePendingTransactions();


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //MatchData matchData = TournamentDetails.convertToMatchData(new TournamentDetails(),0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setScreenSize();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeAdapter/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//    }

    private void setScreenSize() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        BracketsApplication.getInstance().setScreenHeight(height);
    }

    @Override
    public void updatedScores(boolean update) {
        if(update == true) {
            Bundle bundle = new Bundle();
            bundle.putString("token1", token);
            bundle.putParcelable("tournois", tournamentsEntity);
            close = findViewById(R.id.close);
            bracketFragment = new BracketsFragment();
            bracketFragment.setArguments(bundle);
            android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.container, bracketFragment, "brackets_home_fragment");
            bracketFragment.RefreshContent(update);
            transaction.commit();
            manager.executePendingTransactions();
        }
    }
}
