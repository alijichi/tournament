package com.example.desk3.tournaments;

/**
 * Created by Desk 3 on 6/25/2018.
 */

public class Interest {
    private String name;
    private boolean isPressed;
    private String id;

    public Interest(String name, boolean isPressed, String id) {
        this.name = name;
        this.isPressed = isPressed;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPressed() {
        return isPressed;
    }

    public void setPressed(boolean pressed) {
        isPressed = pressed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
