package com.example.desk3.tournaments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Desk 3 on 7/9/2018.
 */

public class CreateTournamentResponse implements Parcelable {

    @Expose
    @SerializedName("TourID")
    private String TourID;

    public String getTourID() {
        return TourID;
    }

    public void setTourID(String tourID) {
        TourID = tourID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.TourID);
    }

    public CreateTournamentResponse() {
    }

    protected CreateTournamentResponse(Parcel in) {
        this.TourID = in.readString();
    }

    public static final Parcelable.Creator<CreateTournamentResponse> CREATOR = new Parcelable.Creator<CreateTournamentResponse>() {
        @Override
        public CreateTournamentResponse createFromParcel(Parcel source) {
            return new CreateTournamentResponse(source);
        }

        @Override
        public CreateTournamentResponse[] newArray(int size) {
            return new CreateTournamentResponse[size];
        }
    };
}
