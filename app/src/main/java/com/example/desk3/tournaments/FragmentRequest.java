package com.example.desk3.tournaments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Desk 3 on 7/20/2018.
 */

public class FragmentRequest extends Fragment implements RequestInterface {
    private RecyclerView recyclerView;
    private RequestAdapter requestAdapter;
    private List<TournamentsEntity> tournamentsEntities = new ArrayList<>();
    private String token;
    private ApiService apiService;
    private int offset =0;
    private static final int ACTIVITY_REQUEST_CODE_TOURNAMENT_REQUEST = 3;
    private int limit;
    private SwipeRefreshLayout swipeRefreshLayout;
    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_request, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        recyclerView = view.findViewById(R.id.request_recycler);
        swipeRefreshLayout = view.findViewById(R.id.activity_main_swipe_refresh_layout);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        token = SharedPrefUtils.getStringPreference(getContext(), VerificationPageActivity.TOKEN);
        requestAdapter = new RequestAdapter(this);
        getInactiveTournaments(0,limit);
        recyclerView.setAdapter(requestAdapter);


        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                offset += 1;
                getInactiveTournaments(offset, limit);


            }
        };
        // Adds the scroll listener to RecyclerView
        recyclerView.addOnScrollListener(scrollListener);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });


    }



    private void refreshContent() {

        getInactiveTournaments(offset,4);
    }


    private void getInactiveTournaments(int offset, int limit) {
        apiService = ApiUtils.getAPIService(token);

        Call<TournamentResponse> call = apiService.getInactiveTournaments(offset,4);


        call.enqueue(new Callback<TournamentResponse>() {
            public static final String TAG = "";

            @Override
            public void onResponse(Call<TournamentResponse> call, Response<TournamentResponse> response) {
                if (response.isSuccessful()) {

                    tournamentsEntities = response.body().getTournaments();
                    requestAdapter.addRequests(tournamentsEntities);



                } else {
                    Log.e(TAG, "On Response :" + response.errorBody());

                }
            }


            @Override
            public void onFailure(Call<TournamentResponse> call, Throwable t) {
                Log.e(TAG, "On Failure :" + t.getMessage());
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTIVITY_REQUEST_CODE_TOURNAMENT_REQUEST) {
            if (resultCode == RESULT_OK) {
                int tournamentID = data.getIntExtra(UpcomingTournamentsDetailsActivity.TOURNAMENT_ID_REQUEST, 0);
                requestAdapter.deleteItem(tournamentID);

            }
        }
    }


    @Override
    public void OnItemClick(TournamentsEntity tournamentsEntity) {
        Intent intent = new Intent(getContext(),UpcomingTournamentsDetailsActivity.class);
        intent.putExtra("key", 2);
        intent.putExtra("tour", (Parcelable) tournamentsEntity);
        startActivityForResult(intent,ACTIVITY_REQUEST_CODE_TOURNAMENT_REQUEST);

    }
}
