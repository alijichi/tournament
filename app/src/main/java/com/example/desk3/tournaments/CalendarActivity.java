package com.example.desk3.tournaments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class CalendarActivity extends AppCompatActivity {

    private List<TournamentDate> dates;
    private MaterialCalendarView calendarView;
    private Button done;
    private TournamentsEntity tournamentsEntity;
    private int key;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        calendarView = findViewById(R.id.calendarViewPicked);
        done = findViewById(R.id.doneshow);
        calendarView.setSelectionColor(R.color.colorPrimary);
        dates = new ArrayList<>();
        tournamentsEntity = getIntent().getParcelableExtra("tournament_dates");

        key = getIntent().getIntExtra("key_created",0);
        if (key==2) {
            updateCalendar(DateUtils.toMilli(tournamentsEntity.getTRNStartDate(),"yyyy-MM-dd'T'hh:mm:ss"),DateUtils.toMilli(tournamentsEntity.getTRNEndDate(),"yyyy-MM-dd'T'hh:mm:ss"));
            dates = tournamentsEntity.getTournamentDates();
        }

        if (key==1) {
            updateCalendar(DateUtils.toMilli(tournamentsEntity.getTRNStartDate(),"yyyy-MM-dd'T'hh:mm:ss"),DateUtils.toMilli(tournamentsEntity.getTRNEndDate(),"yyyy-MM-dd'T'hh:mm:ss"));
            dates = getIntent().getParcelableArrayListExtra("tournament_dates_created");
            if (dates != null) {

                for (int i = 0; i < dates.size(); i++) {

                    calendarView.setDateSelected(DateUtils.toMilli(dates.get(i).getTourDate(), "yyyy-MM-dd'T'hh:mm:ss"), true);
                }
            }
        }

        if (dates != null) {
            updateCalendar(DateUtils.toMilli(tournamentsEntity.getTRNStartDate(),"yyyy-MM-dd'T'hh:mm:ss"),DateUtils.toMilli(tournamentsEntity.getTRNEndDate(),"yyyy-MM-dd'T'hh:mm:ss"));

            for (int i = 0; i < dates.size(); i++) {

                calendarView.setDateSelected(DateUtils.toMilli(dates.get(i).getTourDate(), "yyyy-MM-dd'T'hh:mm:ss"), true);


            }


        }

        HashSet<String> strings = new HashSet<>();
        for(TournamentDate tournamentDate : dates){
            strings.add(DateUtils.formatDate("yyyy-MM-dd'T'hh:mm:ss","yyyy-MM-dd",tournamentDate.getTourDate()));
        }

        calendarView.addDecorator(new DisabledDatesDecorator(strings));

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }


    public void updateCalendar(long minimum,long maximum){

        calendarView.state().edit().setMinimumDate(minimum)
                .setMaximumDate(maximum)
                .commit();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeAdapter/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private static class DisabledDatesDecorator implements DayViewDecorator {

        private HashSet<String> hashSet;

        public DisabledDatesDecorator(HashSet hashSet) {
            this.hashSet = hashSet;
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return !hashSet.contains(DateUtils.convertDate(day.getCalendar().getTimeInMillis(), "yyyy-MM-dd"));
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setDaysDisabled(true);
        }
    }
}
