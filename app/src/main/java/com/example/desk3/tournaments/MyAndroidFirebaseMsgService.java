package com.example.desk3.tournaments;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Desk 3 on 7/25/2018.
 */

public class MyAndroidFirebaseMsgService extends FirebaseMessagingService {

    private static final String TAG = "MyAndroidFCMService";
    private String type;
    private int tournamentId;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            type = remoteMessage.getData().get("type").toString();
            tournamentId = Integer.parseInt(remoteMessage.getData().get("TournamentID"));
//            tournamentID = remoteMessage.getData().get("tournament_id");
        }
        //Log data to Log Cat
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        //create notification

        NotificationUtil notificationUtil1 = new NotificationUtil();
        Intent intent = new Intent(this, UpcomingTournamentsDetailsActivity.class);
        intent.putExtra("tournament_id_from_notification", tournamentId);
        intent.putExtra("notification_data",true);

        NotificationUtil notificationUtil2 = new NotificationUtil();
       Intent intent1 = new Intent(this, UpcomingTournamentsDetailsActivity.class);



        if (type.equals("NotifyAdmin") || type.equals("OntournamentCreation") ||type.equals("OnTreeCreation") || type.equals("OnStartTour") || type.equals("OnAccept") || type.equals("OnReject") || type.equals("OnEndTour")|| type.equals("OnLost") || type.equals("OnWon")) {
            notificationUtil1.create(this, 0, intent, R.mipmap.ic_launcher, getString(R.string.app_name), remoteMessage.getNotification().getBody());
        }
        else if (type .equals( "OnCategoryCreation")){
            notificationUtil2.create(this, 1, intent1, R.mipmap.ic_launcher, getString(R.string.app_name), remoteMessage.getNotification().getBody());
        }
    }

//    public void createNotification(String messageBody) {
//        Intent intent = new Intent( this,MainActivity.class );
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent resultIntent = PendingIntent.getActivity( this , 0, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//
//        Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder( this)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle("Tournaments")
//                .setContentText(messageBody)
//                .setAutoCancel( true )
//                .setSound(notificationSoundURI)
//                .setContentIntent(resultIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(0, mNotificationBuilder.build());
//
//    }
}
