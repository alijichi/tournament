package com.example.desk3.tournaments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

/**
 * Created by Desk 3 on 7/20/2018.
 */

public class FragmentOrganizers extends Fragment implements OrganizerInterface
{
    public static final int ACTIVITY_REQUEST_ORGANIZER = 4;
    private RecyclerView recyclerView;
    private OrganizerAdapter organizerAdapter;
    private List<OrganizerEntity> organizerEntities = new ArrayList<>();
    private String token;
    private ApiService apiService;
    private int offset =0;
    private int limit;
    private EndlessRecyclerViewScrollListener scrollListener;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_organizers, container, false);

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycler_organizer);

        boolean check = getArguments().getBoolean("organizer_add");
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        token = SharedPrefUtils.getStringPreference(getContext(), VerificationPageActivity.TOKEN);
        organizerAdapter = new OrganizerAdapter(this, getContext());
        getOrganizers(offset,limit);
        recyclerView.setAdapter(organizerAdapter);


        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                offset += 1;
                getOrganizers(offset, limit);

            }
        };
        // Adds the scroll listener to RecyclerView
        recyclerView.addOnScrollListener(scrollListener);


        if (check){
            getOrganizers(offset,limit);
        }

    }


    private void getOrganizers(int offset, int limit) {
        apiService = ApiUtils.getAPIService(token);

        Call<OrganizerResponse> call = apiService.getOrganizers(offset,5);


        call.enqueue(new Callback<OrganizerResponse>() {
            @Override
            public void onResponse(Call<OrganizerResponse> call, Response<OrganizerResponse> response) {
                if (response.isSuccessful()) {

                    organizerEntities = response.body().getOrganizers();
                    organizerAdapter.addOrganizers(organizerEntities);



                }

                else if(response.code()==401){
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

                else {
                    Log.e(TAG, "On Response :" + response.errorBody());

                }
            }


            @Override
            public void onFailure(Call<OrganizerResponse> call, Throwable t) {
                Log.e(TAG, "On Failure :" + t.getMessage());
            }
        });

    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == ACTIVITY_REQUEST_ORGANIZER) {
//            if (resultCode == RESULT_OK) {
//
//            }
//        }
//    }


    public void deleteOrganizer(final int organizerId){

        apiService = ApiUtils.getAPIService(token);

        apiService.DeleteOrganizer(organizerId).enqueue(new Callback<String>() {
            public static final String TAG = "";

            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    organizerAdapter.deleteItem(organizerId);
                    Toast.makeText(getContext(), response.body().toString(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getContext(), "An error has occured", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Unable to submit post to API.");
            }
        });

    }

    @Override
    public void OnOrganizerLongClick(OrganizerEntity organizerEntity, View view) {
        final PopupMenu menu = new PopupMenu(getContext(), view);
        menu.getMenuInflater().inflate(R.menu.delete_item, menu.getMenu());
        menu.show();

        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                new MaterialDialog.Builder(getContext())
                        .title("Warning message")
                        .content("Are you sure you want to deactivate " + organizerEntity.getName()+" ?")
                        .positiveText("yes")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                if (organizerEntity.getInActivecount() == 0) {
                                    deleteOrganizer(organizerEntity.getID());
                                }
                                else {
                                    Toast.makeText(getContext(), "Sorry you cannot deactivate this organizer because he has an upcoming tournament", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .negativeText("No")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                            }
                        }).show();

                return true;
            }
        });
    }

    @Override
    public void OnOrganizerClick(OrganizerEntity organizerEntity) {
        Intent intent = new Intent(getContext(),UserProfileActivity.class);
        intent.putExtra("playerID", organizerEntity.getID());
        intent.putExtra("verifyUser",1);
        intent.putExtra("tournament_active", organizerEntity.getActivecount());
        intent.putExtra("tournament_inactive", organizerEntity.getInActivecount());
        startActivity(intent);
    }

//    public static Fragment newInstanceOrganizer() {
//        FragmentOne fragmentOne = new FragmentOne();
//        Bundle bundle = new Bundle();
//        bundle.putBoolean("organizer_add",true);
//        fragmentOne.setArguments(bundle);
//        return fragmentOne;
//    }
}
