package com.example.desk3.tournaments;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class FinishedTournamentDetailsActivity extends AppCompatActivity {

    private TournamentsEntity tournamentsEntity;
    private int category_id;
    private TextView startdate;
    private TextView enddate;
    private TextView description;
    private TextView startsubdate;
    private TextView endsubdate;
    private TextView card_prize;
    private ApiService apiService;
    private String token;
    private TextView cost;
    private TextView players;
    private TextView tree;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finished_tournament_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        startdate = findViewById(R.id.startdate1);
        enddate = findViewById(R.id.enddate1);
        description = findViewById(R.id.desc);
        startsubdate = findViewById(R.id.start_sub_date1);
        endsubdate = findViewById(R.id.end_sub_date1);
        players = findViewById(R.id.playersnb1);
        cost = findViewById(R.id.costamount1);
        card_prize = findViewById(R.id.card_prize1);
        tree = findViewById(R.id.tree);


        tournamentsEntity = (TournamentsEntity) getIntent().getParcelableExtra("tour");
        token = getIntent().getStringExtra("token");
        getTournamentByID(token, tournamentsEntity.getID());
//        token = SharedPrefUtils.getStringPreference(this, VerificationPageActivity.TOKEN);

        String start = tournamentsEntity.getTRNStartDate();
        String start1 = convertTime(start);
        startdate.setText(start1);

        String end = tournamentsEntity.getTRNEndDate();
        String end1 = convertTime(end);
        enddate.setText(end1);

        description.setText(tournamentsEntity.getDescription());
//        cost.setText(tournamentsEntity.getTRNCost() + "$");
//        players.setText(tournamentsEntity.getNumberOfPlayers());

        String start_sub_date = tournamentsEntity.getTRN_SubsStartDate();
        String start_sub_date1 = convertTime(start_sub_date);
        startsubdate.setText(start_sub_date1);

        String end_sub_date = tournamentsEntity.getTRN_SubsStartDate();
        String end_sub_date1 = convertTime(end_sub_date);
        endsubdate.setText(end_sub_date1);

        card_prize.setText(tournamentsEntity.getPrize());

        tree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FinishedTournamentDetailsActivity.this, TreeActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getTournamentByID(String ttoken, int tid) {
        apiService = ApiUtils.getAPIService(token);
//        Locale locale = new Locale(language);
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        config.locale = locale;
//        getBaseContext().getResources().updateConfiguration(config,
//                getBaseContext().getResources().getDisplayMetrics());
        Call<SingleTournamentResponse> call = apiService.getTournamentByID(tid);

        call.enqueue(new Callback<SingleTournamentResponse>() {
            @Override
            public void onResponse(Call<SingleTournamentResponse> call, Response<SingleTournamentResponse> response) {
                if (response.isSuccessful()) {

//                    description.setText(response.body().getDescription());
//                    card_prize.setText(response.body().getPrize());


                } else {
                    Log.e(TAG, "On Response :" + response.errorBody());

                }
            }


            @Override
            public void onFailure(Call<SingleTournamentResponse> call, Throwable t) {
                Log.e(TAG, "On Failure :" + t.getMessage());
            }
        });

    }


    public String convertTime(String date) {
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date newDate = null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("yyyy-MM-dd");
        date = spf.format(newDate);
        return date;
    }
}

