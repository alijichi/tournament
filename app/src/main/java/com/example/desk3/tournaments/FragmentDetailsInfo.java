package com.example.desk3.tournaments;

import android.*;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Desk 3 on 8/3/2018.
 */

public class FragmentDetailsInfo extends Fragment implements ListAdapter.ListAdapterInterface {
    private EditText cost;
    private EditText desc;
    private ImageView image;
    private EditText name;
    private TextView lblListHeader;
    private RecyclerView recyclerView;
    private ListAdapter listAdapter;
    private EditText etRank, etPrize;
    private EditText etRank1, etPrize1;
    private String imageURl;
    private EditText players;
    private String token;
    private Uri selectedImage;
    private Button next;
    private InfoInterface infoInterface;
    private TournamentsEntity tournamentsEntity;
    private LinearLayout fragment_container;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details_info, container, false);


    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        token = SharedPrefUtils.getStringPreference(getContext(), VerificationPageActivity.TOKEN);
        players = view.findViewById(R.id.created_playersnb);
        lblListHeader = view.findViewById(R.id.lblListHeader);
        recyclerView = view.findViewById(R.id.recyclerView_list_prizes);
        image = view.findViewById(R.id.created_image);
        desc = view.findViewById(R.id.created_desc);
        cost = view.findViewById(R.id.created_costamount);
        next = view.findViewById(R.id.next_info);
        fragment_container = view.findViewById(R.id.fragment_container);
        name = view.findViewById(R.id.tourss_name);


        listAdapter = new ListAdapter(this, getContext());
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);




        lblListHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.custom_dialog, null);
                etRank = alertLayout.findViewById(R.id.et_rank);
                etPrize = alertLayout.findViewById(R.id.et_prize);

                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
//                    alert.setTitle("Prize List");
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert.setCancelable(false);
                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        Toast.makeText(getBaseContext(), "Cancel clicked", Toast.LENGTH_SHORT).show();
                    }
                });

                alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String rank = etRank.getText().toString();
                        String prize = etPrize.getText().toString();
//                        Toast.makeText(getBaseContext(), "Rank: " + rank + " Prize: " + prize, Toast.LENGTH_SHORT).show();
                        if (valid()) {
                            listAdapter.addPrize(new PrizeEntity(Integer.valueOf(rank), prize));
                            recyclerView.setAdapter(listAdapter);
                        }
                        else {
                            Toast.makeText(getContext(),"rank already exist", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }

        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage();
            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {

                    tournamentsEntity = new TournamentsEntity();
                    tournamentsEntity.setTRN_Name(name.getText().toString());
                    tournamentsEntity.setMaxPlayers(Integer.valueOf(players.getText().toString()));
                    tournamentsEntity.setTRNCost(Integer.valueOf(cost.getText().toString()));
                    tournamentsEntity.setDescription(desc.getText().toString());
                    tournamentsEntity.setPrizes(listAdapter.getPrizes());

                    if (selectedImage != null)
                    tournamentsEntity.setImgurl(selectedImage.toString());
//                    Log.i("",tournamentsEntity.getDescription());
                    infoInterface.onInfoSet(tournamentsEntity,1);
//                    SharedPrefUtils.setStringPreference(getContext(),"url", selectedImage.toString());



                }
                else{
                    Toast.makeText(getContext(),"please enter a valid data", Toast.LENGTH_SHORT);
                }
//                FragmentTimeInfo fragmentTimeInfo = new FragmentTimeInfo();
//                FragmentTransaction transaction = getFragmentManager().beginTransaction();
//                transaction.replace(R.id.fragment_container, fragmentTimeInfo);
//                transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
//                transaction.commit();

            }
        });

    }


    private void pickImage() {
        CropImage.startPickImageActivity(getContext(),this);
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(16, 4)
                .start(getContext(),this);
    }




    @Override
    public void onAttach(Context context) {
        if (context instanceof InfoInterface)
            infoInterface = (InfoInterface) context;
        else throw new IllegalArgumentException("Main activity should implement table interface");
        super.onAttach(context);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getContext(), data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(getContext(), imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                selectedImage = imageUri;
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {
                // no permissions required or already granted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                selectedImage = result.getUri();
                RequestOptions requestOptions = new RequestOptions();
                requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));

                Glide.with(this).load(selectedImage).apply(requestOptions).into(image);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }




    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (selectedImage != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                startCropImageActivity(selectedImage);
            } else {
                Toast.makeText(getContext(), "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }


    public boolean valid() {
        boolean validity = true;
        String rank = etRank.getText().toString();
        String prize = etPrize.getText().toString();
        if (rank.isEmpty()) {
            etRank.setError("enter a valid rank number");
            validity = false;
        }

        if (prize.isEmpty()) {
            etPrize.setError("enter a valid country");
            validity = false;
        }

        return validity;

    }

    @Override
    public void onItemClick(PrizeEntity prizeEntity, View view) {
        final PopupMenu menu = new PopupMenu(getContext(), view);
        menu.getMenuInflater().inflate(R.menu.delete_menu, menu.getMenu());
        menu.show();

        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.action_delete:
                        listAdapter.deletePrize(prizeEntity.getID());
                        return true;

                    case R.id.action_edit:
                        LayoutInflater inflater = getLayoutInflater();
                        View alertLayout = inflater.inflate(R.layout.new_custom_dialog, null);
                        etRank1 = alertLayout.findViewById(R.id.et_rank_edit);
                        etPrize1 = alertLayout.findViewById(R.id.et_prize_edit);
                        etRank1.setText(String.valueOf(prizeEntity.getRank()));
                        etPrize1.setText(String.valueOf(prizeEntity.getName()));

                        final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
//                    alert.setTitle("Prize List");
                        // this is set the view from XML inside AlertDialog
                        alert.setView(alertLayout);
                        // disallow cancel of AlertDialog on click of back button and outside touch
                        alert.setCancelable(false);
                        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                        Toast.makeText(getBaseContext(), "Cancel clicked", Toast.LENGTH_SHORT).show();
                            }
                        });

                        alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String rank = etRank1.getText().toString();
                                String prize = etPrize1.getText().toString();
//                        Toast.makeText(getBaseContext(), "Rank: " + rank + " Prize: " + prize, Toast.LENGTH_SHORT).show();

                                    if (!etRank1.getText().toString().isEmpty() && !etPrize1.getText().toString().isEmpty()) {
                                        prizeEntity.setName(prize);
                                        prizeEntity.setRank(Integer.valueOf(rank));
                                        listAdapter.updatePrize(prizeEntity);
//                                    recyclerView.setAdapter(listAdapter);
                                    } else {
                                        Toast.makeText(getContext(), "please enter a valid content", Toast.LENGTH_SHORT).show();
                                    }


                                }
                        });
                        AlertDialog dialog = alert.create();
                        dialog.show();


                        return true;


                    default:
                        return true;

                }
            }
        });
    }

    @Override
    public void onError() {
        Toast.makeText(getContext(),"Please enter a valid rank", Toast.LENGTH_SHORT).show();
    }

    public boolean validate() {
        boolean valid = true;

        String touranment_name = name.getText().toString();
        String tournament_players = players.getText().toString();
        String tournament_cost = cost.getText().toString();
        String tournament_desc = desc.getText().toString();


        if (touranment_name == null || touranment_name.isEmpty()) {
            name.setError("enter a valid name");
            valid = false;
        }

        if (tournament_players == null || tournament_players.isEmpty() || tournament_players.equals("0")) {
            players.setError("enter a valid number of players");
            valid = false;
        }

        if (tournament_cost.isEmpty() || tournament_cost.isEmpty()) {
            cost.setError("please enter a valid cost");
            valid = false;
        }

        if (tournament_desc.isEmpty() || tournament_desc.isEmpty()) {
            desc.setError("please enter a valid description");
            valid = false;
        }
//        if (selectedImage == null) {
//            Toast.makeText(getContext(), "please add an image", Toast.LENGTH_SHORT).show();
//            valid = false;
//        }
        return valid;
    }




}
