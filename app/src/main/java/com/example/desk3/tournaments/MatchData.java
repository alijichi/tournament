package com.example.desk3.tournaments;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Desk 3 on 6/19/2018.
 */

    public class MatchData implements Serializable {

        private CompetitorData competitorOne;
        private CompetitorData competitorTwo;
        private String title;
        private int roundNb;
        private String date;
        private int height;
        private int levelId;
        private int tournamentId;
        private int winnerId;
        private boolean canBeUpdate;

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public MatchData(CompetitorData competitorOne, CompetitorData competitorTwo, String title, boolean canUpdate) {
            this.competitorOne = competitorOne;
            this.competitorTwo = competitorTwo;
            this.canBeUpdate = canUpdate;
        }

    public MatchData() {
    }

    public CompetitorData getCompetitorTwo() {
            return competitorTwo;
        }

        public void setCompetitorTwo(CompetitorData competitorTwo) {
            this.competitorTwo = competitorTwo;
        }

        public CompetitorData getCompetitorOne() {

            return competitorOne;
        }

        public void setCompetitorOne(CompetitorData competitorOne) {
            this.competitorOne = competitorOne;
        }

    public int getRoundNb() {
        return roundNb;
    }

    public void setRoundNb(int roundNb) {
        this.roundNb = roundNb;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public int getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(int winnerId) {
        this.winnerId = winnerId;
    }

    public boolean isCanBeUpdate() {
        return canBeUpdate;
    }

    public void setCanBeUpdate(boolean canBeUpdate) {
        this.canBeUpdate = canBeUpdate;
    }
}
