package com.example.desk3.tournaments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

/**
 * Created by Desk 3 on 7/12/2018.
 */

public class FragmentNotification extends Fragment implements NotificationInterface {


    public static final int READ = 5;
    private RecyclerView recyclerView;
    private NotificationAdapter notificationAdapter;
    private List<Notification> notificationList = new ArrayList<>();
    private String token;
    private ApiService apiService;
    private int offset = 0;
    private int limit;
    private EndlessRecyclerViewScrollListener scrollListener;
    private TextView title_dialog;
    private TextView text_dialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notifications, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        recyclerView = view.findViewById(R.id.recycler_notification);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        token = SharedPrefUtils.getStringPreference(getContext(), VerificationPageActivity.TOKEN);
        notificationAdapter = new NotificationAdapter(this, getContext());
        recyclerView.setAdapter(notificationAdapter);
        getUserNotifications(offset, limit);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                offset += 1;
                getUserNotifications(offset, limit);

            }
        };
        // Adds the scroll listener to RecyclerView
        recyclerView.addOnScrollListener(scrollListener);

    }


    private void getUserNotifications(int offset, int limit) {
        apiService = ApiUtils.getAPIService(token);

        Call<NotificationResponse> call = apiService.getUserNotifications(offset, 10);


        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null) {
                        notificationList = response.body().getNotifications();
                        notificationAdapter.addNotifications(notificationList);
                    }


                } else if (response.code() == 401) {
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Log.e(TAG, "On Response :" + response.errorBody());

                }
            }


            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                Log.e(TAG, "On Failure :" + t.getMessage());
            }
        });

    }


    private void updateUserNotification(final int id) {
        apiService = ApiUtils.getAPIService(token);

        Call<String> call = apiService.updateNotification(id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    notificationAdapter.updateNotification(id);


                } else {
                    Log.e(TAG, "On Response :" + response.errorBody());

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, "On Failure :" + t.getMessage());

            }
        });
    }

    @Override
    public void onClick(final Notification notification) {
        updateUserNotification(notification.getID());
        Intent intent = new Intent(getContext(), UpcomingTournamentsDetailsActivity.class);
        intent.putExtra("tournament_id_from_notification", notification.getTRN_ID());
        intent.putExtra("notification_data", true);
        startActivityForResult(intent, READ);
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == READ) {
//            if (resultCode == RESULT_OK) {
//                int tournamentID = data.getIntExtra(UpcomingTournamentsDetailsActivity.TOURNAMENT_ID_NOTIFICATION, 0);
//                updateUserNotification(tournamentID);
//            }
//        }
//    }

//            LayoutInflater inflater = getLayoutInflater();
//            View alertLayout = inflater.inflate(R.layout.notification_dialog, null);
//            title_dialog = alertLayout.findViewById(R.id.tittle_dialog);
//            text_dialog = alertLayout.findViewById(R.id.text_dialog);
//            text_dialog.setText(notification.getText().toString());
//
//            AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
//            alert.setView(alertLayout);
//            alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
//
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    updateUserNotification(notification.getID());
//                }
//            });
//            AlertDialog dialog = alert.create();
//            dialog.show();



       /* FragmentManager fm = getFragmentManager();
        MyCustomDialogFragment dialogFragment = new MyCustomDialogFragment ();
        dialogFragment.show(fm, "Sample Fragment");*/
}
//
//    @Override
//    public void onLongClick(Notification notification) {
//        new MaterialDialog.Builder(getContext())
//                .title("Notification")
//                .content(notification.getText())
//                .positiveText("Done")
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        updateUserNotification(notification.getID());
//                    }
//                }).show();
//    }

