package com.example.desk3.tournaments;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Desk 3 on 5/29/2018.
 */

public class GenerateToken {

    @SerializedName("Token")
    private String Token;


    @SerializedName("UserType")
    private int UserType;


    public GenerateToken() {
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public int getUserType() {
        return UserType;
    }

    public void setUserType(int userType) {
        UserType = userType;
    }

}
