package com.example.desk3.tournaments;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Desk 3 on 5/29/2018.
 */

public interface ApiService {

    @GET("Register/RegisterPhoneNumber")
    Call<CodeResponse> GetVerificationCode(@Query("PhoneNumber") String PhoneNumber, @Query("VersionID") String VersionID, @Query("Device_ID") String Device_ID,@Query("Platform") String Platform);

    @GET("Register/VerifyPhoneNumber")
    Call<GenerateToken> VerifyPhoneNumber(@Query("PhoneNumber") String PhoneNumber, @Query("Code") String Code, @Query("VersionID") String VersionID);

    @GET("User//getCategory")
    Call<CategoryContainer> getData(@Query("Token") String Token);

    @GET("User/getTournamentByStatus")
    Call<TournamentResponse> getTournamentByStatus(@Query("Token") String Token, @Query("type") int type, @Query("cid") int cid, @Query("offset") int offset, @Query("limit") int limit);

    @POST("Register//EditProfileImage")
    @Multipart
    Call<Profile> updateProfile(@Part MultipartBody.Part image);

    @POST("Register/EditProfile")
    @FormUrlEncoded
    Call<String> updateProfileWithoutImage(@Field("NickName") String NickName, @Field("Email") String Email, @Field("DateOfBirth") String DateOfBirth,@Field("PhoneNumber") String PhoneNumber, @Field("InterestedIN")
            String interests);

    @POST("Admin/RegisterOrganizer")
    Call<String> RegisterOrganizer(@Body HashMap<String,Object> map);

    @GET("Admin/getOrganizers")
    Call<OrganizerResponse> getOrganizers(@Query("offset") int offset, @Query("limit") int limit);

    @GET("User/getTournamentByID")
    Call<SingleTournamentResponse> getTournamentByID(@Query("Trn") int Trn);


    @GET("Admin/getInactiveTournaments")
    Call<TournamentResponse>getInactiveTournaments(@Query("offset") int offset,@Query("limit") int limit);

    @GET("User/GetUserTours")
    Call<TournamentResponse> getUserTournaments(@Query("pendingtour") boolean isPending ,@Query("offset") int offset,@Query("limit") int limit);

    @GET("User/GetUserNotifications")
    Call<NotificationResponse> getUserNotifications(@Query("offset") int offset,@Query("limit") int limit);


    @POST("User/UpdateNotification")
    Call<String> updateNotification(@Query("ntfID") int ntfID);


    @GET("User/getTournamentByID")
    Call<TournamentDetails> getTournamentCompetitorsByID(@Query("Trn") int Trn);

    @GET("Register/getUserProfile")
    Call<ProfileData> getUserProfile(@Query("Token") String Token,@Query("offset") int offset,@Query("limit") int limit,@Query("player_ID") int player_ID);

    @GET("Register/getUserProfile")
    Call<ProfileData> getPrivateUserProfile(@Query("Token") String Token);

    @POST("Admin/CreateTournament")
    Call<CreateTournamentResponse> createTournamentWithoutImage(@Body HashMap hashMap);

    @POST("Admin/DeleteTournament")
    Call<String> deleteTournament(@Query("Tourid") int Tourid);

    @POST("Admin/DeleteOrganizer")
    Call<String> DeleteOrganizer(@Query("organizerID") int organizerID);


    @POST("User/subscribeTournament")
    Call<String> subscribeTournament(@Query("Trnmnt_ID") int Trnmnt_ID, @Query("CtID") int CtID);

    @POST("Admin/UploadImage")
    @Multipart
    Call<ImageTournamentResponse> updateImage(@Part MultipartBody.Part image);

    @POST("Admin/AcceptRejectTournament")
    Call<String> AcceptRejectTournament(@Query("TRN_ID") int TRN_ID, @Query("status") int status);

    @POST("User/logOut")
    Call<String> logOut();


    @POST("Admin/CompetitorsWinners")
    Call<String> updateScores(@QueryMap HashMap<String, Object> map);
   /* Call<CreateTournamentResponse> createTournamentWithoutImage(@Field("TRN_Name") String TRN_Name, @Field("Country") String Country, @Field("City")
            String City,@Field("Street") String Street,@Field("Description") String Description,@Field("LocationName") String LocationName, @Field("numberOfPlayers") int numberOfPlayers,@Field("TRNStartDate") String TRNStartDate,@Field("TRNEndDate") String TRNEndDate,@Field("TRN_SubsStartDate") String TRN_SubsStartDate,@Field("TRN_SubsEndDate") String TRN_SubsEndDate,@Field("Longitude") double Longitude,@Field("TRN_SubsEndDate") double Latitude,@Field("TRNCost") int TRNCost);*/

}
