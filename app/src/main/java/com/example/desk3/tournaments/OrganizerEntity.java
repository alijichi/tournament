package com.example.desk3.tournaments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Desk 3 on 7/20/2018.
 */

public class OrganizerEntity implements Parcelable {

    @Expose
    @SerializedName("InActivecount")
    private int InActivecount;
    @Expose
    @SerializedName("Activecount")
    private int Activecount;
    @Expose
    @SerializedName("FinishedCount")
    private int FinishedCount;
    @Expose
    @SerializedName("IsAdmin")
    private int IsAdmin;
    @Expose
    @SerializedName("PhoneNumber")
    private String PhoneNumber;
    @Expose
    @SerializedName("Name")
    private String Name;
    @Expose
    @SerializedName("Email")
    private String Email;
    @Expose
    @SerializedName("ID")
    private int ID;

    @Expose
    @SerializedName("ImageURl")
    private String ImageURl;

    public int getInActivecount() {
        return InActivecount;

    }

    public void setInActivecount(int InActivecount) {
        this.InActivecount = InActivecount;
    }

    public int getActivecount() {
        return Activecount;
    }

    public void setActivecount(int Activecount) {
        this.Activecount = Activecount;
    }

    public int getFinishedCount() {
        return FinishedCount;
    }

    public void setFinishedCount(int FinishedCount) {
        this.FinishedCount = FinishedCount;
    }

    public int getIsAdmin() {
        return IsAdmin;
    }

    public void setIsAdmin(int IsAdmin) {
        this.IsAdmin = IsAdmin;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String PhoneNumber) {
        this.PhoneNumber = PhoneNumber;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getImageURl() {
        return ImageURl;
    }

    public void setImageURl(String imageURl) {
        ImageURl = imageURl;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.InActivecount);
        dest.writeInt(this.Activecount);
        dest.writeInt(this.FinishedCount);
        dest.writeInt(this.IsAdmin);
        dest.writeString(this.PhoneNumber);
        dest.writeString(this.Name);
        dest.writeString(this.Email);
        dest.writeInt(this.ID);
        dest.writeString(this.ImageURl);
    }

    public OrganizerEntity() {
    }

    protected OrganizerEntity(Parcel in) {
        this.InActivecount = in.readInt();
        this.Activecount = in.readInt();
        this.FinishedCount = in.readInt();
        this.IsAdmin = in.readInt();
        this.PhoneNumber = in.readString();
        this.Name = in.readString();
        this.Email = in.readString();
        this.ID = in.readInt();
        this.ImageURl = in.readString();
    }

    public static final Creator<OrganizerEntity> CREATOR = new Creator<OrganizerEntity>() {
        @Override
        public OrganizerEntity createFromParcel(Parcel source) {
            return new OrganizerEntity(source);
        }

        @Override
        public OrganizerEntity[] newArray(int size) {
            return new OrganizerEntity[size];
        }
    };
}