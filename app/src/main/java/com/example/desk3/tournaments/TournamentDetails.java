package com.example.desk3.tournaments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Desk 3 on 6/21/2018.
 */

public class TournamentDetails {


    @Expose
    @SerializedName("Data")
    private DataEntity Data;

    public DataEntity getData() {
        return Data;
    }

    public void setData(DataEntity Data) {
        this.Data = Data;
    }

    public static class DataEntity {
        @Expose
        @SerializedName("Competitors")
        private List<CompetitorsEntity> Competitors;


        @Expose
        @SerializedName("tour")
        private TournamentsEntity tour;

        public List<CompetitorsEntity> getCompetitors() {
            return Competitors;
        }

        public void setCompetitors(List<CompetitorsEntity> Competitors) {
            this.Competitors = Competitors;
        }

        public TournamentsEntity getTour() {
            return tour;
        }

        public void setTour(TournamentsEntity tour) {
            this.tour = tour;
        }
    }

    public static class CompetitorsEntity {
        @Expose
        @SerializedName("player2")
        private Player2Entity player2;
        @Expose
        @SerializedName("player1")
        private Player1Entity player1;
        @Expose
        @SerializedName("PlayerOnePoints")
        private String PlayerOnePoints;

        @Expose
        @SerializedName("PlayerTwoPoints")
        private String PlayerTwoPoints;

        @Expose
        @SerializedName("canBeUpdated")
        private boolean canBeUpdated;

        @Expose
        @SerializedName("CompDate")
        private String CompDate;


        @Expose
        @SerializedName("TRN_ID")
        private int TRN_ID;
        @Expose
        @SerializedName("Winner_ID")
        private int Winner_ID;
        @Expose
        @SerializedName("PlayerTwo_ID")
        private int PlayerTwo_ID;
        @Expose
        @SerializedName("PlayerOne_ID")
        private int PlayerOne_ID;
        @Expose
        @SerializedName("Levelnumber")
        private int Levelnumber;
        @SerializedName("RoundNumber")
        private int RoundNumber;

        public Player2Entity getPlayer2() {
            return player2;
        }

        public void setPlayer2(Player2Entity player2) {
            this.player2 = player2;
        }

        public Player1Entity getPlayer1() {
            return player1;
        }

        public void setPlayer1(Player1Entity player1) {
            this.player1 = player1;
        }

        public int getTRN_ID() {
            return TRN_ID;
        }

        public void setTRN_ID(int TRN_ID) {
            this.TRN_ID = TRN_ID;
        }

        public int getWinner_ID() {
            return Winner_ID;
        }

        public void setWinner_ID(int Winner_ID) {
            this.Winner_ID = Winner_ID;
        }

        public int getPlayerTwo_ID() {
            return PlayerTwo_ID;
        }

        public void setPlayerTwo_ID(int PlayerTwo_ID) {
            this.PlayerTwo_ID = PlayerTwo_ID;
        }

        public int getPlayerOne_ID() {
            return PlayerOne_ID;
        }

        public void setPlayerOne_ID(int PlayerOne_ID) {
            this.PlayerOne_ID = PlayerOne_ID;
        }

        public int getLevelnumber() {
            return Levelnumber;
        }

        public void setLevelnumber(int Levelnumber) {
            this.Levelnumber = Levelnumber;
        }

        public int getRoundNumber() {
            return RoundNumber;
        }

        public void setRoundNumber(int roundNumber) {
            RoundNumber = roundNumber;
        }

        public String getPlayerOnePoints() {
            return PlayerOnePoints;
        }

        public void setPlayerOnePoints(String playerOnePoints) {
            PlayerOnePoints = playerOnePoints;
        }

        public String getPlayerTwoPoints() {
            return PlayerTwoPoints;
        }

        public void setPlayerTwoPoints(String playerTwoPoints) {
            PlayerTwoPoints = playerTwoPoints;
        }

        public boolean isCanBeUpdated() {
            return canBeUpdated;
        }

        public void setCanBeUpdated(boolean canBeUpdated) {
            this.canBeUpdated = canBeUpdated;
        }

        public String getCompDate() {
            return CompDate;
        }

        public void setCompDate(String compDate) {
            CompDate = compDate;
        }
    }

    public static class Player2Entity {
        @Expose
        @SerializedName("NickName")
        private String NickName;
        @Expose
        @SerializedName("PhoneNumber")
        private String PhoneNumber;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("ID")
        private int ID;
        @Expose
        @SerializedName("ImageURl")
        private String ImageURl;



        public String getNickName() {
            return NickName;
        }

        public void setNickName(String NickName) {
            this.NickName = NickName;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String PhoneNumber) {
            this.PhoneNumber = PhoneNumber;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public String getImageURl() {
            return ImageURl;
        }

        public void setImageURl(String imageURl) {
            ImageURl = imageURl;
        }
    }

    public static class Player1Entity {
        @Expose
        @SerializedName("NickName")
        private String NickName;
        @Expose
        @SerializedName("PhoneNumber")
        private String PhoneNumber;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("ID")
        private int ID;
        @Expose
        @SerializedName("ImageURl")
        private String ImageURl;


        public String getNickName() {
            return NickName;
        }

        public void setNickName(String NickName) {
            this.NickName = NickName;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String PhoneNumber) {
            this.PhoneNumber = PhoneNumber;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }


        public String getImageURl() {
            return ImageURl;
        }

        public void setImageURl(String imageURl) {
            ImageURl = imageURl;
        }
    }


    public static MatchData convertToMatchData(TournamentDetails.CompetitorsEntity competitorsEntity) {
        MatchData matchData = new MatchData();
        CompetitorData competitorDataPlayer1;
        CompetitorData competitorDataPlayer2;

        Player1Entity player1Entity1 = competitorsEntity.getPlayer1();
        Player2Entity player1Entity2 = competitorsEntity.getPlayer2();


        competitorDataPlayer1 = new CompetitorData(player1Entity1.getNickName(), competitorsEntity.getPlayerOnePoints(), String.valueOf(player1Entity1.getImageURl()), player1Entity1.getID());
        competitorDataPlayer2 = new CompetitorData(player1Entity2.getNickName(), competitorsEntity.getPlayerTwoPoints(), String.valueOf(player1Entity2.getImageURl()), player1Entity2.getID());

        matchData.setCompetitorOne(competitorDataPlayer1);
        matchData.setCompetitorTwo(competitorDataPlayer2);
        matchData.setRoundNb(competitorsEntity.getRoundNumber());
        matchData.setLevelId(competitorsEntity.getLevelnumber());
        matchData.setTournamentId(competitorsEntity.getTRN_ID());
        matchData.setCanBeUpdate(competitorsEntity.isCanBeUpdated());
        matchData.setDate(competitorsEntity.getCompDate());

//        matchData.setDate(competitorsEntity.get);

        return matchData;
    }
}
