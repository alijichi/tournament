package com.example.desk3.tournaments;

/**
 * Created by Desk 3 on 7/13/2018.
 */

public interface BracketsInterface {
    void OnLongItemClick(MatchData matchData);
    void OnItemClick(CompetitorData competitorData);
}
