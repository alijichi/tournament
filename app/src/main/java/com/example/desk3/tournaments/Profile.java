package com.example.desk3.tournaments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Desk 3 on 5/31/2018.
 */

public class Profile implements Parcelable {


    @Expose
    @SerializedName("NickName")
    private String NickName;
    @Expose
    @SerializedName("ImageURl")
    private String ImageURl;
    @Expose
    @SerializedName("Email")
    private String Email;
//    @Expose
//    @SerializedName("InterestedIN")
//    private List<InterestedIN> InterestedIN;


    public String getNickName() {
        return NickName;
    }

    public void setNickName(String NickName) {
        this.NickName = NickName;
    }

    public String getImageURl() {
        return ImageURl;
    }

    public void setImageURl(String imageURl) {
        ImageURl = imageURl;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.NickName);
        dest.writeString(this.ImageURl);
        dest.writeString(this.Email);
    }

    public Profile() {
    }

    protected Profile(Parcel in) {
        this.NickName = in.readString();
        this.ImageURl = in.readString();
        this.Email = in.readString();
    }

    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel source) {
            return new Profile(source);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
}
