package com.example.desk3.tournaments;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Desk 3 on 5/29/2018.
 */

public class CodeResponse {
    @SerializedName("Code")
    private String Code;
    @SerializedName("exist")
    private boolean exist;

    public CodeResponse() {
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public boolean isExist() {
        return exist;
    }

    public void setExist(boolean exist) {
        this.exist = exist;
    }
}
