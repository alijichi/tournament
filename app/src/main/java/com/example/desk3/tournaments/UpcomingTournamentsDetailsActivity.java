package com.example.desk3.tournaments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class UpcomingTournamentsDetailsActivity extends AppCompatActivity implements ListAdapter.ListAdapterInterface {


    private TournamentsEntity tournamentsEntity;
    private int category_id;
    private TextView startdate;
    private TextView enddate;
    private TextView description;
    private TextView startsubdate;
    private TextView endsubdate;
    private TextView card_location;
    private ApiService apiService;
    private RecyclerView recyclerView;
    private String token;
    private TextView cost;
    private TextView players;
    private TextView country;
    private  TextView organizerName;
    private TextView city;
    private TextView street;
    private TextView lblListHeader;
    private int key;
    private Button tree;
    private Button subscribe;
    private ImageView map;
    private ListAdapter listAdapter;
    private List<PrizeEntity> prizes = new ArrayList<>();
    private String gift;
    private String name;
    private TextView starthour;
    private TextView endHour;
    private ImageView imgUrl;
    private Button accept, reject;
    public static final String EDIT = "EDIT";
    public static final String TOURNAMENT_ID = "tournament_id";
    public static final String TOURNAMENT_ID_REQUEST = "tournament_id_request";
    public static final String TOURNAMENT_ID_NOTIFICATION = "TOURNAMENT_ID_NOTIFICATION";
    private boolean request;
    private int tournamentNotificationId;
    private int type;
    private boolean isNotification = false;
    private DatePickerDialog datePickerDialog;
    private ImageView calendar;
    private int tourID;
    private TextView startHourTime, endHourTime;
    private  TextView maxPlayers;
    private  int status;
    private TextView subscribers;
    private   RequestOptions requestOptionss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tournament_details);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        startdate = findViewById(R.id.startdate);
        enddate = findViewById(R.id.enddate);
        description = findViewById(R.id.desc);
        startsubdate = findViewById(R.id.start_sub_date);
        endsubdate = findViewById(R.id.end_sub_date);
        players = findViewById(R.id.playersnb);
        cost = findViewById(R.id.costamount);
        card_location = findViewById(R.id.card_location);
        country = findViewById(R.id.country);
        city = findViewById(R.id.city);
        street = findViewById(R.id.street);
        tree = findViewById(R.id.tree);
        subscribe = findViewById(R.id.subscribe);
        map = findViewById(R.id.map);
        starthour = findViewById(R.id.start_hour);
        endHour = findViewById(R.id.end_hour);
        lblListHeader = findViewById(R.id.lblListHeader);
        recyclerView = findViewById(R.id.recyclerView_list);
        imgUrl = findViewById(R.id.tour_image);
        accept = findViewById(R.id.accept);
        reject = findViewById(R.id.reject);
        organizerName = findViewById(R.id.organizer_name_tournament);
        calendar = findViewById(R.id.calendar_dates);
        startHourTime = findViewById(R.id.starthourtime);
        endHourTime = findViewById(R.id.endhourtime);
        maxPlayers = findViewById(R.id.maxplayers);
        subscribers = findViewById(R.id.subscribedplayers);


        isNotification = getIntent().getBooleanExtra("notification_data", false);
        token = SharedPrefUtils.getStringPreference(this, VerificationPageActivity.TOKEN);

        requestOptionss = new RequestOptions();
        requestOptionss = requestOptionss.transforms(new CenterCrop(), new RoundedCorners(16));


        if (!isNotification) {


            tournamentsEntity = getIntent().getParcelableExtra("tour");
            name = tournamentsEntity.getTRN_Name();
            getSupportActionBar().setTitle(name);

            type = SharedPrefUtils.getIntegerPreference(this, MainActivity.USER_TYPE, 0);
            category_id = getIntent().getIntExtra("category_id", 0);
//        token = getIntent().getStringExtra("token");

            key = getIntent().getIntExtra("key", 0);
            status = getIntent().getIntExtra("status", 0);





            String start = tournamentsEntity.getTRNStartDate();
            String start1 = convertTime(start);
            startdate.setText(start1);

            String end = tournamentsEntity.getTRNEndDate();
            String end1 = convertTime(end);
            enddate.setText(end1);

            description.setText(tournamentsEntity.getDescription());

//        cost.setText(tournamentsEntity.getTRNCost() + "$");
//        players.setText(tournamentsEntity.getNumberOfPlayers());

//            if (tournamentsEntity.getStatus() == 0 || tournamentsEntity.getStatus() ==1) {
//                calendar.setVisibility(View.GONE);
//            }
//                else {
//                    calendar.setVisibility(View.VISIBLE);
//                }


            String start_sub_date = tournamentsEntity.getTRN_SubsStartDate();
            String start_sub_date1 = convertTime(start_sub_date);
            startsubdate.setText(start_sub_date1);

            String end_sub_date = tournamentsEntity.getTRN_SubsEndDate();
            String end_sub_date1 = convertTime(end_sub_date);
            endsubdate.setText(end_sub_date1);

            card_location.setText(tournamentsEntity.getLocationName());

            country.setText(tournamentsEntity.getCountry());
            maxPlayers.setText(String.valueOf(tournamentsEntity.getMaxPlayers()));

            if (tournamentsEntity.getTRNStartHour()>= 0 && tournamentsEntity.getTRNStartHour() < 12)
            startHourTime.setText(tournamentsEntity.getTRNStartHour() + " AM");
            else{
                startHourTime.setText(tournamentsEntity.getTRNStartHour() + " PM");
            }
            if (tournamentsEntity.getTRNEndHour()>= 12 || tournamentsEntity.getTRNEndHour() <= 23)
                endHourTime.setText(tournamentsEntity.getTRNEndHour() + " PM");
            else{
                endHourTime.setText(tournamentsEntity.getTRNEndHour() + " AM");
            }
            city.setText(tournamentsEntity.getCity());
            if (tournamentsEntity.getOrganizerName() != null) {
                organizerName.setText("Created by " + tournamentsEntity.getOrganizerName());
            }
             else {
                organizerName.setText("Tournament created by unknown");
            }
            street.setText(tournamentsEntity.getStreet());
            if (key != 3) {
                players.setText(String.valueOf(tournamentsEntity.getNumberOfPlayers()));
            }
            else{
                players.setVisibility(View.GONE);
            }
            cost.setText(String.valueOf(tournamentsEntity.getTRNCost()));
            if (tournamentsEntity.getImgurl() != null) {
                Glide.with(this).load(tournamentsEntity.getImgurl()).apply(requestOptionss).into(imgUrl);
            } else {
                Glide.with(this).load(R.drawable.tour).apply(requestOptionss).into(imgUrl);
            }


            String imageUri = "https://maps.googleapis.com/maps/api/staticmap?center=" + tournamentsEntity.getLatitude() + "," + tournamentsEntity.getLongitude() + "&zoom=14&size=400x400&key=AIzaSyDojYOvtzby9pBkDRXb8AMBattDiornKJ0" + "&markers=color:red%7Clabel:S%7C" + tournamentsEntity.getLatitude() + "," + tournamentsEntity.getLongitude();
            RequestOptions requestOptions = new RequestOptions();
            requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));
            Glide.with(getBaseContext())
                    .load(imageUri)
                    .apply(requestOptions)
                    .into(map);

            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AcceptRejectTournaments(tournamentsEntity.getID(), 1);
                    Intent intent = new Intent();
                    intent.putExtra(TOURNAMENT_ID_REQUEST, tournamentsEntity.getID());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });


            reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AcceptRejectTournaments(tournamentsEntity.getID(), 0);
                    Intent intent = new Intent();
                    intent.putExtra(TOURNAMENT_ID_REQUEST, tournamentsEntity.getID());
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });

            calendar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    showDatePicker(new onDateChosenInterface() {
//                        @Override
//                        public void onDateSet(String s) {
//
//                        }
//                    });

                    Intent intent = new Intent(UpcomingTournamentsDetailsActivity.this , CalendarActivity.class);
                    intent.putExtra("tournament_dates", tournamentsEntity);
                    intent.putExtra("key_created", 2);
                    startActivity(intent);
                }
            });


//        starthour.setText(String.valueOf(tournamentsEntity.getTRNStartHour()+ " PM"));
//        endHour.setText(String.valueOf(tournamentsEntity.getTRNEndHour())+" PM");
            getTournamentByID(token, tournamentsEntity.getID(), false);

        } else if (isNotification) {
            tournamentNotificationId = getIntent().getIntExtra("tournament_id_from_notification", 0);
            getTournamentByID(token, tournamentNotificationId, true);
        }

        listAdapter = new ListAdapter(this, getBaseContext());
        recyclerView = findViewById(R.id.recyclerView_list);
        lblListHeader = findViewById(R.id.lblListHeader);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerView.setAdapter(listAdapter);


        if (key == 0) {
            if (status == 0 || status ==1){
                if (tournamentsEntity.getStatus() == 0 || tournamentsEntity.getStatus() == 1){
                    subscribe.setVisibility(View.GONE);
                    tree.setVisibility(View.VISIBLE);
                }
                else {
                    tree.setVisibility(View.GONE);
                    subscribe.setVisibility(View.VISIBLE);
                }
            }
//            if (type == 2) {
//                subscribe.setVisibility(View.GONE);
//                tree.setVisibility(View.GONE);
//            } else {
//                tree.setVisibility(View.GONE);
//                subscribe.setVisibility(View.VISIBLE);
//            }
        } else if (key == 1) {

            subscribe.setVisibility(View.GONE);
            tree.setVisibility(View.VISIBLE);
        }
            else if (key == 3){

            tree.setVisibility(View.GONE);
            subscribe.setVisibility(View.GONE);
            accept.setVisibility(View.GONE);
            reject.setVisibility(View.GONE);
            subscribers.setVisibility(View.GONE);
            players.setVisibility(View.GONE);

        } else {
            tree.setVisibility(View.GONE);
            subscribe.setVisibility(View.GONE);
            accept.setVisibility(View.VISIBLE);
            reject.setVisibility(View.VISIBLE);

        }

        tree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isNotification) {
                    Intent intent = new Intent(UpcomingTournamentsDetailsActivity.this, TreeActivity.class);
                    intent.putExtra("tournois", (Parcelable) tournamentsEntity);
                    intent.putExtra("tokentour", token);
                    startActivity(intent);
                }
            }
        });

        subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isNotification) {
                    subscribeTournament(tournamentsEntity.getID(), category_id);
//                    Intent intent = new Intent(UpcomingTournamentsDetailsActivity.this,PaymentMethodsActivity.class);
//                    intent.putExtra("pay_tournament", tournamentsEntity);
//                    intent.putExtra("category_pay_id", category_id);
//                    startActivity(intent);
                }
                else{
                }
            }
        });
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isNotification) {
                    String uri = String.format(Locale.ENGLISH, "geo:%f,%f", tournamentsEntity.getLatitude(), tournamentsEntity.getLongitude());
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    startActivity(intent);
                }
            }
        });




        lblListHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recyclerView.getVisibility() == View.GONE) {
                    recyclerView.setVisibility(View.VISIBLE);
                } else
                    recyclerView.setVisibility(View.GONE);

            }
        });


//        token = SharedPrefUtils.getStringPreference(this, VerificationPageActivity.TOKEN);


//        Glide.with(getBaseContext()).load(imageUri).into(map);


    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        int type =   SharedPrefUtils.getIntegerPreference(this,MainActivity.USER_TYPE,0);
//
//        if(type != 0)
//            getMenuInflater().inflate(R.menu.edit_menu, menu);
//        return true;
//    }
//

    public void getTournamentByID(String ttoken, int tid, boolean checkeIN) {
        apiService = ApiUtils.getAPIService(token);
//        Locale locale = new Locale(language);
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        config.locale = locale;
//        getBaseContext().getResources().updateConfiguration(config,
//                getBaseContext().getResources().getDisplayMetrics());
        Call<SingleTournamentResponse> call = apiService.getTournamentByID(tid);

        call.enqueue(new Callback<SingleTournamentResponse>() {
            @Override
            public void onResponse(Call<SingleTournamentResponse> call, Response<SingleTournamentResponse> response) {
                if (response.isSuccessful()) {

//                    prizes.addAll(response.body().getTour().getTournamentsEntity().getPrizes());

                    if(!isNotification) {

                        prizes.addAll(response.body().getTour().getTournamentsEntity().getPrizes());
                        for (int i = 0; i < prizes.size(); i++) {
                            if (prizes.get(i).getRank() == 1)
                                gift = prizes.get(i).getName();
                        }
//                    card_prize.setText(gift);

                        if (gift == null) {
                            lblListHeader.setClickable(false);
//                        card_prize.setText("-");
                        }

                        listAdapter.addData(prizes);
                    }
                    else{
                        String start2 = response.body().getTour().getTournamentsEntity().getTRNStartDate();
                        String start3 = convertTime(start2);
                        startdate.setText(start3);

                        String end2 = response.body().getTour().getTournamentsEntity().getTRNEndDate();
                        String end3 = convertTime(end2);
                        enddate.setText(end3);

                        description.setText(response.body().getTour().getTournamentsEntity().getDescription());

                        if (response.body().getTour().getTournamentsEntity().getStatus() == 0 || response.body().getTour().getTournamentsEntity().getStatus() == 1) {
                            calendar.setVisibility(View.GONE);
                        }
                        else {
                            calendar.setVisibility(View.VISIBLE);
                        }

                        String start_sub_date2 = response.body().getTour().getTournamentsEntity().getTRN_SubsStartDate();
                        String start_sub_date3 = convertTime(start_sub_date2);
                        startsubdate.setText(start_sub_date3);

                        String end_sub_date = response.body().getTour().getTournamentsEntity().getTRN_SubsEndDate();
                        String end_sub_date1 = convertTime(end_sub_date);
                        endsubdate.setText(end_sub_date1);

                        card_location.setText(response.body().getTour().getTournamentsEntity().getLocationName());
                        country.setText(response.body().getTour().getTournamentsEntity().getCountry());
                        city.setText(response.body().getTour().getTournamentsEntity().getCity());
                        street.setText(response.body().getTour().getTournamentsEntity().getStreet());
                        if (checkeIN == true){
                            players.setVisibility(View.GONE);
                        }
                        else {
                            players.setText(String.valueOf(response.body().getTour().getTournamentsEntity().getNumberOfPlayers()));
                        }
                        cost.setText(String.valueOf(response.body().getTour().getTournamentsEntity().getTRNCost()));
                        maxPlayers.setText(String.valueOf(response.body().getTour().getTournamentsEntity().getMaxPlayers()));
                        if (response.body().getTour().getTournamentsEntity().getOrganizerName() != null) {
                            organizerName.setText("Tournament created by " + response.body().getTour().getTournamentsEntity().getOrganizerName());
                        }
                        else {
                            organizerName.setText("Tournament created by unknown");
                        }
                        if (response.body().getTour().getTournamentsEntity().getImgurl() != null) {
                            Glide.with(getBaseContext()).load(response.body().getTour().getTournamentsEntity().getImgurl()).apply(requestOptionss).into(imgUrl);
                        } else {
                            Glide.with(getBaseContext()).load(R.drawable.tour).apply(requestOptionss).into(imgUrl);
                        }
                        tourID = response.body().getTour().getTournamentsEntity().getID();


                        if (response.body().getTour().getTournamentsEntity().getTRNStartHour()>= 0 && response.body().getTour().getTournamentsEntity().getTRNStartHour() < 12)
                            startHourTime.setText(response.body().getTour().getTournamentsEntity().getTRNStartHour() + ":00 AM");
                        else{
                            startHourTime.setText(response.body().getTour().getTournamentsEntity().getTRNStartHour() + ":00 PM");
                        }
                        if (response.body().getTour().getTournamentsEntity().getTRNEndHour()>= 12 && response.body().getTour().getTournamentsEntity().getTRNEndHour() <= 23)
                            endHourTime.setText(response.body().getTour().getTournamentsEntity().getTRNEndHour() - 12 + ":00 PM");
                        else{
                            endHourTime.setText(response.body().getTour().getTournamentsEntity().getTRNEndHour() + ":00 AM");
                        }


                        calendar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
//                    showDatePicker(new onDateChosenInterface() {
//                        @Override
//                        public void onDateSet(String s) {
//
//                        }
//                    });

                                Intent intent = new Intent(UpcomingTournamentsDetailsActivity.this , CalendarActivity.class);
                                intent.putExtra("tournament_dates", response.body().getTour().getTournamentsEntity());
                                intent.putExtra("key_created", 2);
                                startActivity(intent);
                            }
                        });


                        map.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String uri = String.format(Locale.ENGLISH, "geo:%f,%f", response.body().getTour().getTournamentsEntity().getLatitude(), response.body().getTour().getTournamentsEntity().getLongitude());
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                                startActivity(intent);
                            }
                        });


                        accept.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AcceptRejectTournaments(response.body().getTour().getTournamentsEntity().getID(), 1);
                                Intent intent = new Intent();
                                intent.putExtra(TOURNAMENT_ID_REQUEST, tournamentsEntity.getID());
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        });


                        reject.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AcceptRejectTournaments(response.body().getTour().getTournamentsEntity().getID(), 0);
                                Intent intent = new Intent();
                                intent.putExtra(TOURNAMENT_ID_REQUEST, tournamentsEntity.getID());
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        });


                        String imageUri = "https://maps.googleapis.com/maps/api/staticmap?center=" + response.body().getTour().getTournamentsEntity().getLatitude() + "," + response.body().getTour().getTournamentsEntity().getLongitude() + "&zoom=14&size=400x400&key=AIzaSyDojYOvtzby9pBkDRXb8AMBattDiornKJ0" + "&markers=color:red%7Clabel:S%7C" + response.body().getTour().getTournamentsEntity().getLatitude() + "," + response.body().getTour().getTournamentsEntity().getLongitude();
                        RequestOptions requestOptions = new RequestOptions();
                        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));
                        Glide.with(getBaseContext())
                                .load(imageUri)
                                .apply(requestOptions)
                                .into(map);

                        prizes.addAll(response.body().getTour().getTournamentsEntity().getPrizes());
                        for (int i = 0; i < prizes.size(); i++) {
                            if (prizes.get(i).getRank() == 1)
                                gift = prizes.get(i).getName();
                        }
//                    card_prize.setText(gift);

                        if (gift == null) {
                            lblListHeader.setClickable(false);
//                        card_prize.setText("-");
                        }

                        listAdapter.addData(prizes);

                     subscribe.setOnClickListener(new View.OnClickListener() {
                         @Override
                         public void onClick(View view) {
                             subscribeTournament(response.body().getTour().getTournamentsEntity().getID(), response.body().getTour().getTournamentsEntity().getCategory_ID());
                         }
                     });
                    }


//                    description.setText(response.body().getTour().getTournamentsEntity().getDescription());
//                    card_prize.setText(response.body().getTour().getTournamentsEntity().getPrize());


                } else {
                    Log.e(TAG, "On Response :" + response.errorBody());

                }
            }


            @Override
            public void onFailure(Call<SingleTournamentResponse> call, Throwable t) {
                Log.e(TAG, "On Failure :" + t.getMessage());
            }
        });

    }



    public void showDatePicker(final UpcomingTournamentsDetailsActivity.onDateChosenInterface onDateSelectedInterface) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, i);
                calendar.set(Calendar.MONTH, i1);
                calendar.set(Calendar.DAY_OF_MONTH, i2);
                onDateSelectedInterface.onDateSet(DateUtils.convertDate(calendar.getTimeInMillis(), "dd-MM-yyyy"));


            }
        }, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }


    private interface onDateChosenInterface {
        void onDateSet(String s);
    }

    public void subscribeTournament(int touId, int catId) {
        apiService = ApiUtils.getAPIService(token);
        apiService.subscribeTournament(touId, catId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    Toast.makeText(UpcomingTournamentsDetailsActivity.this, response.body().toString(), Toast.LENGTH_SHORT).show();
                }
                else if(response.code()==401){
                    Intent intent = new Intent(UpcomingTournamentsDetailsActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(UpcomingTournamentsDetailsActivity.this, "you've already subscribed to this tournament", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }

    public void AcceptRejectTournaments(int id, int status) {
        apiService = ApiUtils.getAPIService(token);
        apiService.AcceptRejectTournament(id, status).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    Toast.makeText(UpcomingTournamentsDetailsActivity.this, response.body().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        if (type != 0 && tournamentsEntity.getMaxPlayers() != 0 && tournamentsEntity.getStatus() == 2 && key != 2)
            getMenuInflater().inflate(R.menu.delete_tournament_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeAdapter/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            Intent intent = new Intent();
            intent.putExtra(TOURNAMENT_ID_NOTIFICATION, tourID);
            setResult(RESULT_OK, intent);
            this.finish();
            return true;
        }
        if (id == R.id.action_delete_tournament) {

            new MaterialDialog.Builder(this)
                    .title("Warning message")
                    .content("Are you sure you want to delete this tournament ?")
                    .positiveText("yes")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            deleteTournament(tournamentsEntity.getID());
//                            Intent intent = new Intent();
//                            intent.putExtra(TOURNAMENT_ID, tournamentsEntity.getID());
//                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    })
                    .negativeText("No")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    }).show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        intent.putExtra(TOURNAMENT_ID_NOTIFICATION, tourID);
        setResult(RESULT_OK, intent);
    }

    public String convertTime(String date) {
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date newDate = null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("dd-MM-yyyy");
        date = spf.format(newDate);
        return date;
    }


    public void deleteTournament(final int tourId) {

        apiService = ApiUtils.getAPIService(token);

        apiService.deleteTournament(tourId).enqueue(new Callback<String>() {
            public static final String TAG = "";

            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    Toast.makeText(UpcomingTournamentsDetailsActivity.this, response.body().toString(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(UpcomingTournamentsDetailsActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Unable to submit post to API.");
            }
        });

    }


    @Override
    public void onItemClick(PrizeEntity prizeEntity, View view) {

    }

    @Override
    public void onError() {

    }
}
