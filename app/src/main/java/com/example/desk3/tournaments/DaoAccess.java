package com.example.desk3.tournaments;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by Desk 3 on 8/2/2018.
 */

@Dao
public interface DaoAccess {

    @Insert
    void insertOnlySingleCategory (Category category);
    @Insert
    void insertMultipleMovies (List<Category> categoryList);
    @Query("SELECT * FROM category_table WHERE CategoryId = CategoryId")
    Category fetchOneMoviesbyMovieId (int category);
    @Update
    void updateMovie (Category category);
    @Delete
    void deleteMovie (Category category);
}
