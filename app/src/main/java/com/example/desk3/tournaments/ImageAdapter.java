package com.example.desk3.tournaments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desk 3 on 6/28/2018.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {

    List<ImageData> imageDataList;
    private ImageInterface imageInterface;

    public ImageAdapter(ImageInterface imageInterface) {
        this.imageDataList = new ArrayList<>();
        this.imageInterface = imageInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false));
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Glide.with(holder.itemView.getContext()).load(R.drawable.backgammon).into(holder.imageView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageInterface.OnImageClick(imageDataList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageDataList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;


        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.category_image);
        }
    }

    public void addCategories(List<ImageData> imageData1) {
        imageDataList.clear();
        imageDataList.addAll(imageData1);
        notifyItemInserted(imageDataList.size());
    }


    public interface ImageInterface {
        void OnImageClick(ImageData imageData);

    }

}
