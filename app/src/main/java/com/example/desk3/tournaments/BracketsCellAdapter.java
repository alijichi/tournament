package com.example.desk3.tournaments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Desk 3 on 6/19/2018.
 */


public class BracketsCellAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private BracketsColomnFragment fragment;
    private Context context;
    private ArrayList<MatchData> list;
    private BracketsInterface bracketsInterface;
    private boolean handler;

    public BracketsCellAdapter(BracketsColomnFragment bracketsColomnFragment, Context context, ArrayList<MatchData> list, BracketsInterface bracketsInterface) {

        this.fragment = bracketsColomnFragment;
        this.context = context;
        this.list = list;
        this.bracketsInterface = bracketsInterface;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.layout_cell_brackets, parent, false);
        return new BracketsCellViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BracketsCellViewHolder viewHolder = null;
        if (holder instanceof BracketsCellViewHolder) {
            try {
                viewHolder = (BracketsCellViewHolder) holder;
                setFields(viewHolder, position);
            } catch (Exception e) {

            }
        }
    }

    private void setFields(final BracketsCellViewHolder viewHolder, final int position) {
        handler = new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                viewHolder.setAnimation(list.get(position).getHeight());
            }
        }, 100);

        if (list.get(position).getCompetitorOne().getId() == list.get(position).getCompetitorTwo().getId()) {
            viewHolder.getTeamOneName().setText(list.get(position).getCompetitorOne().getName());
            Glide.with(context).load(list.get(position).getCompetitorOne().getImageURl()).into(viewHolder.getImage_one());
            viewHolder.getTeam_two_layout().setVisibility(View.GONE);


        } else {

            viewHolder.getTeamOneName().setText(list.get(position).getCompetitorOne().getName());
            viewHolder.getTeamTwoName().setText(list.get(position).getCompetitorTwo().getName());
            if (list.get(position).getCompetitorOne().getScore() != null && list.get(position).getCompetitorTwo().getScore() != null) {
                if (list.get(position).getCompetitorOne().getScore().compareTo(list.get(position).getCompetitorTwo().getScore()) > 0) {
                    viewHolder.getTeamOneScore().setTypeface(Typeface.DEFAULT_BOLD);
                } else if (list.get(position).getCompetitorOne().getScore().compareTo(list.get(position).getCompetitorTwo().getScore()) <= 0) {
                    viewHolder.getTeamTwoScore().setTypeface(Typeface.DEFAULT_BOLD);
                }
            }
            if (list.get(position).getCompetitorOne().getScore() == null) {
                viewHolder.getTeamOneScore().setText("-");
            } else if (list.get(position).getCompetitorOne().getScore().isEmpty()) {
                viewHolder.getTeamOneScore().setText("-");
            } else {

                viewHolder.getTeamOneScore().setText(list.get(position).getCompetitorOne().getScore());
            }

            if (list.get(position).getCompetitorTwo().getScore() == null) {

                viewHolder.getTeamTwoScore().setText("-");
            } else if (list.get(position).getCompetitorTwo().getScore().isEmpty()) {
                viewHolder.getTeamTwoScore().setText("-");
            } else {
                viewHolder.getTeamTwoScore().setText(list.get(position).getCompetitorTwo().getScore());
            }
            Glide.with(context).load(list.get(position).getCompetitorOne().getImageURl()).into(viewHolder.getImage_one());
            Glide.with(context).load(list.get(position).getCompetitorTwo().getImageURl()).into(viewHolder.getImage_two());
        }

//        if (list.get(position).getRoundNb() == 1) {
//            viewHolder.getTitle().setText("Match");
//        } else {
//            viewHolder.getTitle().setText("Finals");
//        }

        viewHolder.getTitle().setText("Round: " + list.get(position).getRoundNb());

        if (list.get(position).getDate() == null) {
            viewHolder.getDate().setText("");
        } else {
            viewHolder.getDate().setText(convertDate(list.get(position).getDate()));
        }


        if (list.get(position).getCompetitorOne().getScore() == null || list.get(position).getCompetitorTwo().getScore() == null) {
            viewHolder.getRootLayout().setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    bracketsInterface.OnLongItemClick(list.get(position));
                    return true;

                }
            });
        }

        viewHolder.getTeamOneName().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bracketsInterface.OnItemClick(list.get(position).getCompetitorOne());
            }
        });

        viewHolder.getTeamTwoName().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bracketsInterface.OnItemClick(list.get(position).getCompetitorTwo());
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    public void setList(ArrayList<MatchData> colomnList) {
        this.list = colomnList;
        notifyDataSetChanged();
    }

    public void removeItem(CompetitorData competitorData) {

    }


    public static String convertDate(String date) {
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date newDate = null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("ddMMM");
        date = spf.format(newDate);
        return date;
    }

    public void updateMactScores(MatchData matchData) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getLevelId() == matchData.getLevelId()) {
                list.set(i, matchData);
                notifyItemChanged(i);
                return;
            }
        }
    }
}