package com.example.desk3.tournaments;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Desk 3 on 8/13/2018.
 */
@Entity
public class TournamentDate implements Parcelable {
    @Expose
    @SerializedName("TourDate")
    private String TourDate;

    @PrimaryKey(autoGenerate = true)
    private int id;




    public String getTourDate() {
        return TourDate;
    }

    public void setTourDate(String tourDate) {
        TourDate = tourDate;
    }


    public TournamentDate(String tourDate) {
        TourDate = tourDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.TourDate);
    }

    public TournamentDate() {
    }

    protected TournamentDate(Parcel in) {
        this.TourDate = in.readString();
    }

    public static final Parcelable.Creator<TournamentDate> CREATOR = new Parcelable.Creator<TournamentDate>() {
        @Override
        public TournamentDate createFromParcel(Parcel source) {
            return new TournamentDate(source);
        }

        @Override
        public TournamentDate[] newArray(int size) {
            return new TournamentDate[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
