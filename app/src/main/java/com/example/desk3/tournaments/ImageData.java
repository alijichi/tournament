package com.example.desk3.tournaments;

/**
 * Created by Desk 3 on 6/28/2018.
 */

public class ImageData {
    private String imagUrl;

    public ImageData(String imagUrl) {
        this.imagUrl = imagUrl;
    }

    public String getImagUrl() {
        return imagUrl;
    }

    public void setImagUrl(String imagUrl) {
        this.imagUrl = imagUrl;
    }
}
