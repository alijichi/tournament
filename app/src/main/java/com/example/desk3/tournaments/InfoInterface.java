package com.example.desk3.tournaments;

import android.net.Uri;

/**
 * Created by Desk 3 on 8/3/2018.
 */

public interface InfoInterface {
    void onInfoSet(TournamentsEntity tournamentsEntity, int pageNumber);

}
