package com.example.desk3.tournaments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrizesEntity {
    @Expose
    @SerializedName("Rank")
    private int Rank;
    @Expose
    @SerializedName("Name")
    private String Name;

    public int getRank() {
        return Rank;
    }

    public void setRank(int Rank) {
        this.Rank = Rank;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }
}
