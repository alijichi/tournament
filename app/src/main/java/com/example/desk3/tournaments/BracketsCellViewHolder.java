package com.example.desk3.tournaments;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Desk 3 on 6/19/2018.
 */

public class BracketsCellViewHolder extends RecyclerView.ViewHolder {

    private TextView teamOneName;
    private TextView teamTwoName;
    private TextView teamOneScore;
    private TextView teamTwoScore;
    private TextView title;
    private Animation animation;
    private RelativeLayout rootLayout;
    private ImageView image_one;
    private ImageView image_two;
    private RelativeLayout team_one_layout;
    private RelativeLayout team_two_layout;
    private TextView date;
   // private String title;

    public BracketsCellViewHolder(View itemView) {
        super(itemView);
        teamOneName = (TextView) itemView.findViewById(R.id.team_one_name);
        teamTwoName = (TextView) itemView.findViewById(R.id.team_two_name);
        teamOneScore = (TextView) itemView.findViewById(R.id.team_one_score);
        teamTwoScore = (TextView) itemView.findViewById(R.id.team_two_score);
        rootLayout = (RelativeLayout) itemView.findViewById(R.id.layout_root);
        image_one = itemView.findViewById(R.id.team_one_image);
        image_two = itemView.findViewById(R.id.team_two_image);
        team_one_layout = itemView.findViewById(R.id.team_one_layout);
        team_two_layout = itemView.findViewById(R.id.team_two_layout);
        date = itemView.findViewById(R.id.play_date);

        title = itemView.findViewById(R.id.team_title);
    }

    public void setAnimation(int height){
        animation = new SlideAnimation(rootLayout, rootLayout.getHeight(),
                height);
        animation.setInterpolator(new LinearInterpolator());
        animation.setDuration(200);
        rootLayout.setAnimation(animation);
        rootLayout.startAnimation(animation);
    }

    public TextView getTeamTwoName() {
        return teamTwoName;
    }

    public TextView getTeamOneScore() {
        return teamOneScore;
    }

    public TextView getTeamTwoScore() {
        return teamTwoScore;
    }

    public TextView getTeamOneName() {
        return teamOneName;
    }

    public ImageView getImage_one() {
        return image_one;
    }

    public void setImage_one(ImageView image_one) {
        this.image_one = image_one;
    }

    public ImageView getImage_two() {
        return image_two;
    }

    public void setImage_two(ImageView image_two) {
        this.image_two = image_two;
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }

    public RelativeLayout getTeam_one_layout() {
        return team_one_layout;
    }

    public void setTeam_one_layout(RelativeLayout team_one_layout) {
        this.team_one_layout = team_one_layout;
    }

    public RelativeLayout getTeam_two_layout() {
        return team_two_layout;
    }

    public void setTeam_two_layout(RelativeLayout team_two_layout) {
        this.team_two_layout = team_two_layout;
    }

    public RelativeLayout getRootLayout() {
        return rootLayout;
    }

    public void setRootLayout(RelativeLayout rootLayout) {
        this.rootLayout = rootLayout;
    }

    public TextView getDate() {
        return date;
    }

    public void setDate(TextView date) {
        this.date = date;
    }
}