package com.example.desk3.tournaments;

import android.text.format.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Desk 3 on 7/9/2018.
 */

public class DateUtils {

    public static String convertDate(String date) {
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date newDate = null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("dd-MM-yyyy");
        date = spf.format(newDate);
        return date;
    }

    public static String convertDate(long date, String format) {
        return DateFormat.format(format, new Date(date)).toString();
    }

    public static String formatDate(String format, String result, String date) {
        SimpleDateFormat spf = new SimpleDateFormat(format);
        Date newDate = null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat(result);
        date = spf.format(newDate);
        return date;
    }

    public static long toMilli(String input, String format) {
        Date date = null;
        try {
            date = new SimpleDateFormat(format, Locale.ENGLISH).parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
            return System.currentTimeMillis();
        }
        return date.getTime();
    }
}
