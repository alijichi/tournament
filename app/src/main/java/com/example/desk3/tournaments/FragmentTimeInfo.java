package com.example.desk3.tournaments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Desk 3 on 8/3/2018.
 */

public class FragmentTimeInfo extends Fragment implements DatePickerDialog.OnDateSetListener, NumberPicker.OnValueChangeListener {
    private EditText start_date;
    private EditText end_date;
    private EditText sub_start_date;
    private EditText sub_end_date;
    private TextView starthour;
    private TextView endhour;
    private DatePickerDialog datePickerDialog;
    private InfoInterface infoInterface;
    private Button next;
    private TextView previous;
    private TournamentsEntity tournamentsEntity;
    private int endhour1, endminute1, starthour1, startminute1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_time_info, container, false);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        start_date = view.findViewById(R.id.created_startdate);
        end_date = view.findViewById(R.id.created_enddate);
        sub_start_date = view.findViewById(R.id.created_start_sub_date);
        sub_end_date = view.findViewById(R.id.created_end_sub_date);
        starthour = view.findViewById(R.id.started_hour);
        endhour = view.findViewById(R.id.ended_hour);
        next = view.findViewById(R.id.next_time);
        previous = view.findViewById(R.id.previous_time);


        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(new onDateSelectedInterface() {
                    @Override
                    public void onDateSet(String s) {
                        if (isDateAfterOrEqual(sub_end_date.getText().toString(), s)) {
                            if (start_date.getText().toString().equals("") || start_date.getText() == null) {
                                start_date.setText(s);
                            } else {
                                Toast.makeText(getContext(), "please ensure that end date should be greater than end date", Toast.LENGTH_LONG).show();
                            }
                        }

                        else {
                            Toast.makeText(getContext(), "please ensure that start date should be greater than or equal to subscription end date date", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(new onDateSelectedInterface() {
                    @Override
                    public void onDateSet(String s) {
                        if (isDateAfter(start_date.getText().toString(), s)) {
                            end_date.setText(s);

                        } else {
                            Toast.makeText(getContext(), "please ensure that end date should be greater than or equal to start date", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        sub_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(new onDateSelectedInterface() {
                    @Override
                    public void onDateSet(String s) {
                        if (isDateAfter(sub_start_date.getText().toString(), s)) {
                            sub_end_date.setText(s);
                            sub_start_date.setClickable(true);
                            sub_end_date.setClickable(true);

                        } else {
                            Toast.makeText(getContext(), "please ensure that end date should be greater than or equal to start date", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        sub_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePicker(new onDateSelectedInterface() {
                    @Override
                    public void onDateSet(String s) {
                        if (sub_end_date.getText().toString().equals("") || sub_end_date.getText() == null){
                            sub_start_date.setText(s);
                        }
                        else if (isDateAfter(s,sub_end_date.getText().toString())) {
                            sub_start_date.setText(s);
                        }
                        else {
                            Toast.makeText(getContext(),"please ensure that start subscription date must be greater than the end subscription date",Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    tournamentsEntity = new TournamentsEntity();
                    tournamentsEntity.setTRNStartDate(DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", start_date.getText().toString()));
                    tournamentsEntity.setTRNEndDate(DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", end_date.getText().toString()));
                    tournamentsEntity.setTRN_SubsStartDate(DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", sub_start_date.getText().toString()));
                    tournamentsEntity.setTRN_SubsEndDate(DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", sub_end_date.getText().toString()));
                    String startHour = firstTwoCharacters(starthour.getText().toString().trim());
                    String endHour = firstTwoCharacters(endhour.getText().toString().trim());
                    if (startHour != null) {
                        tournamentsEntity.setTRNStartHour(Integer.valueOf(startHour));
                    }
                    if (endHour != null) {
                        tournamentsEntity.setTRNEndHour(Integer.valueOf(endHour));
                    }

                    infoInterface.onInfoSet(tournamentsEntity, 2);
                } else {
                    Toast.makeText(getContext(), "please enter a valid data", Toast.LENGTH_SHORT);
                }
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoInterface.onInfoSet(tournamentsEntity, 0);
            }
        });


        starthour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getContext())
                        .items(R.array.hours)
                        .title("Select start time")
                        .itemsCallback((dialog, itemView, position, text) -> {
                            starthour.setText(getResources().getStringArray(R.array.hours)[position]);
                        }).show();

              /*  // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                 starthour1 = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                 startminute1 = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        starthour.setText(String.valueOf(selectedHour));
                    }
                }, starthour1, startminute1, true);//Yes 24 hour time
                mTimePicker.setTitle("Select start time");
                mTimePicker.show();*/

            }
        });

        endhour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(getContext())
                        .items(R.array.hours)
                        .title("Select end time")
                        .itemsCallback((dialog, itemView, position, text) -> {
                            String selectedHour = getResources().getStringArray(R.array.hours)[position];
                            if (starthour.getText().toString() == null || starthour.getText().equals("Pick a start hour")) {
                                Toast.makeText(getContext(), "please enter a start hour", Toast.LENGTH_SHORT).show();
                            } else if (Integer.valueOf(selectedHour) > Integer.parseInt(starthour.getText().toString())) {

                                endhour.setText(String.valueOf(selectedHour));
                            } else {
                                Toast.makeText(getContext(), "end hour must be greater than start hour", Toast.LENGTH_SHORT).show();
                            }
                        }).show();
              /*  // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                endhour1 = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                endminute1 = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if (starthour.getText().toString() == null || starthour.getText().equals("Pick a start hour")) {
                            Toast.makeText(getContext(), "please enter a start hour", Toast.LENGTH_SHORT).show();
                        } else if (selectedHour > Integer.parseInt(starthour.getText().toString())) {

                            endhour.setText(String.valueOf(selectedHour));
                        } else {
                            Toast.makeText(getContext(), "end hour must be greater than start hour", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, endhour1, endminute1, true);//Yes 24 hour time
                mTimePicker.setTitle("Select end time");
                mTimePicker.show();*/

            }
        });
    }

//    public void show(String s)
//    {
//
//        final Dialog d = new Dialog(getContext());
//        d.setTitle("Time Picker");
//        d.setContentView(R.layout.time_dialog);
//        Button b1 = (Button) d.findViewById(R.id.button1);
//        Button b2 = (Button) d.findViewById(R.id.button2);
//        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
//        np.setMaxValue(100);
//        np.setMinValue(0);
//        np.setWrapSelectorWheel(false);
//        np.setOnValueChangedListener(this);
//        b1.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v) {
//                s.equals(np.getValue());
//                d.dismiss();
//            }
//        });
//        b2.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v) {
//                d.dismiss();
//            }
//        });
//        d.show();
//
//
//    }


    public void showDatePicker(final FragmentTimeInfo.onDateSelectedInterface onDateSelectedInterface) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, i);
                calendar.set(Calendar.MONTH, i1);
                calendar.set(Calendar.DAY_OF_MONTH, i2);
                onDateSelectedInterface.onDateSet(DateUtils.convertDate(calendar.getTimeInMillis(), "dd-MM-yyyy"));


            }
        }, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }


    public static boolean isTimeAfter(String startTime, String endTime) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
            Date inTime = sdf.parse(startTime);
            Date outTime = sdf.parse(endTime);

            if (outTime.after(inTime))
                return true;
            else
                return false;
        } catch (Exception e) {

            return false;
        }
    }


    public static boolean isDateAfter(String startDate, String endDate) {
        try {
            String myFormatString = "dd-MM-yyyy"; // for example
            SimpleDateFormat df = new SimpleDateFormat(myFormatString);
            Date date1 = df.parse(endDate);
            Date startingDate = df.parse(startDate);

            if (date1.after(startingDate))
                return true;
            else
                return false;
        } catch (Exception e) {

            return false;
        }
    }


    public static boolean isDateAfterOrEqual(String startDate, String endDate) {
        try {
            String myFormatString = "dd-MM-yyyy"; // for example
            SimpleDateFormat df = new SimpleDateFormat(myFormatString);
            Date date1 = df.parse(endDate);
            Date startingDate = df.parse(startDate);

            if (date1.after(startingDate) || (date1.equals(startingDate)))
                return true;
            else
                return false;
        } catch (Exception e) {

            return false;
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

        start_date.setText(String.valueOf(i));
    }

    public String firstTwoCharacters(String str) {
        String[] seperated = str.split(":");
        return seperated[0];
    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof InfoInterface)
            infoInterface = (InfoInterface) context;
        else throw new IllegalArgumentException("Main activity should implement table interface");
        super.onAttach(context);
    }

    public boolean validate() {
        boolean valid = true;

        String startDate = start_date.getText().toString();
        String endDate = end_date.getText().toString();
        String subStartDate = sub_start_date.getText().toString();
        String subEndDate = sub_end_date.getText().toString();
        String start_hour = starthour.getText().toString();
        String end_hour = endhour.getText().toString();


        if (startDate == null || startDate.isEmpty()) {
            start_date.setError("please enter a valid date ");
            valid = false;
        }
        if (endDate == null || endDate.isEmpty()) {
            end_date.setError("please enter a valid date ");
            valid = false;
        }
        if (subStartDate == null || subStartDate.isEmpty()) {
            sub_start_date.setError("please enter a valid date ");
            valid = false;
        }
        if (subEndDate == null || subEndDate.isEmpty()) {
            sub_end_date.setError("please enter a valid date ");
            valid = false;
        }

        if (start_hour.equals("Pick a start hour")) {
            starthour.setError("please enter a valid time ");
            valid = false;
        }
        if (end_hour.equals("Pick an end hour")) {
            endhour.setError("please enter a valid time ");
            valid = false;
        }
        return valid;
    }

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i1) {

    }


    private interface onDateSelectedInterface {
        void onDateSet(String s);
    }

}
