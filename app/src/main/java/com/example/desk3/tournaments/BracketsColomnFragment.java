package com.example.desk3.tournaments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Desk 3 on 6/19/2018.
 */
public class BracketsColomnFragment extends Fragment implements BracketsInterface {

    private ColomnData colomnData;
    private int sectionNumber = 0;
    private int previousBracketSize;
    private ArrayList<MatchData> list;
    private RecyclerView bracketsRV;
    private EditText score1, score2;
    private TextView scoreOneName, scoreTwoName;
    private UpdateScoresInterface updateScoresInterface;
    private String token;



    private BracketsCellAdapter adapter;
    private ApiService apiService;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    token =    SharedPrefUtils.getStringPreference(getContext(), VerificationPageActivity.TOKEN);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_brackets_colomn, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        getExtras();
        initAdapter();
    }

    private void initViews() {

        bracketsRV = (RecyclerView) getView().findViewById(R.id.rv_score_board);
    }

    public ArrayList<MatchData> getColomnList() {
        return list;
    }

    private void getExtras() {
        if (getArguments() != null) {
            list = new ArrayList<>();
            colomnData = (ColomnData) getArguments().getSerializable("colomn_data");
            sectionNumber = getArguments().getInt("section_number");
            previousBracketSize = getArguments().getInt("previous_section_size");
            list.addAll(colomnData.getMatches());
            setInitialHeightForList();
        }
    }

    public int getSectionNumber() {
        return sectionNumber;
    }

    private void setInitialHeightForList() {
        for (MatchData data : list) {
            if (sectionNumber == 0) {
                data.setHeight(BracketsUtility.dpToPx(131));
            } else if (sectionNumber == 1 && previousBracketSize != list.size()) {
                data.setHeight(BracketsUtility.dpToPx(262));
            } else if (sectionNumber == 1 && previousBracketSize == list.size()) {
                data.setHeight(BracketsUtility.dpToPx(131));
            } else if (previousBracketSize > list.size()) {
                data.setHeight(BracketsUtility.dpToPx(262));
            } else if (previousBracketSize == list.size()) {
                data.setHeight(BracketsUtility.dpToPx(131));
            }
        }

    }

    public void expandHeight(int height) {

        for (MatchData data : list) {
            data.setHeight(height);
        }
        adapter.setList(list);
    }

    public void shrinkView(int height) {
        for (MatchData data : list) {
            data.setHeight(height);
        }
        adapter.setList(list);
    }

    private void initAdapter() {

//        pBar.setVisibility(View.GONE);
        adapter = new BracketsCellAdapter(this, getContext(), list, this);
        if (bracketsRV != null) {
            bracketsRV.setHasFixedSize(true);
            bracketsRV.setNestedScrollingEnabled(false);
            bracketsRV.setAdapter(adapter);
            bracketsRV.smoothScrollToPosition(0);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            bracketsRV.setLayoutManager(layoutManager);
            bracketsRV.setItemAnimator(new DefaultItemAnimator());
        }
    }

    public int getCurrentBracketSize() {
        return list.size();
    }

    public int getPreviousBracketSize() {
        return previousBracketSize;
    }

    @Override
    public void OnLongItemClick(final MatchData matchData) {

        int type = SharedPrefUtils.getIntegerPreference(getContext(), MainActivity.USER_TYPE, 0);

        if (type != 0 && matchData.getCompetitorOne().getId() != matchData.getCompetitorTwo().getId() && matchData.isCanBeUpdate()) {
            LayoutInflater inflater = getLayoutInflater();
            View alertLayout = inflater.inflate(R.layout.compatitors_dialog_scores, null);
            score1 = alertLayout.findViewById(R.id.et_score1);
            score2 = alertLayout.findViewById(R.id.et_score2);
            scoreOneName = alertLayout.findViewById(R.id.first_score_name);
            scoreTwoName = alertLayout.findViewById(R.id.second_score_name);
            scoreOneName.setText(matchData.getCompetitorOne().getName());
            scoreTwoName.setText(matchData.getCompetitorTwo().getName());
            new MaterialDialog.Builder(getContext())
                    .title("Update scores")
                    .customView(alertLayout,false)
                    .theme(Theme.LIGHT)
                    .positiveText("yes").theme(Theme.LIGHT)
                    .negativeText("cancel")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            int et_score1 = Integer.valueOf(score1.getText().toString());
                            int et_score2 = Integer.valueOf(score2.getText().toString());
                            matchData.getCompetitorOne().setScore(String.valueOf(et_score1));
                            matchData.getCompetitorTwo().setScore(String.valueOf(et_score2));
//                    Toast.makeText(getContext(), "score1: " + score1 + " score2: " + score2, Toast.LENGTH_SHORT).show();
                            if(!score1.getText().toString().isEmpty() && !score2.getText().toString().isEmpty()&& score1.getText().toString() != score2.getText().toString()) {
                                if (et_score1 == et_score2){
                                    Toast.makeText(getContext(),"please select a winner", Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                }

                             if (et_score1 > et_score2) {
                                 matchData.setWinnerId(matchData.getCompetitorOne().getId());
                                 updateScored(matchData);
                             }

                                else if (et_score1 < et_score2) {
                                 matchData.setWinnerId(matchData.getCompetitorTwo().getId());
                                 updateScored(matchData);
                             }

                                }

                            else
                                {
                                Toast.makeText(getContext(),"please enter required fields",Toast.LENGTH_SHORT);
                            }

                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        }
                    }).show();

        }
    }

    @Override
    public void OnItemClick(CompetitorData competitorData) {
 Intent intent = new Intent(getContext(), UserProfileActivity.class);
 intent.putExtra("playerID",competitorData.getId());
 intent.putExtra("verifyUser",0);
 startActivity(intent);

    }

    private void updateScored(final MatchData matchData) {
        apiService =  apiService = ApiUtils.getAPIService(token);
        HashMap<String, Object> map = new HashMap<>();
        map.put("levelid", matchData.getLevelId());
        map.put("TrnID", matchData.getTournamentId());
        map.put("winnerID", matchData.getWinnerId());
        map.put("player1score", matchData.getCompetitorOne().getScore());
        map.put("player2score", matchData.getCompetitorTwo().getScore());
        apiService.updateScores(map).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), "Scores updated", Toast.LENGTH_SHORT).show();
                    adapter.updateMactScores(matchData);
                    sendScore();


                }

//                else if(response.code()==401){
//                    Intent intent = new Intent(getContext(), LoginActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                }
                else {
                    Toast.makeText(getContext(), "An error has occured", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void sendScore(){
        updateScoresInterface.updatedScores(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            updateScoresInterface = (UpdateScoresInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        updateScoresInterface = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }

    public interface UpdateScoresInterface{
        public void updatedScores(boolean update);
    }
}

