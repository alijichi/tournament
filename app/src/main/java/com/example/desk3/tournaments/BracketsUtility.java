package com.example.desk3.tournaments;

import android.util.DisplayMetrics;

/**
 * Created by Desk 3 on 6/19/2018.
 */

public class BracketsUtility {
    public static int dpToPx(int dp) {
        DisplayMetrics displayMetrics = BracketsApplication.getInstance().getBaseContext().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

}
