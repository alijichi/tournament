//package com.example.desk3.tournaments;
//
//import android.Manifest;
//import android.app.Activity;
//import android.app.DatePickerDialog;
//import android.app.TimePickerDialog;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.annotation.RequiresApi;
//import android.support.design.widget.Snackbar;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.PopupMenu;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.Button;
//import android.widget.DatePicker;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//import android.widget.TimePicker;
//import android.widget.Toast;
//
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.resource.bitmap.CenterCrop;
//import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
//import com.bumptech.glide.request.RequestOptions;
//import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
//import com.google.android.gms.common.GooglePlayServicesRepairableException;
//import com.google.android.gms.location.places.ui.PlacePicker;
//import com.google.firebase.messaging.FirebaseMessaging;
//import com.theartofdev.edmodo.cropper.CropImage;
//import com.theartofdev.edmodo.cropper.CropImageView;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.List;
//
//import okhttp3.MediaType;
//import okhttp3.MultipartBody;
//import okhttp3.RequestBody;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//public class CreateTournamentActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, ListAdapter.ListAdapterInterface {
//
//    private EditText country;
//    private EditText city;
//    private EditText street;
//    private EditText players;
//    private EditText cost;
//    private EditText location;
//    private EditText desc;
//    private ImageView map;
//    private ImageView image;
//    private ImageView choose_map;
//    int PLACE_PICKER_REQUEST = 1;
//    private Uri selectedImage;
//    private double latitude, longitude;
//    private Button add_tournament;
//    private EditText start_date;
//    private EditText end_date;
//    private EditText sub_start_date;
//    private EditText sub_end_date;
//    private ApiService apiService;
//    private String token;
//    private EditText name;
//    private ProgressBar progressBar;
//    private DataEntity category;
//    private int catId;
//    private TextView lblListHeader;
//    private RecyclerView recyclerView;
//    private ListAdapter listAdapter;
//    private EditText etRank, etPrize;
//    private EditText etRank1, etPrize1;
//    private String imageURl;
//    private TextView starthour;
//    private TextView endhour;
//
//
//    private DatePickerDialog datePickerDialog;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_create_tournament);
//
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        token = SharedPrefUtils.getStringPreference(CreateTournamentActivity.this, VerificationPageActivity.TOKEN);
//        apiService = ApiUtils.getAPIService(token);
//        country = findViewById(R.id.create_country);
//        city = findViewById(R.id.create_city);
//        street = findViewById(R.id.create_street);
//        players = findViewById(R.id.create_playersnb);
//        cost = findViewById(R.id.create_costamount);
//        location = findViewById(R.id.create_card_location);
//        desc = findViewById(R.id.create_desc);
//        map = findViewById(R.id.create_map);
////        choose_map = findViewById(R.id.choose_map);
//        image = findViewById(R.id.create_image);
//        add_tournament = findViewById(R.id.add_tournament);
//        start_date = findViewById(R.id.create_startdate);
//        end_date = findViewById(R.id.create_enddate);
//        sub_start_date = findViewById(R.id.create_start_sub_date);
//        sub_end_date = findViewById(R.id.create_end_sub_date);
//        progressBar = findViewById(R.id.progress_tour);
//        name = findViewById(R.id.tour_name);
//        lblListHeader = findViewById(R.id.lblListHeader);
//        recyclerView = findViewById(R.id.recyclerView_list_prizes);
//        starthour = findViewById(R.id.start_hour);
//        endhour = findViewById(R.id.end_hour);
//
//
//        catId = getIntent().getIntExtra(TournamentActivity.CATEGORY_ID, 0);
//
//
//        listAdapter = new ListAdapter(this);
//        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(linearLayoutManager);
//
//        start_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showDatePicker(new onDateSelectedInterface() {
//                    @Override
//                    public void onDateSet(String s) {
//                        start_date.setText(s);
//                    }
//                });
//            }
//        });
//
//        end_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showDatePicker(new onDateSelectedInterface() {
//                    @Override
//                    public void onDateSet(String s) {
//                        end_date.setText(s);
//                    }
//                });
//            }
//        });
//
//        sub_end_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showDatePicker(new onDateSelectedInterface() {
//                    @Override
//                    public void onDateSet(String s) {
//                        sub_end_date.setText(s);
//                    }
//                });
//            }
//        });
//
//        sub_start_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showDatePicker(new onDateSelectedInterface() {
//                    @Override
//                    public void onDateSet(String s) {
//                        sub_start_date.setText(s);
//                    }
//                });
//            }
//        });
//
//        starthour.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Calendar mcurrentTime = Calendar.getInstance();
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(CreateTournamentActivity.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                        starthour.setText(selectedHour + ":" + selectedMinute);
//                    }
//                }, hour, minute, true);//Yes 24 hour time
//                mTimePicker.setTitle("Select Time");
//                mTimePicker.show();
//
//            }
//        });
//
//        endhour.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Calendar mcurrentTime = Calendar.getInstance();
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(CreateTournamentActivity.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                        endhour.setText(selectedHour + ":" + selectedMinute);
//                    }
//                }, hour, minute, true);//Yes 24 hour time
//                mTimePicker.setTitle("Select Time");
//                mTimePicker.show();
//
//            }
//        });
//
//        lblListHeader.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                LayoutInflater inflater = getLayoutInflater();
//                View alertLayout = inflater.inflate(R.layout.custom_dialog, null);
//                etRank = alertLayout.findViewById(R.id.et_rank);
//                etPrize = alertLayout.findViewById(R.id.et_prize);
//
//                AlertDialog.Builder alert = new AlertDialog.Builder(CreateTournamentActivity.this);
////                    alert.setTitle("Prize List");
//                // this is set the view from XML inside AlertDialog
//                alert.setView(alertLayout);
//                // disallow cancel of AlertDialog on click of back button and outside touch
//                alert.setCancelable(false);
//                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
////                        Toast.makeText(getBaseContext(), "Cancel clicked", Toast.LENGTH_SHORT).show();
//                    }
//                });
//
//                alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        String rank = etRank.getText().toString();
//                        String prize = etPrize.getText().toString();
////                        Toast.makeText(getBaseContext(), "Rank: " + rank + " Prize: " + prize, Toast.LENGTH_SHORT).show();
//                        if (valid()) {
//                            listAdapter.addPrize(new PrizeEntity(Integer.valueOf(rank), prize));
//                            recyclerView.setAdapter(listAdapter);
//                        }
//
//                    }
//                });
//                AlertDialog dialog = alert.create();
//                dialog.show();
//            }
//
//        });
//
//
//        add_tournament.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (validate()) {
//                    progressBar.setVisibility(View.VISIBLE);
//
//                    RequestBody requestFile;
//                    MultipartBody.Part body = null;
//                    File file = new File(selectedImage.getPath());
//                    requestFile =
//                            RequestBody.create(
//                                    MediaType.parse("image/jpeg"),
//                                    file
//                            );
//
//                    body =
//                            MultipartBody.Part.createFormData("cewef", file.getName(), requestFile);
//
//
//                    updateImage(body);
//
//
//                } else {
//                    Snackbar.make(v, "please enter a correct data !!", Snackbar.LENGTH_SHORT).show();
//                    return;
//                }
//            }
//        });
//
//
//        map.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                pickPlace();
//            }
//        });
//
//        image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                pickImage();
//            }
//        });
//
//        initDatePicker();
//
//
//    }
//
//    private void initDatePicker() {
//
//    }
//
//
//    private void pickPlace() {
//
//
//        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//        Intent intent = null;
//        try {
//            intent = builder.build(CreateTournamentActivity.this);
//            startActivityForResult(intent, PLACE_PICKER_REQUEST);
//        } catch (GooglePlayServicesRepairableException e) {
//            e.printStackTrace();
//        } catch (GooglePlayServicesNotAvailableException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    private void pickImage() {
//        CropImage.startPickImageActivity(this);
//    }
//
//    private void startCropImageActivity(Uri imageUri) {
//        CropImage.activity(imageUri)
//                .setGuidelines(CropImageView.Guidelines.ON)
//                .setAspectRatio(16, 4)
//                .start(this);
//    }
//
//    public void showDatePicker(final onDateSelectedInterface onDateSelectedInterface) {
//        final Calendar c = Calendar.getInstance();
//        int year = c.get(Calendar.YEAR);
//        int month = c.get(Calendar.MONTH);
//        int day = c.get(Calendar.DAY_OF_MONTH);
//
//        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
//                Calendar calendar = Calendar.getInstance();
//                calendar.set(Calendar.YEAR, i);
//                calendar.set(Calendar.MONTH, i1);
//                calendar.set(Calendar.DAY_OF_MONTH, i2);
//                onDateSelectedInterface.onDateSet(DateUtils.convertDate(calendar.getTimeInMillis(), "dd-MM-yyyy"));
//
//            }
//        }, year, month, day);
//        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//        datePickerDialog.show();
//    }
//
//
//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        // handle result of pick image chooser
//        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
//            Uri imageUri = CropImage.getPickImageResultUri(this, data);
//
//            // For API >= 23 we need to check specifically that we have permissions to read external storage.
//            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
//                // request permissions and handle the result in onRequestPermissionsResult()
//                selectedImage = imageUri;
//                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
//            } else {
//                // no permissions required or already granted, can start crop image activity
//                startCropImageActivity(imageUri);
//            }
//        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//                selectedImage = result.getUri();
//                RequestOptions requestOptions = new RequestOptions();
//                requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));
//
//                Glide.with(this).load(selectedImage).apply(requestOptions).into(image);
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                Exception error = result.getError();
//            }
//        } else if (requestCode == PLACE_PICKER_REQUEST) {
//            if (resultCode == RESULT_OK) {
//                com.google.android.gms.location.places.Place place = PlacePicker.getPlace(this, data);
//                latitude = place.getLatLng().latitude;
//                longitude = place.getLatLng().longitude;
//                String toastMsg = String.format("Place: %s", place.getName());
//                String imageUri = "https://maps.googleapis.com/maps/api/staticmap?center=" + place.getLatLng().latitude + "," + place.getLatLng().longitude + "&zoom=17&size=400x400&key=AIzaSyDojYOvtzby9pBkDRXb8AMBattDiornKJ0" + "&markers=color:red%7Clabel:S%7C" + place.getLatLng().latitude + "," + place.getLatLng().longitude;
//                RequestOptions requestOptions = new RequestOptions();
//                requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));
//
//                Glide.with(this).load(imageUri).apply(requestOptions).into(map);
//            }
//        }
//
//
//    }
//
////    private void CreateUserImage(final Uri uri) {
////        if (uri == null) {
//////            sendTournamentWithoutImage();
////        } else {
////
////            RequestBody requestFile;
////            MultipartBody.Part body = null;
////            File file = new File(uri.getPath());
////            requestFile =
////                    RequestBody.create(
////                            MediaType.parse("image/jpeg"),
////                            file
////                    );
////
////            body =
////                    MultipartBody.Part.createFormData("ImageURl", file.getName(), requestFile);
////
////
////            sendTournamentWithImage(body);
////
////        }
////
////    }
//
////    private void sendTournamentWithImage(MultipartBody.Part body) {
////        apiService.updateProfile(body).enqueue(new Callback<Profile>() {
////            public static final String TAG = "";
////
////            @Override
////            public void onResponse(Call<Profile> call, Response<Profile> response) {
////                if (response.isSuccessful()) {
////
////
////                    String imageURl = response.body().getImageURl();
////
////
////                    SharedPrefUtils.setStringPreference(CreateTournamentActivity.this, "imgurl", imageURl);
////
////
//////                    progressBar.setVisibility(View.GONE);
////
//////                    sendPostWithoutImage(username, email, interests);
////                }
////            }
////
////            @Override
////            public void onFailure(Call<Profile> call, Throwable t) {
////                Log.e(TAG, "Unable to submit post to API.");
////            }
////        });
////    }
//
//    public void sendTournamentWithoutImage(String TRN_Name, String Country, String City, String Street, String Description, String LocationName, int numberOfPlayers, String TRNStartDate
//            , String TRNEndDate, String TRN_SubsStartDate, String TRN_SubsEndDate, double latitude, double longitude, int cost, int categoryId, String Imgurl, String start_hour, String end_hour) {
//        List<PrizeEntity> prizeEntityList = new ArrayList<>();
//        prizeEntityList = listAdapter.getPrizes();
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("TRN_Name", TRN_Name);
//        map.put("Country", Country);
//        map.put("City", City);
//        map.put("Street", Street);
//        map.put("Description", Description);
//        map.put("LocationName", LocationName);
//        map.put("MaxPlayers", numberOfPlayers);
//        map.put("TRNStartDate", TRNStartDate);
//        map.put("TRNEndDate", TRNEndDate);
//        map.put("TRN_SubsStartDate", TRN_SubsStartDate);
//        map.put("TRN_SubsEndDate", TRN_SubsEndDate);
//        map.put("Latitude", latitude);
//        map.put("Longitude", longitude);
//        map.put("TRNCost", cost);
//        map.put("Category_ID", categoryId);
//        map.put("Prizes", prizeEntityList);
//        map.put("Imgurl", Imgurl);
//        map.put("TRNStartHour",start_hour);
//        map.put("TRNEndHour", end_hour);
//
//        apiService.createTournamentWithoutImage(map).enqueue(new Callback<CreateTournamentResponse>() {
//            public static final String TAG = "";
//
//            @Override
//            public void onResponse(Call<CreateTournamentResponse> call, Response<CreateTournamentResponse> response) {
//                if (response.isSuccessful()) {
//
//
//                    progressBar.setVisibility(View.GONE);
//                    Toast.makeText(CreateTournamentActivity.this, "Tournament added successfuly", Toast.LENGTH_SHORT).show();
//                    finish();
//
//                }
//                else if(response.code()==401){
//                    Intent intent = new Intent(CreateTournamentActivity.this, LoginActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CreateTournamentResponse> call, Throwable t) {
//                Toast.makeText(CreateTournamentActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
//                Log.e(TAG, "Unable to submit post to API.");
//            }
//        });
//    }
//
//    public void updateImage(MultipartBody.Part body) {
//
//        apiService.updateImage(body).enqueue(new Callback<ImageTournamentResponse>() {
//            public static final String TAG = "";
//
//            @Override
//            public void onResponse(Call<ImageTournamentResponse> call, Response<ImageTournamentResponse> response) {
//                if (response.isSuccessful()) {
//
//                    imageURl = response.body().getPathUrl();
//
//                    String startHour = firstTwoCharacters(starthour.getText().toString().trim());
//                    String endHour = firstTwoCharacters(endhour.getText().toString().trim());
//
//                    sendTournamentWithoutImage(name.getText().toString(), country.getText().toString(), city.getText().toString(), street.getText().toString(), desc.getText().toString(), location.getText().toString(), Integer.valueOf(players.getText().toString())
//                            , DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", start_date.getText().toString())
//                            , DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", end_date.getText().toString())
//                            , DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", sub_start_date.getText().toString())
//                            , DateUtils.formatDate("dd-MM-yyyy", "yyyy-MM-dd'T'hh:mm:ss", sub_end_date.getText().toString())
//                            , latitude, longitude, Integer.valueOf(cost.getText().toString()), catId, imageURl,startHour, endHour);
//                }
//
//                else if(response.code()==401){
//                    Intent intent = new Intent(CreateTournamentActivity.this, LoginActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                }
//
//
//                else {
//                    Toast.makeText(CreateTournamentActivity.this, "An error has occurred", Toast.LENGTH_SHORT).show();
//                    progressBar.setVisibility(View.GONE);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ImageTournamentResponse> call, Throwable t) {
//                Log.e(TAG, "Unable to submit post to API.");
//                Toast.makeText(CreateTournamentActivity.this, "Error message", Toast.LENGTH_SHORT).show();
//                progressBar.setVisibility(View.GONE);
//            }
//        });
//    }
//
//
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
//            if (selectedImage != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                // required permissions granted, start crop image activity
//                startCropImageActivity(selectedImage);
//            } else {
//                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
//            }
//        }
//    }
//
//    public boolean valid() {
//        boolean validity = true;
//        String rank = etRank.getText().toString();
//        String prize = etPrize.getText().toString();
//        if (rank.isEmpty()) {
//            etRank.setError("enter a valid rank number");
//            validity = false;
//        }
//
//        if (prize.isEmpty()) {
//            etPrize.setError("enter a valid country");
//            validity = false;
//        }
//
//        return validity;
//
//    }
//
//
//    public boolean validate() {
//        boolean valid = true;
//
//        String countryName = country.getText().toString();
//        String cityName = city.getText().toString();
//        String streetName = street.getText().toString();
//        String playersCount = players.getText().toString();
//        String costAmount = cost.getText().toString();
//        String locationName = location.getText().toString();
//        String descContent = desc.getText().toString();
//        String nametour = name.getText().toString();
//        String startDate = start_date.getText().toString();
//        String endDate = end_date.getText().toString();
//        String subStartDate = sub_start_date.getText().toString();
//        String subEndDate = sub_end_date.getText().toString();
//
//
//        if (nametour == null || nametour.isEmpty()) {
//            name.setError("enter a valid country");
//            valid = false;
//        }
//
//        if (countryName == null || countryName.isEmpty()) {
//            country.setError("enter a valid country");
//            valid = false;
//        }
//
//        if (cityName.isEmpty() || cityName.isEmpty()) {
//            city.setError("please enter a valid city");
//            valid = false;
//        }
//
//        if (streetName.isEmpty() || streetName.isEmpty()) {
//            street.setError("please enter a valid street");
//            valid = false;
//        }
//
//
//        if (playersCount.isEmpty() || playersCount.isEmpty()) {
//            players.setError("please enter a valid players count");
//            valid = false;
//        }
//
//
//        if (costAmount.isEmpty() || costAmount.isEmpty()) {
//            cost.setError("please enter a valid cost");
//            valid = false;
//        }
//
//
//        if (descContent.isEmpty() || descContent.isEmpty()) {
//            desc.setError("please enter a valid desc");
//            valid = false;
//        }
//
//        if (locationName.isEmpty() || locationName.isEmpty()) {
//            location.setError("please enter a valid place");
//            valid = false;
//        }
//
//
//        if (map == null) {
//            Toast.makeText(this, "please add location", Toast.LENGTH_SHORT).show();
//            valid = false;
//        }
//
//        if (selectedImage == null) {
//            Toast.makeText(this, "please add an image", Toast.LENGTH_SHORT).show();
//            valid = false;
//        }
//
//        if (startDate == null || startDate.isEmpty()) {
//            start_date.setError("please enter a valid date ");
//            valid = false;
//        }
//        if (endDate == null || endDate.isEmpty()) {
//            end_date.setError("please enter a valid date ");
//            valid = false;
//        }
//        if (subStartDate == null || subStartDate.isEmpty()) {
//            sub_start_date.setError("please enter a valid date ");
//            valid = false;
//        }
//        if (subEndDate == null || subEndDate.isEmpty()) {
//            sub_end_date.setError("please enter a valid date ");
//            valid = false;
//        }
//
//        if (latitude == 0 || longitude == 0) {
//            Toast.makeText(this, "please add location", Toast.LENGTH_SHORT).show();
//            valid = false;
//        }
//        return valid;
//    }
//
////    @Override
////    public void onActivityResult(int requestCode, int resultCode, Intent data) {
////        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
////            CropImage.ActivityResult result = CropImage.getActivityResult(data);
////            if (resultCode == RESULT_OK) {
////                selectedImage = result.getUri();
////                Glide.with(this).load(selectedImage).into(image);
////            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
////                Exception error = result.getError();
////            }
////        }
////    }
//
//
//    public String firstTwoCharacters(String str){
//        String[] seperated = str.split(":");
//        return seperated[0];
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                // app icon in action bar clicked; goto parent activity.
//                this.finish();
//                return true;
//
//
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
//
//    @Override
//    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
//
//        start_date.setText(String.valueOf(i));
//    }
//
//    @Override
//    public void onItemClick(final PrizeEntity prizeEntity, View view) {
//        final PopupMenu menu = new PopupMenu(this, view);
//        menu.getMenuInflater().inflate(R.menu.delete_menu, menu.getMenu());
//        menu.show();
//
//
//        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//
//                switch (item.getItemId()) {
//
//                    case R.id.action_delete:
//                        listAdapter.deletePrize(prizeEntity.getID());
//                        return true;
//
//                    case R.id.action_edit:
//                        LayoutInflater inflater = getLayoutInflater();
//                        View alertLayout = inflater.inflate(R.layout.new_custom_dialog, null);
//                        etRank1 = alertLayout.findViewById(R.id.et_rank_edit);
//                        etPrize1 = alertLayout.findViewById(R.id.et_prize_edit);
//                        etRank1.setText(String.valueOf(prizeEntity.getRank()));
//                        etPrize1.setText(String.valueOf(prizeEntity.getName()));
//
//                        final AlertDialog.Builder alert = new AlertDialog.Builder(CreateTournamentActivity.this);
////                    alert.setTitle("Prize List");
//                        // this is set the view from XML inside AlertDialog
//                        alert.setView(alertLayout);
//                        // disallow cancel of AlertDialog on click of back button and outside touch
//                        alert.setCancelable(false);
//                        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
////                        Toast.makeText(getBaseContext(), "Cancel clicked", Toast.LENGTH_SHORT).show();
//                            }
//                        });
//
//                        alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                String rank = etRank1.getText().toString();
//                                String prize = etPrize1.getText().toString();
////                        Toast.makeText(getBaseContext(), "Rank: " + rank + " Prize: " + prize, Toast.LENGTH_SHORT).show();
//                                if (!etRank1.getText().toString().isEmpty() && !etPrize1.getText().toString().isEmpty()) {
//                                    prizeEntity.setName(prize);
//                                    prizeEntity.setRank(Integer.valueOf(rank));
//                                    listAdapter.updatePrize(prizeEntity);
////                                    recyclerView.setAdapter(listAdapter);
//                                }
//
//                                else{
//                                    Toast.makeText(getBaseContext(),"please enter a valid content",Toast.LENGTH_SHORT).show();
//                                }
//
//
//                            }
//                        });
//                        AlertDialog dialog = alert.create();
//                        dialog.show();
//
//
//                        return true;
//
//
//                    default:
//                        return true;
//
//                }
//            }
//        });
//    }
//
//    private interface onDateSelectedInterface {
//        void onDateSet(String s);
//    }
//
//}
