package com.example.desk3.tournaments;

import android.view.View;

/**
 * Created by Desk 3 on 7/20/2018.
 */

public interface OrganizerInterface {

    void OnOrganizerLongClick(OrganizerEntity organizerEntity, View view);
    void OnOrganizerClick(OrganizerEntity organizerEntity);
}
