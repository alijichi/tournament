package com.example.desk3.tournaments;

/**
 * Created by Desk 3 on 7/20/2018.
 */

public class SelectedCategories {

    private String name;
    private boolean isPressed;

    public SelectedCategories(String name, boolean isPressed) {
        this.name = name;
        this.isPressed = isPressed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPressed() {
        return isPressed;
    }

    public void setPressed(boolean pressed) {
        isPressed = pressed;
    }
}


