package com.example.desk3.tournaments;

/**
 * Created by Desk 3 on 5/23/2018.
 */

public interface CategoryInterface {

    void OnCategoryClick (DataEntity category);
    void OnInterestClick(DataEntity interest,boolean isPressed);
}
