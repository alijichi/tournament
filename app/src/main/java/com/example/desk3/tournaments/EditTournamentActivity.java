package com.example.desk3.tournaments;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.Calendar;

public class EditTournamentActivity extends AppCompatActivity {

    private TournamentsEntity tournamentsEntity;
    private EditText edit_country;
    private EditText edit_city;
    private EditText edit_street;
    private EditText edit_players;
    private EditText edit_cost;
    private EditText edit_location;
    private EditText edit_desc;
    private ImageView edit_map;
    private ImageView edit_image;
    private Uri selectedImage;
    private double edit_latitude, edit_longitude;
    private Button save_changes;
    private EditText edit_start_date;
    private EditText edit_end_date;
    private EditText edit_sub_start_date;
    private EditText edit_sub_end_date;
    private ApiService apiService;
    private String token;
    private EditText edit_name;
    private ProgressBar edit_progressBar;
    private DataEntity category;
    private int edit_catId;
    private DatePickerDialog datePickerDialog;
    int PLACE_PICKER_REQUEST = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_tournament);

//
//
//        edit_country = findViewById(R.id.edit_country);
//        edit_city = findViewById(R.id.edit_city);
//        edit_street = findViewById(R.id.edit_street);
//        edit_players = findViewById(R.id.edit_playersnb);
//        edit_cost = findViewById(R.id.edit_costamount);
//        edit_location = findViewById(R.id.edit_card_location);
//        edit_desc = findViewById(R.id.edit_desc);
//        edit_map = findViewById(R.id.edit_map);
//        edit_image = findViewById(R.id.edit_image);
//        save_changes = findViewById(R.id.save_changes);
//        edit_start_date = findViewById(R.id.edit_startdate);
//        edit_end_date = findViewById(R.id.edit_enddate);
//        edit_sub_start_date = findViewById(R.id.edit_start_sub_date);
//        edit_sub_end_date = findViewById(R.id.edit_end_sub_date);
//        edit_progressBar = findViewById(R.id.edit_progress);
//        edit_name = findViewById(R.id.edit_name);
//
//        edit_catId = getIntent().getIntExtra(TournamentActivity.CATEGORY_ID, 0);
//        tournamentsEntity = getIntent().getParcelableExtra(UpcomingTournamentsDetailsActivity.EDIT);
//
//        token = SharedPrefUtils.getStringPreference(EditTournamentActivity.this, VerificationPageActivity.TOKEN);
//
//        edit_name.setText(tournamentsEntity.getTRN_Name());
//        edit_country.setText(tournamentsEntity.getCountry());
//        edit_city.setText(tournamentsEntity.getCity());
//        edit_street.setText(tournamentsEntity.getStreet());
//        edit_players.setText(tournamentsEntity.getNumberOfPlayers());
//        edit_cost.setText(tournamentsEntity.getTRNCost());
//        edit_desc.setText(tournamentsEntity.getDescription());
//        edit_start_date.setText(tournamentsEntity.getTRNStartDate());
//        edit_end_date.setText(tournamentsEntity.getTRNEndDate());
//        edit_sub_start_date.setText(tournamentsEntity.getTRN_SubsStartDate());
//        edit_sub_end_date.setText(tournamentsEntity.getTRN_SubsEndDate());
//
//        String imageMap = "https://maps.googleapis.com/maps/api/staticmap?center=" + tournamentsEntity.getLatitude() + "," + tournamentsEntity.getLongitude() + "&zoom=17&size=400x400&key=AIzaSyDojYOvtzby9pBkDRXb8AMBattDiornKJ0" + "&markers=color:red%7Clabel:S%7C" + tournamentsEntity.getLatitude() + "," + tournamentsEntity.getLongitude();
//        Glide.with(this).load(imageMap).into(edit_map);
//
//
//
//       edit_start_date.setOnClickListener(new View.OnClickListener() {
//           @Override
//           public void onClick(View view) {
//               showDatePicker(new onDateSelectedChangeInterface() {
//                   @Override
//                   public void onDateChangeSet(String s) {
//                       edit_start_date.setText(s);
//                   }
//               });
//           }
//       });
//
//        edit_end_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showDatePicker(new onDateSelectedChangeInterface() {
//                    @Override
//                    public void onDateChangeSet(String s) {
//                        edit_end_date.setText(s);
//                    }
//                });
//            }
//        });
//
//        edit_sub_start_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showDatePicker(new onDateSelectedChangeInterface() {
//                    @Override
//                    public void onDateChangeSet(String s) {
//                        edit_sub_start_date.setText(s);
//                    }
//                });
//            }
//        });
//
//        edit_sub_end_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showDatePicker(new onDateSelectedChangeInterface() {
//                    @Override
//                    public void onDateChangeSet(String s) {
//                        edit_sub_end_date.setText(s);
//                    }
//                });
//            }
//        });
//
//
//        edit_map.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                pickPlace();
//            }
//        });
//
//        edit_image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                pickImage();
//            }
//        });
//
//
//
//    }
//
//
//    private void pickPlace() {
//
//
//        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//        Intent intent = null;
//        try {
//            intent = builder.build(EditTournamentActivity.this);
//            startActivityForResult(intent, PLACE_PICKER_REQUEST);
//        } catch (GooglePlayServicesRepairableException e) {
//            e.printStackTrace();
//        } catch (GooglePlayServicesNotAvailableException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    private void pickImage() {
//        CropImage.startPickImageActivity(this);
//    }
//
//    private void startCropImageActivity(Uri imageUri) {
//        CropImage.activity(imageUri)
//                .setGuidelines(CropImageView.Guidelines.ON)
//                .setAspectRatio(16, 4)
//                .start(this);
//    }
//
//
//
//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        // handle result of pick image chooser
//        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
//            Uri imageUri = CropImage.getPickImageResultUri(this, data);
//
//            // For API >= 23 we need to check specifically that we have permissions to read external storage.
//            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
//                // request permissions and handle the result in onRequestPermissionsResult()
//                selectedImage = imageUri;
//                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
//            } else {
//                // no permissions required or already granted, can start crop image activity
//                startCropImageActivity(imageUri);
//            }
//        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//                selectedImage = result.getUri();
//                RequestOptions requestOptions = new RequestOptions();
//                requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));
//
//                Glide.with(this).load(selectedImage).apply(requestOptions).into(edit_image);
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                Exception error = result.getError();
//            }
//        } else if (requestCode == PLACE_PICKER_REQUEST) {
//            if (resultCode == RESULT_OK) {
//                com.google.android.gms.location.places.Place place = PlacePicker.getPlace(this, data);
//                edit_latitude = place.getLatLng().latitude;
//                edit_longitude = place.getLatLng().longitude;
//                String toastMsg = String.format("Place: %s", place.getName());
//                String imageUri = "https://maps.googleapis.com/maps/api/staticmap?center=" + place.getLatLng().latitude + "," + place.getLatLng().longitude + "&zoom=17&size=400x400&key=AIzaSyDojYOvtzby9pBkDRXb8AMBattDiornKJ0" + "&markers=color:red%7Clabel:S%7C" + place.getLatLng().latitude + "," + place.getLatLng().longitude;
//                RequestOptions requestOptions = new RequestOptions();
//                requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(16));
//
//                Glide.with(this).load(imageUri).apply(requestOptions).into(edit_map);
//            }
//        }
//
//
//    }
//
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
//            if (selectedImage != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                // required permissions granted, start crop image activity
//                startCropImageActivity(selectedImage);
//            } else {
//                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
//            }
//        }
//    }
//
//
//    public boolean validate() {
//        boolean valid = true;
//
//        String countryName = edit_country.getText().toString();
//        String cityName = edit_city.getText().toString();
//        String streetName = edit_street.getText().toString();
//        String playersCount = edit_players.getText().toString();
//        String costAmount = edit_cost.getText().toString();
//        String locationName = edit_location.getText().toString();
//        String descContent = edit_desc.getText().toString();
//        String nametour = edit_name.getText().toString();
//        String startDate = edit_start_date.getText().toString();
//        String endDate = edit_end_date.getText().toString();
//        String subStartDate = edit_sub_start_date.getText().toString();
//        String subEndDate = edit_sub_end_date.getText().toString();
//
//
//        if (nametour == null || nametour.isEmpty()) {
//            edit_name.setError("enter a valid country");
//            valid = false;
//        }
//
//        if (countryName == null || countryName.isEmpty()) {
//            edit_country.setError("enter a valid country");
//            valid = false;
//        }
//
//        if (cityName.isEmpty() || cityName.isEmpty()) {
//            edit_city.setError("please enter a valid city");
//            valid = false;
//        }
//
//        if (streetName.isEmpty() || streetName.isEmpty()) {
//            edit_street.setError("please enter a valid street");
//            valid = false;
//        }
//
//
//        if (playersCount.isEmpty() || playersCount.isEmpty()) {
//            edit_players.setError("please enter a valid players count");
//            valid = false;
//        }
//
//
//        if (costAmount.isEmpty() || costAmount.isEmpty()) {
//            edit_cost.setError("please enter a valid cost");
//            valid = false;
//        }
//
//
//        if (descContent.isEmpty() || descContent.isEmpty()) {
//            edit_desc.setError("please enter a valid desc");
//            valid = false;
//        }
//
//        if (locationName.isEmpty() || locationName.isEmpty()) {
//            edit_location.setError("please enter a valid place");
//            valid = false;
//        }
//
//
//        if (edit_map == null) {
//            Toast.makeText(this, "please add location", Toast.LENGTH_SHORT).show();
//            valid = false;
//        }
//
//        if (selectedImage == null) {
//            Toast.makeText(this, "please add an image", Toast.LENGTH_SHORT).show();
//            valid = false;
//        }
//        if (selectedImage == null) {
//            Toast.makeText(this, "please add an image", Toast.LENGTH_SHORT).show();
//            valid = false;
//        }
//
//        if (startDate == null || startDate.isEmpty()) {
//            edit_start_date.setError("please enter a valid date ");
//            valid = false;
//        }
//        if (endDate == null || endDate.isEmpty()) {
//            edit_end_date.setError("please enter a valid date ");
//            valid = false;
//        }
//        if (subStartDate == null || subStartDate.isEmpty()) {
//            edit_sub_start_date.setError("please enter a valid date ");
//            valid = false;
//        }
//        if (subEndDate == null || subEndDate.isEmpty()) {
//            edit_sub_end_date.setError("please enter a valid date ");
//            valid = false;
//        }
//
//
//        return valid;
//    }
//
//
//
//    public void showDatePicker(final EditTournamentActivity.onDateSelectedChangeInterface onDateSelectedInterface) {
//        final Calendar c = Calendar.getInstance();
//        int year = c.get(Calendar.YEAR);
//        int month = c.get(Calendar.MONTH);
//        int day = c.get(Calendar.DAY_OF_MONTH);
//
//        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
//                Calendar calendar = Calendar.getInstance();
//                calendar.set(Calendar.YEAR, i);
//                calendar.set(Calendar.MONTH, i1);
//                calendar.set(Calendar.DAY_OF_MONTH, i2);
//                onDateSelectedInterface.onDateChangeSet(DateUtils.convertDate(calendar.getTimeInMillis(), "dd-MM-yyyy"));
//
//            }
//        }, year, month, day);
//        datePickerDialog.show();
//    }
//
//    private interface onDateSelectedChangeInterface {
//        void onDateChangeSet(String s);
//    }

    }

}
