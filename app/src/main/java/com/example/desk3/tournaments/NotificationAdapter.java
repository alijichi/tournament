package com.example.desk3.tournaments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desk 3 on 7/16/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private List<Notification> notifications;
    private NotificationInterface notificationInterface;
    private Context context;


    public NotificationAdapter(NotificationInterface notificationInterface, Context context) {
        notifications = new ArrayList<>();
        this.notificationInterface = notificationInterface;
        this.context = context;
    }

    @NonNull
    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notifications_list, parent, false);

        return new NotificationAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.text.setText(notifications.get(position).getText());

//        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                notificationInterface.onLongClick(notifications.get(position));
//                return true;
//            }
//        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notificationInterface.onClick(notifications.get(position));
            }
        });



        if (notifications.get(position).getReaded() == true) {
            holder.linearLayout.setBackgroundResource(R.drawable.rounded_cat);

        } else {
            holder.linearLayout.setBackgroundResource(R.drawable.notification_grey);

        }

        if (notifications.get(position).getType().equals("OnWon")) {
            Glide.with(context).load(R.drawable.won).into(holder.imageIcon);
        }

           else if
                (notifications.get(position).getType() .equals("NotifyAdmin")) {
             holder.imageIcon.setImageResource(R.drawable.add);

            }
        else if
                (notifications.get(position).getType() .equals( "OnTournamentCreation")) {
            Glide.with(context).load(R.drawable.treecreated).into(holder.imageIcon);

        }
        else if
                (notifications.get(position).getType() .equals( "OnCategoryCreation")) {
            Glide.with(context).load(R.drawable.add).into(holder.imageIcon);

        }
        else if
                (notifications.get(position).getType() .equals( "OnStartTour")) {
            Glide.with(context).load(R.drawable.start).into(holder.imageIcon);

        }
        else if
                (notifications.get(position).getType() .equals( "OnAccept")) {
            Glide.with(context).load(R.drawable.acceptes).into(holder.imageIcon);

        }
        else if
                (notifications.get(position).getType() .equals( "OnReject")) {
            Glide.with(context).load(R.drawable.rejected).into(holder.imageIcon);

        }
        else if
                (notifications.get(position).getType() .equals( "OnEndTour")) {
            Glide.with(context).load(R.drawable.endtournament).into(holder.imageIcon);

        }

        else if
                (notifications.get(position).getType() .equals( "OnTreeCreation")) {
            Glide.with(context).load(R.drawable.treecreated).into(holder.imageIcon);

        }

        else if
                (notifications.get(position).getType().equals("OnLost")) {
            Glide.with(context).load(R.drawable.lost).into(holder.imageIcon);

        }
        else{
            Glide.with(context).load(0).into(holder.imageIcon);
        }

    }


    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public void updateNotification(int id) {
        for (int i = 0; i < notifications.size(); i++) {
            if (notifications.get(i).getID() == id) {
                notifications.get(i).setReaded(true);
                notifyItemChanged(i);
                return;
            }
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout linearLayout;
        private TextView text;
        private ImageView imageIcon;


        public MyViewHolder(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.notification_text);
            imageIcon = itemView.findViewById(R.id.notification_icon);
            linearLayout = itemView.findViewById(R.id.not_lyout);

        }
    }

    public void addNotifications(List<Notification> notifications1) {
        notifications.addAll(notifications1);
        notifyDataSetChanged();

    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }
}
