package com.example.desk3.tournaments;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by Desk 3 on 8/16/2018.
 */


    @Database(entities = {DataEntity.class,TournamentsEntity.class/*,PrizeEntity.class,TournamentDate.class*/}, version = 1, exportSchema = false)
    public abstract class RoomDatabaseCategory extends RoomDatabase {
        public abstract CategoryDao categoryDao();

    private static RoomDatabaseCategory INSTANCE;


    static RoomDatabaseCategory getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RoomDatabaseCategory.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RoomDatabaseCategory.class, "word_database")
                            .build();

                }
            }
        }
        return INSTANCE;
    }

}



