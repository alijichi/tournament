package com.example.desk3.tournaments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Desk 3 on 7/10/2018.
 */

public class ImageTournamentResponse implements Parcelable {

    @SerializedName("pathUrl")
    private String pathUrl;

    public String getPathUrl() {
        return pathUrl;
    }

    public void setPathUrl(String pathUrl) {
        this.pathUrl = pathUrl;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.pathUrl);
    }

    public ImageTournamentResponse() {
    }

    protected ImageTournamentResponse(Parcel in) {
        this.pathUrl = in.readString();
    }

    public static final Parcelable.Creator<ImageTournamentResponse> CREATOR = new Parcelable.Creator<ImageTournamentResponse>() {
        @Override
        public ImageTournamentResponse createFromParcel(Parcel source) {
            return new ImageTournamentResponse(source);
        }

        @Override
        public ImageTournamentResponse[] newArray(int size) {
            return new ImageTournamentResponse[size];
        }
    };
}

