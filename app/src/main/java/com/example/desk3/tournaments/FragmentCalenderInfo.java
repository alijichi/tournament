package com.example.desk3.tournaments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnRangeSelectedListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desk 3 on 8/3/2018.
 */

public class FragmentCalenderInfo extends Fragment implements OnDateSelectedListener {
    private  MaterialCalendarView materialCalendarView;
    private FragmentCalenderInfoInterface fragmentCalenderInfoInterface;
    private Button next;
    private TournamentsEntity tournamentsEntity;
    private TextView previous;
    private List<TournamentDate> dates;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dates = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calender_info, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       materialCalendarView = view.findViewById(R.id.calendarView);
       materialCalendarView.setOnDateChangedListener(this);


        next = view.findViewById(R.id.next_calender);
        previous = view.findViewById(R.id.previous_calender);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dates != null) {
                    fragmentCalenderInfoInterface.onDatesRangeSet(dates);
                }
                else {
                    Toast.makeText(getContext(), " please select dates", Toast.LENGTH_SHORT).show();
                }
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentCalenderInfoInterface.showPrevious();
            }
        });


    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof InfoInterface)
            fragmentCalenderInfoInterface = (FragmentCalenderInfoInterface) context;
        else throw new IllegalArgumentException("Main activity should implement table interface");
        super.onAttach(context);
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
        if(b){
            dates.add(new TournamentDate(DateUtils.convertDate(calendarDay.getCalendar().getTimeInMillis(), "yyyy-MM-dd'T'hh:mm:ss")));
        }
        else{
            String s = DateUtils.convertDate(calendarDay.getCalendar().getTimeInMillis(), "yyyy-MM-dd'T'hh:mm:ss");
            if(dates.contains(s))
                dates.remove(s);
        }
    }

  /*  @Override
    public void onRangeSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull List<CalendarDay> list) {
        dates.clear();
        for (CalendarDay day : list) {
            dates.add(DateUtils.convertDate(day.getCalendar().getTimeInMillis(), "dd-MM-yyyy"));
        }
    }*/


    public interface FragmentCalenderInfoInterface {
        void onDatesRangeSet(List<TournamentDate> dates);

        void showPrevious();
    }

    public void updateCalendar(long minimum,long maximum){

        materialCalendarView.state().edit().setMinimumDate(minimum)
                .setMaximumDate(maximum)
                .commit();

    }
}
