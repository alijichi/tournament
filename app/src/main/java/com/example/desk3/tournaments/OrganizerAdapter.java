package com.example.desk3.tournaments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desk 3 on 7/20/2018.
 */

public class OrganizerAdapter extends RecyclerView.Adapter<OrganizerAdapter.MyViewHolder> {

    private List<OrganizerEntity> organizerEntities;
    private OrganizerInterface organizerInterface;
    private Context context;


    public OrganizerAdapter(OrganizerInterface organizerInterface1, Context context) {
        organizerEntities = new ArrayList<>();
        this.organizerInterface = organizerInterface1;
        this.context = context;
    }

    @NonNull
    @Override
    public OrganizerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.organizers_list, parent, false);

        return new OrganizerAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrganizerAdapter.MyViewHolder holder, int position) {

        holder.name.setText(organizerEntities.get(position).getName());
        holder.phoneNb.setText(organizerEntities.get(position).getPhoneNumber());

        holder.inactiveCount.setText(String.valueOf(organizerEntities.get(position).getInActivecount()));

        holder.activeCount.setText(String.valueOf(organizerEntities.get(position).getActivecount()));

        holder.finishedCount.setText(String.valueOf(organizerEntities.get(position).getFinishedCount()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View view) {
                                                       organizerInterface.OnOrganizerClick(organizerEntities.get(position));
                                               }
                                           }
        );

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                organizerInterface.OnOrganizerLongClick(organizerEntities.get(position), view);
                return  true;
            }
        });

        if (organizerEntities.get(position).getEmail() == null) {
            holder.email.setText("-");
        } else {
            holder.email.setText(String.valueOf(organizerEntities.get(position).getEmail()));
        }

        if (organizerEntities.get(position).getImageURl() == null || organizerEntities.get(position).getImageURl().isEmpty()) {
            Glide.with(context).load(R.drawable.organizer_backgammon).into(holder.imageView);
        } else {
            Glide.with(context).load(organizerEntities.get(position).getImageURl()).into(holder.imageView);
        }


    }

    @Override
    public int getItemCount() {
        return organizerEntities.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView phoneNb;
        private TextView email;
        private TextView inactiveCount;
        private TextView activeCount;
        private TextView finishedCount;
        private ImageView imageView;


        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.org_name);
            phoneNb = itemView.findViewById(R.id.org_phonenb);
            inactiveCount = itemView.findViewById(R.id.inactive_count_tournaments);
            activeCount = itemView.findViewById(R.id.active_count_tournaments);
            finishedCount = itemView.findViewById(R.id.finished_count_tournaments);
            imageView = itemView.findViewById(R.id.org_image);
            email = itemView.findViewById(R.id.org_email);

        }
    }

    public void addOrganizers(List<OrganizerEntity> organizerEntities1) {
//        tournamentsEntityList.clear();
        organizerEntities.addAll(organizerEntities1);
        notifyDataSetChanged();

    }

    public void deleteItem(int id) {
        for (int i = 0; i < organizerEntities.size(); i++) {
            if (organizerEntities.get(i).getID() == id) {
                organizerEntities.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }
}
