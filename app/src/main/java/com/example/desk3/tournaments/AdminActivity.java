package com.example.desk3.tournaments;

import android.content.Intent;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class AdminActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private List<DataEntity> dataEntityList = new ArrayList<>();
    private boolean organizerAddCheck = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dataEntityList = getIntent().getParcelableArrayListExtra("cats");

        viewPager = (ViewPager) findViewById(R.id.viewpager_admin);
        setupViewPager(viewPager);
        viewPager.setOffscreenPageLimit(2);

        tabLayout = (TabLayout) findViewById(R.id.tab_admin);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.isSmoothScrollingEnabled();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }




    private void setupViewPager(ViewPager viewPager) {


     ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundle = new Bundle();
        bundle.putBoolean("organizer_add", organizerAddCheck);
        FragmentOrganizers fragmentOrganizers = new FragmentOrganizers();
        fragmentOrganizers.setArguments(bundle);

        adapter.addFragment( new FragmentRequest(), "Requested");
        adapter.addFragment(fragmentOrganizers, "Organizers");
        viewPager.setAdapter(adapter);
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

            getMenuInflater().inflate(R.menu.add_organizer_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeAdapter/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if(id== R.id.action_add_organizer){
            Intent i = new Intent(AdminActivity.this, AddOrganizerActivity.class);
            i.putParcelableArrayListExtra("cats", (ArrayList<? extends Parcelable>) dataEntityList);
            startActivityForResult(i,FragmentOrganizers.ACTIVITY_REQUEST_ORGANIZER);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        organizerAddCheck = getIntent().getBooleanExtra("organizer_add", false);

    }
}
