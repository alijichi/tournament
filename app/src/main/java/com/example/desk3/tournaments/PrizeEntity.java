package com.example.desk3.tournaments;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.NonNull;

/**
 * Created by Desk 3 on 6/26/2018.
 */

@Entity
public class PrizeEntity implements Parcelable {
    @Expose
    @SerializedName("ID")
    @PrimaryKey
    @NonNull
    private int ID;
    @Expose
    @SerializedName("Rank")
    private int Rank;
    @Expose
    @SerializedName("Name")
    private String Name;

    public int getRank() {
        return Rank;
    }

    public void setRank(int Rank) {
        this.Rank = Rank;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }


    public PrizeEntity(int rank, String name) {
        Rank = rank;
        Name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.ID);
        dest.writeInt(this.Rank);
        dest.writeString(this.Name);
    }

    public PrizeEntity() {
    }

    protected PrizeEntity(Parcel in) {
        this.ID = in.readInt();
        this.Rank = in.readInt();
        this.Name = in.readString();
    }

    public static final Creator<PrizeEntity> CREATOR = new Creator<PrizeEntity>() {
        @Override
        public PrizeEntity createFromParcel(Parcel source) {
            return new PrizeEntity(source);
        }

        @Override
        public PrizeEntity[] newArray(int size) {
            return new PrizeEntity[size];
        }
    };
}


