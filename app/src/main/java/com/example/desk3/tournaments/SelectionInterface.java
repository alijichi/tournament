package com.example.desk3.tournaments;

/**
 * Created by Desk 3 on 7/19/2018.
 */

public interface SelectionInterface {
    void onClick (DataEntity dataEntity, boolean pressed);
}
