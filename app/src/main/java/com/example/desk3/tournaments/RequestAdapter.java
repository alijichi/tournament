package com.example.desk3.tournaments;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desk 3 on 7/23/2018.
 */

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.MyViewHolder>{
    private List<TournamentsEntity> tournamentsEntities;
    private RequestInterface requestInterface;


    public RequestAdapter(RequestInterface requestInterface) {
        tournamentsEntities = new ArrayList<>();
        this.requestInterface = requestInterface;
    }

    @NonNull
    @Override
    public RequestAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tournament_card, parent, false);

        return new RequestAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.Requestname.setText(tournamentsEntities.get(position).getTRN_Name());
        if(tournamentsEntities.get(position).getPrizes() == null ) {
            holder.Requestprize.setText("-");
        }
        else if(tournamentsEntities.get(position).getPrizes().isEmpty()){
            holder.Requestprize.setText("-");
        }
        else{
            holder.Requestprize.setText(tournamentsEntities.get(position).getPrizes().get(0).getName().toString());
        }

        holder.Requestcost.setText(String.valueOf(tournamentsEntities.get(position).getTRNCost()));
        holder.Requestplayers.setText(String.valueOf(tournamentsEntities.get(position).getMaxPlayers()));
        holder.Requestcolor.setBackgroundResource(R.color.colorPrimary);
        String date20 = tournamentsEntities.get(position).getTRN_SubsStartDate();

        holder.Requestsubs.setText(DateUtils.convertDate(date20));

        String date22 = tournamentsEntities.get(position).getTRNEndDate();
        holder.Requestsube.setText(DateUtils.convertDate(date22));

        String date = tournamentsEntities.get(position).getTRNStartDate();
        holder.Requestartdate.setText(DateUtils.convertDate(date));

        String date2 = tournamentsEntities.get(position).getTRNEndDate();
        holder.Requestenddate.setText(DateUtils.convertDate(date2));
        holder.Requestcity.setText(tournamentsEntities.get(position).getCategoryName());
//                    ((InactiveViewHolder)holder).InActivestreet.setText(tournaments.get(position).getStreet());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestInterface.OnItemClick(tournamentsEntities.get(position));
            }
        });


        String subdate1 = tournamentsEntities.get(position).getTRN_SubsStartDate();
        String subdate11 = DateUtils.convertDate(subdate1);
        holder.Requestsubs.setText(subdate11);
        String subdate2 = tournamentsEntities.get(position).getTRN_SubsEndDate();
        String subdate12 =DateUtils.convertDate(subdate1);
        holder.Requestsube.setText(subdate12);

    }

    @Override
    public int getItemCount() {
        return tournamentsEntities.size();
    }





    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView Requestname;
        private TextView Requestartdate;
        private TextView Requestenddate;
        private TextView Requestcity;
        private TextView Requesttreet;
        private TextView Requestprize;
        private TextView Requestcolor;
        private TextView Requestsubs;
        private TextView Requestsube;
        private TextView Requestcost;
        private TextView Requestplayers;



        public MyViewHolder(View itemView) {
            super(itemView);
            Requestname = (TextView) itemView.findViewById(R.id.card_name);
            Requestprize = (TextView) itemView.findViewById(R.id.card_prize);
            Requestartdate = (TextView) itemView.findViewById(R.id.start_date_tour);
            Requestenddate = (TextView) itemView.findViewById(R.id.end_date_tour);
            Requestcity = (TextView) itemView.findViewById(R.id.city);
            Requestcolor = itemView.findViewById(R.id.color_type);
            Requestsubs = (TextView) itemView.findViewById(R.id.start_date);
            Requestsube = itemView.findViewById(R.id.end_date);
            Requestcost = (TextView) itemView.findViewById(R.id.cost);
            Requestplayers = itemView.findViewById(R.id.players);

        }
    }

    public void addRequests(List<TournamentsEntity> tournamentsEntityList) {
//        tournamentsEntityList.clear();
        tournamentsEntities.addAll(tournamentsEntityList);
        notifyDataSetChanged();

    }

    public void deleteItem(int id) {
        for (int i = 0; i < tournamentsEntities.size(); i++) {
            if (tournamentsEntities.get(i).getID() == id) {
                tournamentsEntities.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }
    }
