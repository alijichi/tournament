package com.example.desk3.tournaments;

import android.content.Context;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desk 3 on 5/23/2018.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    List<DataEntity> categorList;
    private CategoryInterface categoryInterface;

    public CategoryAdapter(CategoryInterface categoryInterface) {
        this.categorList = new ArrayList<>();
        this.categoryInterface = categoryInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false));
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


//
//        String imgUrl = categorList.get(position).getImgurl();
//        Glide.with(holder.itemView.getContext()).load(imgUrl).into(holder.imageView);
        String name = categorList.get(position).getCategory_Name().toLowerCase();
        int id = getDrawableId(holder.itemView.getContext(), name);
        if (categorList.get(position).getImgurl() == null)
            Glide.with(holder.itemView.getContext()).load(getDrawableId(holder.itemView.getContext(), categorList.get(position).getCategory_Name().toLowerCase().replace(" ",""))).into(holder.imageView);
        else {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(getDrawableId(holder.itemView.getContext(), categorList.get(position).getCategory_Name().toLowerCase().replace(" ", "")));
            Glide.with(holder.itemView.getContext()).setDefaultRequestOptions(requestOptions).load(categorList.get(position).getImgurl()).into(holder.imageView);
        }
        holder.textView.setText(categorList.get(position).getCategory_Name());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryInterface.OnCategoryClick(categorList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return categorList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        private ImageView imageView;


        public MyViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.category_name);
            imageView = (ImageView) itemView.findViewById(R.id.category_image);
        }
    }

    public void addCategories(List<DataEntity> categories1) {
        final CategoryDiffUtil diffCallback = new CategoryDiffUtil(this.categorList, categories1);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.categorList.clear();
        this.categorList.addAll(categories1);
        diffResult.dispatchUpdatesTo(this);
    }


    private int getDrawableId(Context context, String name) {
        final int resourceId = context.getResources().getIdentifier(name, "drawable",
                context.getPackageName());
        if (resourceId == 0)
            return R.drawable.backgammon;
        return resourceId;
    }

    public class CategoryDiffUtil extends DiffUtil.Callback {

        private List<DataEntity> oldList;
        private List<DataEntity> newList;

        public CategoryDiffUtil(List<DataEntity> oldLis, List<DataEntity> newList) {
            this.oldList = oldLis;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition).getID() == newList.get(newItemPosition).getID();
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return false;
        }
    }
}

