package com.example.desk3.tournaments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desk 3 on 6/25/2018.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private ListAdapterInterface listAdapterInterface;
    private List<PrizeEntity> prizes ;
    private Context context;

    public ListAdapter(ListAdapterInterface ListAdapterInterface, Context context) {
        this.listAdapterInterface = ListAdapterInterface;
        prizes = new ArrayList<>();
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.prizes_list, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.title.setText(prizes.get(position).getName());
        holder.rank.setText("Rank: " + prizes.get(position).getRank());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listAdapterInterface.onItemClick(prizes.get(position),v);
            }
        });


    }

    @Override
    public int getItemCount() {
        return prizes.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView rank;



        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.exp_name);
            rank = itemView.findViewById(R.id.exp_rank);

        }
    }

    public interface ListAdapterInterface {
        void onItemClick(PrizeEntity prizeEntity, View view);
        void onError();

    }

    public void  addData(List<PrizeEntity> prizes1){
        prizes.clear();
            prizes.addAll(prizes1);
            notifyItemInserted(prizes.size());

    }

    public void addPrize(PrizeEntity prizeEntity){
        if (prizes == null || prizes.isEmpty()) {
        prizes.add(prizeEntity);
        notifyDataSetChanged();
        }
        else {
           boolean exist = false;
            for (int i = 0; i < prizes.size(); i++)
                if (prizeEntity.getRank() == prizes.get(i).getRank()) {
                    exist = true;
                }

                if (exist) {
                listAdapterInterface.onError();

            }else{
                    prizes.add(prizeEntity);
                    notifyDataSetChanged();
                }
        }

        }

    public void deletePrize(int id){
        for(int i =0; i< prizes.size(); i++) {
            if(prizes.get(i).getID() == id){
                prizes.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }

    public void updatePrize(PrizeEntity prizeEntity) {

        boolean exist1 = false;
        for (int i = 0; i < prizes.size(); i++)
            if (prizeEntity.getRank() == prizes.get(i).getRank()) {
                exist1 = true;
            }

        if (exist1) {
            listAdapterInterface.onError();
        }

        else {


            for(int i =0; i< prizes.size(); i++) {
                if (prizes.get(i).getID() == prizeEntity.getID()) {
                    prizes.set(i, prizeEntity);
                    notifyItemChanged(i);
                }
            }


        }
    }



    public List<PrizeEntity> getPrizes() {
        return prizes;
    }
}


