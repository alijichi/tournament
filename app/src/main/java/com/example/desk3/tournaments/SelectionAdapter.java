package com.example.desk3.tournaments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desk 3 on 7/19/2018.
 */

public class SelectionAdapter extends RecyclerView.Adapter<SelectionAdapter.ViewHolder> {

    private SelectionInterface selectionInterface;
    private List<DataEntity> dataEntities;
    private Context context;

    public SelectionAdapter(SelectionInterface selectionInterface, Context context) {
        this.selectionInterface = selectionInterface;
        this.context = context;
        dataEntities = new ArrayList<>();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selection_list, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.name.setText(dataEntities.get(position).getCategory_Name());


        if (dataEntities.get(position).isPressed() == true) {

            holder.name.setBackgroundResource(R.drawable.rounded_cat);
        } else {
            holder.name.setBackgroundResource(R.drawable.rounded_cat_dark);
        }


        changeInterestColor(dataEntities.get(position).isPressed(), holder, context);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dataEntities.get(position).isPressed()) {

                    dataEntities.get(position).setPressed(false);
                    selectionInterface.onClick(dataEntities.get(position), dataEntities.get(position).isPressed());
                    //changeInterestColor(interestList.get(position).isPressed(), holder, context);

                } else {

                    dataEntities.get(position).setPressed(true);
                    selectionInterface.onClick(dataEntities.get(position), dataEntities.get(position).isPressed());
                    //changeInterestColor(interestList.get(position).isPressed(), holder, context);
                }
                notifyItemChanged(position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return dataEntities.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;


        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.selection_name);

        }
    }

    private void changeInterestColor(boolean isPressed, ViewHolder holder, Context context) {


        if (isPressed)
            holder.name.setBackgroundResource(R.drawable.rounded_cat);
        else
            holder.name.setBackgroundResource(R.drawable.rounded_cat_dark);


    }

    public void addCategories(List<DataEntity> categories1) {

        dataEntities.clear();
        dataEntities.addAll(categories1);
        notifyDataSetChanged();
    }


}
