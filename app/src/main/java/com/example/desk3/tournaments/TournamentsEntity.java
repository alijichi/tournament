package com.example.desk3.tournaments;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.reactivex.annotations.NonNull;

@Entity
public class TournamentsEntity implements Parcelable {


    @Expose
    @SerializedName("Description")
    private String Description;
    @Expose
    @SerializedName("Status")
    private int Status;
    @Expose
    @SerializedName("Prize")
    private String Prize;
    @Expose
    @SerializedName("TRNEndDate")
    private String TRNEndDate;
    @Expose
    @SerializedName("TRNStartDate")
    private String TRNStartDate;
    @Expose
    @SerializedName("Street")
    private String Street;
    @Expose
    @SerializedName("City")
    private String City;
    @Expose
    @SerializedName("Country")
    private String Country;
    @Expose
    @SerializedName("Latitude")
    private double Latitude;
    @Expose
    @SerializedName("Longitude")
    private double Longitude;
    @Expose
    @SerializedName("Category_ID")
    private int Category_ID;
    @Expose
    @SerializedName("Admin_ID")
    private int Admin_ID;
    @Expose
    @SerializedName("TRN_Name")
    private String TRN_Name;
    @Expose
    @SerializedName("ID")
    @PrimaryKey
    @NonNull
    private int ID;
    @Expose
    @SerializedName("TRN_SubsStartDate")
    private String TRN_SubsStartDate;
    @Expose
    @SerializedName("TRN_SubsEndDate")
    private String TRN_SubsEndDate;
    @Expose
    @SerializedName("TRNCost")
    private int TRNCost;

    @Expose
    @SerializedName("NumberOfPlayers")
    private int NumberOfPlayers;
    @Expose
    @SerializedName("MaxPlayers")
    private int MaxPlayers;


    @Expose
    @SerializedName("Prizes")
    @TypeConverters(PrizeEntityConverter.class)
    private List<PrizeEntity> Prizes;

    @Expose
    @SerializedName("WinnerName")
    private String WinnerName;

    @Expose
    @SerializedName("CategoryName")
    private String categoryName;

    @Expose
    @SerializedName("LocationName")
    private String LocationName;


    @Expose
    @SerializedName("TRNStartHour")
    private int TRNStartHour;

    @Expose
    @SerializedName("TRNEndHour")
    private int TRNEndHour;
    @Expose
    @SerializedName("Imgurl")
    private String Imgurl;

    @Expose
    @SerializedName("TournamentDates")
    @TypeConverters(TournamentDateConverter.class)
    private List<TournamentDate> TournamentDates;

    @Expose
    @SerializedName("organizerName")
    private String organizerName;

    public TournamentsEntity() {
    }

    public TournamentsEntity(String description, int status, String prize, String TRNEndDate, String TRNStartDate, String street, String city, String country, double latitude, double longitude, int category_ID, int admin_ID, String TRN_Name, int ID, String TRN_SubsStartDate, String TRN_SubsEndDate, int TRNCost, int numberOfPlayers, int maxPlayers, List<PrizeEntity> prizes, String winnerName, String categoryName, String locationName, int TRNStartHour, int TRNEndHour, String imgurl, List<TournamentDate> tournamentDates, String organizerName) {
        Description = description;
        Status = status;
        Prize = prize;
        this.TRNEndDate = TRNEndDate;
        this.TRNStartDate = TRNStartDate;
        Street = street;
        City = city;
        Country = country;
        Latitude = latitude;
        Longitude = longitude;
        Category_ID = category_ID;
        Admin_ID = admin_ID;
        this.TRN_Name = TRN_Name;
        this.ID = ID;
        this.TRN_SubsStartDate = TRN_SubsStartDate;
        this.TRN_SubsEndDate = TRN_SubsEndDate;
        this.TRNCost = TRNCost;
        NumberOfPlayers = numberOfPlayers;
        MaxPlayers = maxPlayers;
        Prizes = prizes;
        WinnerName = winnerName;
        this.categoryName = categoryName;
        LocationName = locationName;
        this.TRNStartHour = TRNStartHour;
        this.TRNEndHour = TRNEndHour;
        Imgurl = imgurl;
        TournamentDates = tournamentDates;
        this.organizerName = organizerName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }


    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getPrize() {
        return Prize;
    }

    public void setPrize(String Prize) {
        this.Prize = Prize;
    }

    public String getTRNEndDate() {
        return TRNEndDate;
    }

    public void setTRNEndDate(String TRNEndDate) {
        this.TRNEndDate = TRNEndDate;
    }

    public String getTRNStartDate() {
        return TRNStartDate;
    }

    public void setTRNStartDate(String TRNStartDate) {
        this.TRNStartDate = TRNStartDate;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String Street) {
        this.Street = Street;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double Latitude) {
        this.Latitude = Latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double Longitude) {
        this.Longitude = Longitude;
    }

    public int getCategory_ID() {
        return Category_ID;
    }

    public void setCategory_ID(int Category_ID) {
        this.Category_ID = Category_ID;
    }

    public int getAdmin_ID() {
        return Admin_ID;
    }

    public void setAdmin_ID(int Admin_ID) {
        this.Admin_ID = Admin_ID;
    }

    public String getTRN_Name() {
        return TRN_Name;
    }

    public void setTRN_Name(String TRN_Name) {
        this.TRN_Name = TRN_Name;
    }

    public String getTRN_SubsEndDate() {
        return TRN_SubsEndDate;
    }

    public List<PrizeEntity> getPrizes() {
        return Prizes;
    }

    public void setPrizes(List<PrizeEntity> prizes) {
        Prizes = prizes;
    }

    public static Creator<TournamentsEntity> getCREATOR() {
        return CREATOR;
    }

    public void setTRN_SubsEndDate(String TRN_SubsEndDate) {
        this.TRN_SubsEndDate = TRN_SubsEndDate;
    }

    public int getTRNStartHour() {
        return TRNStartHour;
    }

    public void setTRNStartHour(int TRNStartHour) {
        this.TRNStartHour = TRNStartHour;
    }

    public int getTRNEndHour() {
        return TRNEndHour;
    }

    public void setTRNEndHour(int TRNEndHour) {
        this.TRNEndHour = TRNEndHour;
    }


    public String getTRN_SubsStartDate() {
        return TRN_SubsStartDate;
    }

    public void setTRN_SubsStartDate(String TRN_SubsStartDate) {
        this.TRN_SubsStartDate = TRN_SubsStartDate;
    }

    public String getImgurl() {
        return Imgurl;
    }

    public void setImgurl(String imgurl) {
        Imgurl = imgurl;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }


    public int getTRNCost() {
        return TRNCost;
    }

    public void setTRNCost(int TRNCost) {
        this.TRNCost = TRNCost;
    }

    public int getNumberOfPlayers() {
        return NumberOfPlayers;
    }

    public void setNumberOfPlayers(int numberOfPlayers) {
        NumberOfPlayers = numberOfPlayers;
    }

    public String getWinnerName() {
        return WinnerName;
    }

    public void setWinnerName(String winnerName) {
        WinnerName = winnerName;
    }

    public String getLocationName() {
        return LocationName;
    }

    public void setLocationName(String locationName) {
        LocationName = locationName;
    }


    public int getMaxPlayers() {
        return MaxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        MaxPlayers = maxPlayers;
    }

    public List<TournamentDate> getTournamentDates() {
        return TournamentDates;
    }

    public void setTournamentDates(List<TournamentDate> tournamentDates) {
        TournamentDates = tournamentDates;
    }

    public String getOrganizerName() {
        return organizerName;
    }

    public void setOrganizerName(String organizerName) {
        this.organizerName = organizerName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Description);
        dest.writeInt(this.Status);
        dest.writeString(this.Prize);
        dest.writeString(this.TRNEndDate);
        dest.writeString(this.TRNStartDate);
        dest.writeString(this.Street);
        dest.writeString(this.City);
        dest.writeString(this.Country);
        dest.writeDouble(this.Latitude);
        dest.writeDouble(this.Longitude);
        dest.writeInt(this.Category_ID);
        dest.writeInt(this.Admin_ID);
        dest.writeString(this.TRN_Name);
        dest.writeInt(this.ID);
        dest.writeString(this.TRN_SubsStartDate);
        dest.writeString(this.TRN_SubsEndDate);
        dest.writeInt(this.TRNCost);
        dest.writeInt(this.NumberOfPlayers);
        dest.writeInt(this.MaxPlayers);
        dest.writeTypedList(this.Prizes);
        dest.writeString(this.WinnerName);
        dest.writeString(this.categoryName);
        dest.writeString(this.LocationName);
        dest.writeInt(this.TRNStartHour);
        dest.writeInt(this.TRNEndHour);
        dest.writeString(this.Imgurl);
        dest.writeTypedList(this.TournamentDates);
        dest.writeString(this.organizerName);
    }

    protected TournamentsEntity(Parcel in) {
        this.Description = in.readString();
        this.Status = in.readInt();
        this.Prize = in.readString();
        this.TRNEndDate = in.readString();
        this.TRNStartDate = in.readString();
        this.Street = in.readString();
        this.City = in.readString();
        this.Country = in.readString();
        this.Latitude = in.readDouble();
        this.Longitude = in.readDouble();
        this.Category_ID = in.readInt();
        this.Admin_ID = in.readInt();
        this.TRN_Name = in.readString();
        this.ID = in.readInt();
        this.TRN_SubsStartDate = in.readString();
        this.TRN_SubsEndDate = in.readString();
        this.TRNCost = in.readInt();
        this.NumberOfPlayers = in.readInt();
        this.MaxPlayers = in.readInt();
        this.Prizes = in.createTypedArrayList(PrizeEntity.CREATOR);
        this.WinnerName = in.readString();
        this.categoryName = in.readString();
        this.LocationName = in.readString();
        this.TRNStartHour = in.readInt();
        this.TRNEndHour = in.readInt();
        this.Imgurl = in.readString();
        this.TournamentDates = in.createTypedArrayList(TournamentDate.CREATOR);
        this.organizerName = in.readString();
    }

    public static final Creator<TournamentsEntity> CREATOR = new Creator<TournamentsEntity>() {
        @Override
        public TournamentsEntity createFromParcel(Parcel source) {
            return new TournamentsEntity(source);
        }

        @Override
        public TournamentsEntity[] newArray(int size) {
            return new TournamentsEntity[size];
        }
    };
}