package com.example.desk3.tournaments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Desk 3 on 7/20/2018.
 */

public class OrganizerResponse {

    @Expose
    @SerializedName("Data")
    private List<OrganizerEntity> organizers;


    public List<OrganizerEntity> getOrganizers() {
        return organizers;
    }

    public void setOrganizers(List<OrganizerEntity> organizers) {
        this.organizers = organizers;
    }
}
