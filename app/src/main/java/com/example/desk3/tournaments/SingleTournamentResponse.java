package com.example.desk3.tournaments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Desk 3 on 6/14/2018.
 */

public class SingleTournamentResponse {

    @Expose
    @SerializedName("Data")
    private Tour tour;

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public class Tour {
        @Expose
        @SerializedName("tour")
        private TournamentsEntity tournamentsEntity;

        public TournamentsEntity getTournamentsEntity() {
            return tournamentsEntity;
        }

        public void setTournamentsEntity(TournamentsEntity tournamentsEntity) {
            this.tournamentsEntity = tournamentsEntity;
        }
    }

}
