package com.example.desk3.tournaments;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.reactivex.annotations.NonNull;

@Entity
public class DataEntity implements Parcelable {

    /* @ColumnInfo(name = "Finished")*/
    @Expose
    @SerializedName("Finished")
    @TypeConverters(TournamentConverter.class)
    private List<TournamentsEntity> Finished;

    /* @ColumnInfo(name = "Active")*/
    @Expose
    @SerializedName("Active")
    @TypeConverters(TournamentConverter.class)
    private List<TournamentsEntity> Active;

    /* @ColumnInfo(name = "InActive")*/
    @Expose
    @SerializedName("InActive")
    @TypeConverters(TournamentConverter.class)
    private List<TournamentsEntity> InActive;

    /*@ColumnInfo(name = "Tournaments")*/
    @Expose
    @SerializedName("Tournaments")
    @TypeConverters(TournamentConverter.class)
    private List<TournamentsEntity> Tournaments;

    @ColumnInfo(name = "Imgurl")
    @Expose
    @SerializedName("Imgurl")
    private String Imgurl;

    @ColumnInfo(name = "Category_Name")
    @Expose
    @SerializedName("Category_Name")
    private String Category_Name;

    @PrimaryKey
    @NonNull
    @Expose
    @SerializedName("ID")
    private int ID;

    @Expose
    @SerializedName("canBeAccessed")
    private boolean canBeAccessed;

    private boolean isPressed;

    public DataEntity(String category_Name, int ID) {
        Category_Name = category_Name;
        this.ID = ID;
    }

    public DataEntity(String category_Name, boolean isPressed) {
        Category_Name = category_Name;
        this.isPressed = isPressed;

    }




    public List<TournamentsEntity> getTournaments() {
        return Tournaments;
    }

    public void setTournaments(List<TournamentsEntity> Tournaments) {
        this.Tournaments = Tournaments;
    }

    public List<TournamentsEntity> getFinished() {
        return Finished;
    }

    public void setFinished(List<TournamentsEntity> finished) {
        Finished = finished;
    }

    public List<TournamentsEntity> getActive() {
        return Active;
    }

    public void setActive(List<TournamentsEntity> active) {
        Active = active;
    }

    public List<TournamentsEntity> getInActive() {
        return InActive;
    }

    public void setInActive(List<TournamentsEntity> inActive) {
        InActive = inActive;
    }

    public String getImgurl() {
        return Imgurl;
    }

    public void setImgurl(String Imgurl) {
        this.Imgurl = Imgurl;
    }

    public String getCategory_Name() {
        return Category_Name;
    }

    public void setCategory_Name(String Category_Name) {
        this.Category_Name = Category_Name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public boolean isPressed() {
        return isPressed;
    }

    public void setPressed(boolean pressed) {
        isPressed = pressed;
    }

    public boolean isCanBeAccessed() {
        return canBeAccessed;
    }

    public void setCanBeAccessed(boolean canBeAccessed) {
        this.canBeAccessed = canBeAccessed;
    }

    public DataEntity() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.Finished);
        dest.writeTypedList(this.Active);
        dest.writeTypedList(this.InActive);
        dest.writeTypedList(this.Tournaments);
        dest.writeString(this.Imgurl);
        dest.writeString(this.Category_Name);
        dest.writeInt(this.ID);
        dest.writeByte(this.canBeAccessed ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isPressed ? (byte) 1 : (byte) 0);
    }

    protected DataEntity(Parcel in) {
        this.Finished = in.createTypedArrayList(TournamentsEntity.CREATOR);
        this.Active = in.createTypedArrayList(TournamentsEntity.CREATOR);
        this.InActive = in.createTypedArrayList(TournamentsEntity.CREATOR);
        this.Tournaments = in.createTypedArrayList(TournamentsEntity.CREATOR);
        this.Imgurl = in.readString();
        this.Category_Name = in.readString();
        this.ID = in.readInt();
        this.canBeAccessed = in.readByte() != 0;
        this.isPressed = in.readByte() != 0;
    }

    public static final Creator<DataEntity> CREATOR = new Creator<DataEntity>() {
        @Override
        public DataEntity createFromParcel(Parcel source) {
            return new DataEntity(source);
        }

        @Override
        public DataEntity[] newArray(int size) {
            return new DataEntity[size];
        }
    };
}
