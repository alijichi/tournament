package com.example.desk3.tournaments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Desk 3 on 6/12/2018.
 */

public class InterestsAdapter extends RecyclerView.Adapter<InterestsAdapter.MyViewHolder> {
    List<Interest> interestList;
    private CategoryInterface categoryInterface;
    private Context context;

    public InterestsAdapter(CategoryInterface categoryInterface, Context context) {
        this.interestList = new ArrayList<>();
        this.context = context;
        this.categoryInterface = categoryInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.interest_item, parent, false));
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.textView.setText(interestList.get(position).getName());

        if (interestList.get(position).isPressed() == true) {

            holder.textView.setBackgroundResource(R.drawable.rounded_cat);
        } else {
            holder.textView.setBackgroundResource(R.drawable.rounded_cat_dark);
        }
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });

        changeInterestColor(interestList.get(position).isPressed(), holder, context);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (interestList.get(position).isPressed()) {
                    // categoryInterface.OnInterestClick(interestList.get(position), interestList.get(position).isPressed());
                    interestList.get(position).setPressed(false);
                    //changeInterestColor(interestList.get(position).isPressed(), holder, context);

                } else {
                    // categoryInterface.OnInterestClick(interestList.get(position), interestList.get(position).isPressed());
                    interestList.get(position).setPressed(true);
                    //changeInterestColor(interestList.get(position).isPressed(), holder, context);
                }
                notifyItemChanged(position);

            }
        });


    }

    @Override
    public int getItemCount() {
        return interestList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;


        public MyViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.interest_name);
        }
    }

    public void addInterests(List<Interest> interests) {

        interestList.clear();
        interestList.addAll(interests);
        notifyItemRangeInserted(0, interestList.size());
//        notifyDataSetChanged();


    }


    public void updateInterests(List<Interest> interests) {

        for (Interest interest : interestList) {
            for (Interest interest1 : interests) {
                if (interest1.getName().toLowerCase().equals(interest.getName().toLowerCase()))
                    interest.setPressed(true);
            }
        }
        notifyDataSetChanged();


    }


    private void changeInterestColor(boolean isPressed, MyViewHolder holder, Context context) {


        if (isPressed)
            holder.textView.setBackgroundResource(R.drawable.rounded_cat);
        else
            holder.textView.setBackgroundResource(R.drawable.rounded_cat_dark);


    }

    public String getInterests() {

        StringBuilder stringBuilder = new StringBuilder("");
        for (Interest interest : interestList)
            if (interest.isPressed()) {
                stringBuilder.append(interest.getName() + ",");
                FirebaseMessaging.getInstance().subscribeToTopic("topic_" + interest.getId());
            } else {
                FirebaseMessaging.getInstance().unsubscribeFromTopic("topic_" + interest.getId());
            }

        if (stringBuilder.toString().contains(","))
            stringBuilder.replace(stringBuilder.toString().lastIndexOf(","), stringBuilder.toString().lastIndexOf(",") + 1, "");


        return stringBuilder.toString();


    }

    public void addItems(List<String> strings) {
        for (int i = 0; i < interestList.size(); i++) {
            if (interestList.get(i).getName().equals(strings.get(i))) {
                interestList.get(i).setPressed(true);
                notifyDataSetChanged();
            }
            return;
        }

    }
}
