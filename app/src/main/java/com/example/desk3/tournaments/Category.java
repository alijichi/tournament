package com.example.desk3.tournaments;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Desk 3 on 8/1/2018.
 */

@Entity(tableName = "category_table")
public class Category {

    @PrimaryKey
    @NonNull
    private String CategoryId;

    @ColumnInfo(name = "category_name")
    private String CategoryName;

    @ColumnInfo(name = "category_image")
    private int categoryImage;

    public Category() {
    }

    @NonNull
    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(@NonNull String categoryId) {
        CategoryId = categoryId;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public int getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(int categoryImage) {
        this.categoryImage = categoryImage;
    }
}
