package com.example.desk3.tournaments;

import android.support.annotation.Nullable;

/**
 * Created by Desk 3 on 5/29/2018.
 */
public class ApiUtils {
    private ApiUtils() {}

    public static final String BASE_URL = "http://166.62.43.19:1797/api/";
    public static final String BASE_URL_2 = "http://166.62.43.19:1797/api/";

    public static ApiService getAPIService(@Nullable String token) {

        return RetrofitClient.getClient(BASE_URL,token).create(ApiService.class);
    }
    public static ApiService getAPIServices(@Nullable String token) {

        return RetrofitClient.getClient(BASE_URL_2,token).create(ApiService.class);
    }
}
